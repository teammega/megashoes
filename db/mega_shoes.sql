-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-10-2017 a las 00:12:10
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mega_shoes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id`, `id_empresa`, `nombre`, `total`, `estado`) VALUES
(2, 1, 'L (A)', 20, 1),
(3, 1, 'L (B)', 14, 1),
(4, 1, 'L (C)', 12, 1),
(5, 1, 'M (A)', 28, 1),
(6, 1, 'M (B)', 22, 1),
(7, 1, 'M (C)', 26, 1),
(8, 1, 'S (A)', 18, 1),
(9, 1, 'S (B)', 18, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `codigo` varchar(95) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limite_credito` int(250) DEFAULT NULL,
  `dias_credito` int(10) DEFAULT NULL,
  `saldo_disponible` int(250) DEFAULT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `negocio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `id_empresa`, `id_usuario`, `codigo`, `nombre`, `apellido`, `direccion`, `telefono`, `nit`, `limite_credito`, `dias_credito`, `saldo_disponible`, `observacion`, `negocio`, `tipo`, `estado`) VALUES
(1, 1, 2, 'CKF3A33R', 'Kenny', 'Chavez', 'zona 2', '34323456', '3380433-1', 4000, 28, 3000, 'Ninguna', '', '2', 1),
(2, 1, 2, '', 'Josue', 'Orantes', 'zona 2', '3344332', '3380455-1', 5000, 12, 1000, 'Ninguna', NULL, '1', 0),
(3, 1, 2, '123', 'Roberto', 'Salvatierra', 'Guatemala', '2938743987', '0', 1000, 90, 1000, 'El mejor', NULL, '1', 1),
(4, 1, 3, '4ABC123', 'Kenny', 'Chavez', 'DirecciÃ³n', '98738978', '98273', 4500, 15, 5000, 'Observaciones', NULL, '1', 1),
(5, 1, 2, '23sdf', 'prueba', 'prueba', 'zona 2', '34566765', '3370455-1', 10000, 50, 5000, 'ninguna', 'shopcodesss', '2', 1),
(6, 1, 2, '2323', 'pruebas', 'prueba', 'Hacienda de las flores zona 2 de villa nueva', '30654324', '3370455-1', 120000, 35, 3700, 'Ninguna', 'shopcode', '2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE `color` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `color`
--

INSERT INTO `color` (`id`, `id_empresa`, `nombre`, `color`, `color2`, `estado`) VALUES
(1, 1, 'Rojo', '#F44336', '#FFAC59', 1),
(2, 1, 'Celeste', '#03A9F4', '#4A7AFF', 1),
(3, 1, 'Negro', '#222222', NULL, 1),
(4, 1, 'Verde', '#8BC34A', NULL, 1),
(6, 1, 'Rojo', '#FF2929', NULL, 0),
(7, 1, 'Verde shumo', '#4AFF74', NULL, 0),
(8, 1, 'Prueba', '#FFADDC', '#C0FF2E', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja`
--

CREATE TABLE `detalle_caja` (
  `id` int(11) NOT NULL,
  `caja_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_caja`
--

INSERT INTO `detalle_caja` (`id`, `caja_id`, `talla_id`, `cantidad`, `estado`) VALUES
(1, 3, 1, 5, 1),
(2, 2, 1, 10, 1),
(3, 3, 2, 10, 1),
(4, 2, 2, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja_producto`
--

CREATE TABLE `detalle_caja_producto` (
  `id` int(11) NOT NULL,
  `id_caja` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_caja_producto`
--

INSERT INTO `detalle_caja_producto` (`id`, `id_caja`, `id_producto`, `estado`) VALUES
(1, 4, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 5, 2, 1),
(5, 6, 1, 1),
(6, 2, 3, 1),
(7, 2, 4, 1),
(8, 3, 5, 1),
(9, 4, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_pedido` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `cantidad` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id`, `id_empresa`, `id_pedido`, `id_producto`, `cantidad`, `estado`) VALUES
(1, 1, 1, 1, 2, 1),
(2, 1, 1, 2, 5, 1),
(3, 1, 1, 5, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `cuenta` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `descripcion`, `direccion`, `logo`, `favicon`, `correo`, `telefono`, `nit`, `titulo_flotante`, `titulo_flotante2`, `texto_flotante`, `titulo_inicio`, `texto_inicio`, `estado`, `cuenta`) VALUES
(1, 'MegaShoes', '', 'zona 1', '../img/L4lRZSv2rbI.jpeg', '../img/favicon.ico', 'info@megashoes.com', '24773245', NULL, 'Mega', 'Shoes', 'Perseguimos tus metas, para hacerlas realidad.', 'MegaShoes', 'Perseguimos tus metas, para hacerlas realidad.', 1, '555645434444');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id`, `id_empresa`, `nombre`, `estado`) VALUES
(1, 1, 'Vans', 1),
(2, 1, 'Converse', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE `modulo` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modulo`
--

INSERT INTO `modulo` (`id`, `id_empresa`, `img`, `nombre`, `link`, `estado`) VALUES
(1, 1, '../img/producto.jpg', 'Agregar producto', 'index.php', 1),
(2, 1, '../img/listado.jpg', 'Listado Productos', 'index.php', 1),
(3, 1, '../img/producto.jpg', 'Pedidos', 'index.php', 1),
(4, 1, '../img/producto.jpg', 'Agregar Pedido', 'index.php', 1),
(5, 1, '../img/listado.jpg', 'Vendedores', 'index.php', 1),
(6, 1, '../img/producto.jpg', 'Crear vendedor', 'index.php', 1),
(7, 1, '../img/listado.jpg', 'Empresa', 'index.php', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `observaciones` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` float DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `id_empresa`, `id_usuario`, `id_cliente`, `fecha`, `fecha_entrega`, `observaciones`, `total`, `estado`) VALUES
(1, 1, 2, 1, '2017-07-17', '2017-07-31', 'Se ha enviado el pedido', 2650, 3),
(2, 1, 2, 1, '2017-07-17', '2017-07-27', 'El pedido se ha enviado con un mensajero.', 0, 2),
(3, 1, 2, 1, '2017-07-04', '2017-07-19', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_base`
--

CREATE TABLE `pedido_base` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `tiempo_pedido` int(10) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_marca` int(10) DEFAULT NULL,
  `id_talla` int(10) DEFAULT NULL,
  `id_color` int(10) DEFAULT NULL,
  `id_padre` int(10) DEFAULT NULL,
  `id_proveedor` int(10) DEFAULT NULL,
  `id_caja` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `cantidad` int(10) DEFAULT NULL,
  `codigo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` int(250) DEFAULT NULL,
  `liquidacion` int(10) DEFAULT '0',
  `fecha` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `id_empresa`, `id_marca`, `id_talla`, `id_color`, `id_padre`, `id_proveedor`, `id_caja`, `nombre`, `img`, `descripcion`, `cantidad`, `codigo`, `precio`, `liquidacion`, `fecha`, `estado`) VALUES
(1, 1, 1, 1, 1, 0, 1, 2, 'Vans ', '../img/50zG5Gkai4HW.jpeg', NULL, 36, 'Q12R2JS', 300, 10, '24/05/2017', 1),
(2, 1, 1, 2, 2, 1, 1, 3, 'Vans Celestes', '../img/vans_celestes.jpg', '...', 24, 'N2R2JS', 230, 10, '24/05/2017', 1),
(3, 1, 2, 1, 3, 0, 1, 4, 'Converse ', '../img/converse_negros.jpg', NULL, 50, 'Q62R2JN', 399, 0, '24/05/2017', 1),
(4, 1, 2, 2, 4, 3, 1, 4, 'Converse verdes', '../img/converse_verdes.jpg', NULL, 48, 'S1R2JS', 200, 0, '24/05/2017', 1),
(5, 1, 2, 2, 1, 3, 1, 5, 'Converse rojos', '../img/converse_rojo.jpg', NULL, 24, 'AQ22JS', 300, 0, '24/05/2017', 1),
(6, 1, 2, 2, 2, 3, 1, 6, 'Converse celestes', '../img/converse_celestes.jpg', NULL, 48, 'Q12R2ZZ', 150, 0, '24/05/2017', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `id_empresa`, `nombre`, `apellido`, `img`, `direccion`, `telefono`, `nit`, `estado`) VALUES
(1, 1, 'Payless', NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `estado`) VALUES
(1, 'Administrador', 1),
(2, 'Vendedor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `id` int(11) NOT NULL,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ruta`
--

INSERT INTO `ruta` (`id`, `nombre`, `id_empresa`, `estado`) VALUES
(1, 'Oriente', 1, 1),
(2, 'Metropolitanas', 1, 1),
(3, 'Norte', 1, 1),
(4, 'pruebaas', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talla`
--

CREATE TABLE `talla` (
  `id` int(10) NOT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `talla` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `talla`
--

INSERT INTO `talla` (`id`, `id_empresa`, `talla`, `estado`) VALUES
(1, 1, '32', 1),
(2, 1, '33', 1),
(3, 1, '45', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id` int(11) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id`, `id_empresa`, `tipo`, `descripcion`, `estado`) VALUES
(1, 1, 'A', 'Cliente que paga anticipado', 1),
(2, 1, 'B', 'Cliente que paga en el dia exacto.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `id_rol` int(10) DEFAULT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `id_rol`, `id_empresa`, `nombre`, `apellido`, `img`, `correo`, `clave`, `telefono`, `direccion`, `nit`, `estado`) VALUES
(1, 1, 1, 'Admin', 'MegaShoes', '../img/Klo47BUkDUxg.png', 'admin@megashoes.com', '123', '12345678', '', NULL, 1),
(2, 2, 1, 'Fredy', 'Reyes', '../img/fredy.jpg', 'fredyreyesorantes123@gmail.com', '123', '43456543', 'zona 11', '337044-1', 1),
(3, 2, 1, 'Marco Tulio', 'Portillo', '../img/MJg2mfbmjCC9.bmp', 'mtp@gmail.com', '123', '87982734', 'Villa Nueva', '0', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_caja`
--
ALTER TABLE `detalle_caja`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_caja_producto`
--
ALTER TABLE `detalle_caja_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedido_base`
--
ALTER TABLE `pedido_base`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `talla`
--
ALTER TABLE `talla`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `color`
--
ALTER TABLE `color`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `detalle_caja`
--
ALTER TABLE `detalle_caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `detalle_caja_producto`
--
ALTER TABLE `detalle_caja_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `pedido_base`
--
ALTER TABLE `pedido_base`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ruta`
--
ALTER TABLE `ruta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `talla`
--
ALTER TABLE `talla`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
