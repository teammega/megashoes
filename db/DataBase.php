<?php
class DataBase{

	protected $dns = 'mysql:dbname=mega_shoes_pruebas;host=127.0.0.1';
	protected $user = 'root';
	protected $password = '';

	public function initDB(){
		$db = new PDO($this->dns, $this->user, $this->password);
		return $db;
	}

	public function getData($query){
		$db = $this->initDB();
		$resultPDO = $db->query($query);
		return $resultPDO->fetchAll(PDO::FETCH_ASSOC);
	}

	public function makeQuery($query){
		$db = $this->initDB();
		
		$makeQuery = $db->prepare($query);
		$makeQuery->execute();
		$row_count = $makeQuery->rowCount();

		if($row_count == 1){
			return true;
		}else{
			return false;
		}
	}
}