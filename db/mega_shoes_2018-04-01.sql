# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: mega_shoes
# Generation Time: 2018-04-02 02:34:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;

INSERT INTO `caja` (`id`, `id_empresa`, `nombre`, `total`, `estado`)
VALUES
	(2,1,'L (A)',20,1),
	(3,1,'L (B)',14,1),
	(4,1,'L (C)',12,1),
	(5,1,'M (A)',28,1),
	(6,1,'M (B)',22,1),
	(7,1,'M (C)',26,1),
	(8,1,'S (A)',18,1),
	(9,1,'S (B)',18,1);

/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `codigo` varchar(95) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limite_credito` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dias_credito` int(10) DEFAULT NULL,
  `saldo_disponible` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `negocio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;

INSERT INTO `cliente` (`id`, `id_empresa`, `id_usuario`, `codigo`, `nombre`, `apellido`, `direccion`, `telefono`, `nit`, `limite_credito`, `dias_credito`, `saldo_disponible`, `observacion`, `negocio`, `tipo`, `id_ruta`, `estado`)
VALUES
	(1,1,2,'CKF3A33R','Kenny','Chavez','zona 2','34323456','3380433-1','4000',28,'-14429.3','Ninguna','','2',0,1),
	(2,1,2,'4444','Josue','Orantes','zona 2','3344332','3380455-1','5000',12,'578','Ninguna',NULL,'1',0,1),
	(3,1,2,'123','Roberto','Salvatierra','Guatemala','2938743987','0','1000',90,'1000','El mejor',NULL,'1',0,1),
	(4,1,3,'4ABC123','Kenny','Chavez','Dirección','98738978','98273','45000',15,'29359','Observaciones',NULL,'1',1,1),
	(5,1,2,'23sdf','prueba','prueba','zona 2','34566765','3370455-1','10000',50,'5000','ninguna','shopcodesss','2',0,1),
	(6,1,2,'2323','pruebas','prueba','Hacienda de las flores zona 2 de villa nueva','30654324','3370455-1','120000',35,'3700','Ninguna','shopcode','2',0,1),
	(7,1,2,'XYZ123','Client Test','Test','DirecciÃ³n','12345678','98239487','5000',15,'5000','Usuario en limpio para pruebas','Zapato Kool! ','1',0,1),
	(8,1,1,'klje234','Rodrigo','Rosso','Zona 2','987293874','982739487','40000',15,'3842.9','Observaciones','Nombre','1',1,1);

/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `color`;

CREATE TABLE `color` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;

INSERT INTO `color` (`id`, `id_empresa`, `nombre`, `color`, `color2`, `estado`)
VALUES
	(1,1,'Rojo','#F44336','#FFAC59',1),
	(2,1,'Celeste','#03A9F4','#4A7AFF',1),
	(3,1,'Negro','#222222',NULL,1),
	(4,1,'Verde','#8BC34A',NULL,1),
	(6,1,'Rojo','#FF2929',NULL,0),
	(7,1,'Verde shumo','#4AFF74',NULL,0),
	(8,1,'Prueba','#FFADDC','#C0FF2E',0);

/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja`;

CREATE TABLE `detalle_caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caja_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_caja` WRITE;
/*!40000 ALTER TABLE `detalle_caja` DISABLE KEYS */;

INSERT INTO `detalle_caja` (`id`, `caja_id`, `talla_id`, `cantidad`, `estado`)
VALUES
	(1,3,1,5,1),
	(2,2,1,10,1),
	(3,3,2,10,1),
	(4,2,2,10,1);

/*!40000 ALTER TABLE `detalle_caja` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_caja_producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja_producto`;

CREATE TABLE `detalle_caja_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_caja_producto` WRITE;
/*!40000 ALTER TABLE `detalle_caja_producto` DISABLE KEYS */;

INSERT INTO `detalle_caja_producto` (`id`, `id_caja`, `id_producto`, `estado`)
VALUES
	(1,4,1,1),
	(2,2,1,1),
	(3,3,1,1),
	(4,5,2,1),
	(5,6,1,1),
	(6,2,3,1),
	(7,2,4,1),
	(8,3,5,1),
	(9,4,6,1);

/*!40000 ALTER TABLE `detalle_caja_producto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_pedido`;

CREATE TABLE `detalle_pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_pedido` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `cantidad` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_pedido` WRITE;
/*!40000 ALTER TABLE `detalle_pedido` DISABLE KEYS */;

INSERT INTO `detalle_pedido` (`id`, `id_empresa`, `id_pedido`, `id_producto`, `cantidad`, `estado`)
VALUES
	(1,1,1,1,2,1),
	(2,1,1,2,5,1),
	(3,1,1,5,3,1),
	(4,1,5,2,1,1),
	(5,1,5,4,2,1),
	(6,1,6,2,1,1),
	(7,1,6,4,1,1),
	(8,1,7,2,1,1),
	(9,1,7,4,1,1),
	(10,1,9,2,1,1),
	(11,1,9,4,1,1),
	(12,1,10,2,1,1),
	(13,1,10,4,1,1),
	(14,1,12,3,9,1),
	(15,1,12,4,8,1),
	(16,1,15,2,2,1),
	(17,1,15,4,2,1),
	(18,1,16,2,2,1),
	(19,1,16,5,1,1),
	(20,1,17,2,1,1),
	(21,1,17,4,1,1),
	(22,1,15,1,2,1),
	(23,1,15,2,2,1),
	(24,1,20,2,2,1),
	(25,1,20,4,2,1);

/*!40000 ALTER TABLE `detalle_pedido` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table empresa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `cuenta` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;

INSERT INTO `empresa` (`id`, `nombre`, `descripcion`, `direccion`, `logo`, `favicon`, `correo`, `telefono`, `nit`, `titulo_flotante`, `titulo_flotante2`, `texto_flotante`, `titulo_inicio`, `texto_inicio`, `estado`, `cuenta`)
VALUES
	(1,'Mekadessh','','zona 1','../img/L4lRZSv2rbI.jpeg','../img/favicon.ico','info@megashoes.com','24773245',NULL,'Meka','Desh','Perseguimos tus metas, para hacerlas realidad.','MegaShoes','Perseguimos tus metas, para hacerlas realidad.',1,'555645434444');

/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table formas_pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pago`;

CREATE TABLE `formas_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_id_doc` int(11) NOT NULL,
  `monto` float(11,2) NOT NULL,
  `banco` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_cambio` date NOT NULL,
  `lugar_ingreso` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `no_doc` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(1500) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_pago` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `formas_pago` WRITE;
/*!40000 ALTER TABLE `formas_pago` DISABLE KEYS */;

INSERT INTO `formas_pago` (`id`, `no_id_doc`, `monto`, `banco`, `id_pedido`, `fecha_ingreso`, `fecha_cambio`, `lugar_ingreso`, `no_doc`, `descripcion`, `tipo_pago`, `id_empresa`, `estado`)
VALUES
	(1,2876346,300.00,'Banco Industrial',10,'2018-01-11','2018-01-13','Original/Oficina','98238','Descripción','cheques',1,1),
	(2,2398747,1500.00,'Banco G&T',1,'2017-12-12','2018-01-01','Visto en linea','984983','Descricion','depositos',1,1),
	(3,2356214,200.00,'Banrural',7,'0000-00-00','0000-00-00','oficina','200','una descripcion','depositos',1,1),
	(4,254565,200.00,'G&T',7,'0000-00-00','0000-00-00','oficina','56464','daf','depositos',1,3),
	(5,546546,213153.00,'G&T',9,'0000-00-00','0000-00-00','oficina','232131','ninguna','depositos',1,2),
	(6,546546,213153.00,'G&T',9,'0000-00-00','0000-00-00','oficina','232131','ninguna','depositos',1,2),
	(7,546546,213153.00,'G&T',9,'0000-00-00','0000-00-00','oficina','232131','ninguna','depositos',1,2),
	(8,111,111.00,'Promerica',14,'0000-00-00','0000-00-00','casa','111','adf','depositos',1,1),
	(9,234324,234324.00,'BAC',7,'0000-00-00','0000-00-00','324','324','324','depositos',1,3),
	(10,234,234324.00,'Banrural',12,'2018-11-02','0000-00-00','casa','234324','bafdasf','depositos',1,1),
	(11,234324,234324.00,'Banrural',9,'0000-00-00','0000-00-00','casa','234324','asdf','depositos',1,3),
	(12,222,222.00,'Promerica',7,'2018-02-09','2018-02-16','oficina','222','adsfadsf','depositos',1,2),
	(13,222,222.00,'Promerica',6,'2018-02-12','2018-02-22','oficina','222','2222','depositos',1,2),
	(14,222,222.00,'Promerica',6,'2018-02-12','2018-02-22','oficina','222','2222','depositos',1,2),
	(15,0,100.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(16,0,200.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(17,0,100.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(18,0,100.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(19,0,100.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(20,0,100.00,'G&T',6,'2018-03-11','2018-03-11','Guatemala','0','Descripción','efectivo',1,1),
	(21,0,10.00,'G&T',13,'2018-03-11','2018-03-11','Oficina','0','Descripción','efectivo',1,1),
	(22,30003413,300.00,'BI',16,'2018-03-30','2018-03-30','Oficina','783774993','Pago del 50% del pedido 16','cheques',1,1);

/*!40000 ALTER TABLE `formas_pago` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table marca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marca`;

CREATE TABLE `marca` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;

INSERT INTO `marca` (`id`, `id_empresa`, `nombre`, `estado`)
VALUES
	(1,1,'Vans',1),
	(2,1,'Converse',1);

/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modulo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modulo`;

CREATE TABLE `modulo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `modulo` WRITE;
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;

INSERT INTO `modulo` (`id`, `id_empresa`, `img`, `nombre`, `link`, `estado`)
VALUES
	(1,1,'../img/producto.jpg','Agregar producto','index.php',1),
	(2,1,'../img/listado.jpg','Listado Productos','index.php',1),
	(3,1,'../img/producto.jpg','Pedidos','index.php',1),
	(4,1,'../img/producto.jpg','Agregar Pedido','index.php',1),
	(5,1,'../img/listado.jpg','Vendedores','index.php',1),
	(6,1,'../img/producto.jpg','Crear vendedor','index.php',1),
	(7,1,'../img/listado.jpg','Empresa','index.php',1);

/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pedido`;

CREATE TABLE `pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` float(11,2) DEFAULT NULL,
  `flete` float(11,2) NOT NULL,
  `descuento` int(11) NOT NULL,
  `no_envio` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(10) DEFAULT NULL,
  `factura` int(2) DEFAULT NULL,
  `porcentaje_pedido` int(3) DEFAULT NULL,
  `porcentaje_factura` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;

INSERT INTO `pedido` (`id`, `id_empresa`, `id_usuario`, `id_cliente`, `fecha`, `fecha_entrega`, `id_ruta`, `direccion`, `observaciones`, `total`, `flete`, `descuento`, `no_envio`, `estado`, `factura`, `porcentaje_pedido`, `porcentaje_factura`)
VALUES
	(1,1,2,1,'2017-07-17','2017-07-31',0,'','Se ha enviado el pedido',2650.00,0.00,0,'',5,NULL,NULL,NULL),
	(3,1,2,1,'2017-07-04','2017-07-19',0,'',NULL,NULL,0.00,0,'',5,NULL,NULL,NULL),
	(4,1,1,1,'0000-00-00','0000-00-00',2,'Dirección','Observaciones',0.00,15.00,0,'',5,NULL,NULL,NULL),
	(5,1,2,3,'0000-00-00','0000-00-00',2,'DirecciÃ³n','Observaciones',0.00,15.00,0,'',5,NULL,NULL,NULL),
	(8,1,1,3,'0000-00-00','0000-00-00',2,'DirecciÃ³n','Observaciones',0.00,15.00,0,'',5,NULL,NULL,NULL),
	(10,1,2,3,'0000-00-00','0000-00-00',2,'Jeje','Observaciones',430.00,100.00,0,'',5,NULL,NULL,NULL),
	(15,1,1,1,'2018-03-11','2018-03-11',2,'Direccion','Observaciones',16641.00,15.00,0,'11032018',1,1,30,70),
	(16,1,3,8,'2018-03-17','2018-03-19',2,'Zona 3','Observaciones',760.00,15.00,10,'17032018',2,1,55,45),
	(20,1,3,4,'2018-04-02','2018-04-02',1,'z 4','Observaciones',10641.00,45.00,0,'02042018',1,1,60,40);

/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_marca` int(10) DEFAULT NULL,
  `id_talla` int(10) DEFAULT NULL,
  `id_color` int(10) DEFAULT NULL,
  `id_padre` int(10) DEFAULT NULL,
  `id_proveedor` int(10) DEFAULT NULL,
  `id_caja` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `cantidad` int(10) DEFAULT NULL,
  `codigo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` float(11,2) DEFAULT NULL,
  `liquidacion` int(10) DEFAULT '0',
  `fecha` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;

INSERT INTO `producto` (`id`, `id_empresa`, `id_marca`, `id_talla`, `id_color`, `id_padre`, `id_proveedor`, `id_caja`, `nombre`, `img`, `descripcion`, `cantidad`, `codigo`, `precio`, `liquidacion`, `fecha`, `estado`)
VALUES
	(1,1,1,1,1,0,1,2,'Vans ','../img/50zG5Gkai4HW.jpeg','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',34,'Q12R2JS',300.00,10,'24/05/2017',1),
	(2,1,1,2,2,1,1,3,'Vans Celestes','../img/vans_celestes.jpg','...',15,'N2R2JS',230.00,10,'24/05/2017',1),
	(3,1,2,1,3,0,1,4,'Converse ','../img/converse_negros.jpg',NULL,50,'Q62R2JN',399.00,0,'24/05/2017',1),
	(4,1,2,2,4,3,1,4,'Converse verdes','../img/converse_verdes.jpg',NULL,43,'S1R2JS',200.00,0,'24/05/2017',1),
	(5,1,2,2,1,3,1,5,'Converse rojos','../img/converse_rojo.jpg',NULL,23,'AQ22JS',300.00,0,'24/05/2017',1),
	(6,1,2,2,2,3,1,6,'Converse celestes','../img/converse_celestes.jpg',NULL,48,'Q12R2ZZ',150.00,0,'24/05/2017',1),
	(7,1,1,0,1,0,1,6,'Producto','../img/no_image.png','Descripcion',2,'ABC123',100.00,0,'14-12-2017',1),
	(8,1,1,0,3,0,1,6,'Producto','../img/no_image.png','Descripcion',2,'ABC123',100.00,0,'14-12-2017',1),
	(9,1,1,0,2,0,1,3,'Product Test','../img/no_image.png','Descripcion',5,'yzx987',100.00,0,'20-12-2017',1),
	(10,1,1,0,3,0,1,3,'Product Test','../img/no_image.png','Descripcion',5,'yzx987',100.00,0,'20-12-2017',1),
	(11,1,1,0,2,0,1,2,'Producto QA 1','../img/no_image.png','Descripción QA 1',5,'QAENVY',120.00,0,'30-03-2018',1);

/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table proveedor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;

INSERT INTO `proveedor` (`id`, `id_empresa`, `nombre`, `apellido`, `img`, `direccion`, `telefono`, `nit`, `estado`)
VALUES
	(1,1,'Payless',NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;

INSERT INTO `rol` (`id`, `nombre`, `estado`)
VALUES
	(1,'Administrador',1),
	(2,'Vendedor',1);

/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ruta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ruta`;

CREATE TABLE `ruta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `ruta` WRITE;
/*!40000 ALTER TABLE `ruta` DISABLE KEYS */;

INSERT INTO `ruta` (`id`, `nombre`, `id_empresa`, `estado`)
VALUES
	(1,'Oriente',1,1),
	(2,'Metropolitanas',1,1),
	(3,'Norte',1,1),
	(4,'pruebaas',1,0);

/*!40000 ALTER TABLE `ruta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table talla
# ------------------------------------------------------------

DROP TABLE IF EXISTS `talla`;

CREATE TABLE `talla` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `talla` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `talla` WRITE;
/*!40000 ALTER TABLE `talla` DISABLE KEYS */;

INSERT INTO `talla` (`id`, `id_empresa`, `talla`, `estado`)
VALUES
	(1,1,'32',1),
	(2,1,'33',1),
	(3,1,'45',0);

/*!40000 ALTER TABLE `talla` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tipo_cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_cliente`;

CREATE TABLE `tipo_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tipo_cliente` WRITE;
/*!40000 ALTER TABLE `tipo_cliente` DISABLE KEYS */;

INSERT INTO `tipo_cliente` (`id`, `id_empresa`, `tipo`, `descripcion`, `estado`)
VALUES
	(1,1,'A','Cliente que paga anticipado',1),
	(2,1,'B','Cliente que paga en el dia exacto.',1);

/*!40000 ALTER TABLE `tipo_cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(10) DEFAULT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;

INSERT INTO `usuario` (`id`, `id_rol`, `id_empresa`, `nombre`, `apellido`, `img`, `correo`, `clave`, `telefono`, `direccion`, `nit`, `estado`)
VALUES
	(1,1,1,'Admin','MegaShoes','../img/Klo47BUkDUxg.png','admin@megashoes.com','123','12345678','',NULL,1),
	(2,2,1,'Fredy','Reyes','../img/fredy.jpg','fredyreyesorantes123@gmail.com','123','43456543','zona 11','337044-1',1),
	(3,2,1,'Marco Tulio','Portillo','../img/MJg2mfbmjCC9.bmp','mtp@gmail.com','123','87982734','Villa Nueva','0',1);

/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
