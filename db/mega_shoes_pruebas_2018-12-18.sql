# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: mega_shoes_pruebas
# Generation Time: 2018-12-18 22:08:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;

INSERT INTO `caja` (`id`, `id_empresa`, `nombre`, `total`, `estado`, `token`)
VALUES
	(17,1,'KIDS (B)',12,1,'sin_token'),
	(18,1,'Caballeros (A)',12,1,'sin_token');

/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `codigo` varchar(95) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limite_credito` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dias_credito` int(10) DEFAULT NULL,
  `saldo_disponible` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `negocio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;

INSERT INTO `cliente` (`id`, `id_empresa`, `id_usuario`, `codigo`, `nombre`, `apellido`, `direccion`, `telefono`, `nit`, `limite_credito`, `dias_credito`, `saldo_disponible`, `observacion`, `negocio`, `tipo`, `id_ruta`, `estado`, `token`)
VALUES
	(1,1,8,'CL - 0001','Joselyn','Rodríguez','Villa Nueva','45612989','48892004','10000',20,'5000','Cliente QA','Zara','2',2,1,'sin_token'),
	(2,1,8,'CL - 0002','Cristian','Ramírez','Villa Hermosa','89343432','98745948739','20000',50,'16870','Cliente QA','PDC','3',4,1,'sin_token'),
	(3,1,9,'987234897','Evelyn','Chavarria','Villa Nueva','98798798','98798798','10000',30,'3895','...','Cualquiera','2',3,1,'sin_token');

/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `color`;

CREATE TABLE `color` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;

INSERT INTO `color` (`id`, `id_empresa`, `nombre`, `color`, `color2`, `estado`, `token`)
VALUES
	(1,1,'Black And Black','#000000','#000000',1,'sin_token'),
	(2,1,'Black And Red','#050505','#FF0000',1,'sin_token'),
	(3,1,'White And Red','#FFFFFF','#FF0000',1,'sin_token');

/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja`;

CREATE TABLE `detalle_caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caja_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_caja` WRITE;
/*!40000 ALTER TABLE `detalle_caja` DISABLE KEYS */;

INSERT INTO `detalle_caja` (`id`, `caja_id`, `talla_id`, `cantidad`, `estado`)
VALUES
	(1,17,2,4,1),
	(2,17,5,4,1),
	(3,17,5,4,1),
	(4,18,14,4,1),
	(5,18,5,5,1),
	(6,18,24,3,1);

/*!40000 ALTER TABLE `detalle_caja` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_caja_producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja_producto`;

CREATE TABLE `detalle_caja_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_caja_producto` WRITE;
/*!40000 ALTER TABLE `detalle_caja_producto` DISABLE KEYS */;

INSERT INTO `detalle_caja_producto` (`id`, `id_caja`, `id_producto`, `estado`)
VALUES
	(1,17,1,1),
	(2,17,2,1),
	(3,17,3,1);

/*!40000 ALTER TABLE `detalle_caja_producto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detalle_pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_pedido`;

CREATE TABLE `detalle_pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_pedido` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `cantidad` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `detalle_pedido` WRITE;
/*!40000 ALTER TABLE `detalle_pedido` DISABLE KEYS */;

INSERT INTO `detalle_pedido` (`id`, `id_empresa`, `id_pedido`, `id_producto`, `cantidad`, `estado`)
VALUES
	(23,1,12,1,1,1),
	(24,1,12,2,1,1),
	(25,1,13,1,5,1),
	(26,1,13,2,12,1),
	(27,1,14,1,1,1),
	(28,1,14,2,1,1),
	(29,1,15,1,1,1),
	(30,1,15,2,1,1);

/*!40000 ALTER TABLE `detalle_pedido` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table empresa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `cuenta` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;

INSERT INTO `empresa` (`id`, `nombre`, `descripcion`, `direccion`, `logo`, `favicon`, `correo`, `telefono`, `nit`, `titulo_flotante`, `titulo_flotante2`, `texto_flotante`, `titulo_inicio`, `texto_inicio`, `estado`, `cuenta`)
VALUES
	(1,'MEGA-SHOES','Mekaddesh','GUATEMALA CITY','../img/UrQod4MgA0vv.png','...','megashoesg@gmail.com','3190-6644','...','Mega','Shoes','...','...','...',1,'1928337474');

/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table formas_pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pago`;

CREATE TABLE `formas_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_id_doc` int(11) DEFAULT NULL,
  `no_recibo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monto` float(11,2) DEFAULT NULL,
  `banco` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `id_pedido` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_deposito` date DEFAULT NULL,
  `fecha_recibo` date DEFAULT NULL,
  `lugar_ingreso` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `no_doc` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `descripcion` varchar(1500) COLLATE utf8_unicode_ci DEFAULT '',
  `tipo_pago` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `id_empresa` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table marca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marca`;

CREATE TABLE `marca` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;

INSERT INTO `marca` (`id`, `id_empresa`, `nombre`, `estado`)
VALUES
	(1,1,'GEERS',0),
	(3,1,'BAMBOO',1),
	(4,1,'generic_brand',1),
	(5,1,'Litte Big Riot v2',1);

/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modulo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modulo`;

CREATE TABLE `modulo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pedido`;

CREATE TABLE `pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `no_factura` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` float(11,2) DEFAULT NULL,
  `flete` float(11,2) NOT NULL,
  `descuento` int(11) NOT NULL,
  `no_envio` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(10) DEFAULT NULL,
  `factura` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `porcentaje_pedido` int(11) NOT NULL,
  `porcentaje_factura` int(11) NOT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;

INSERT INTO `pedido` (`id`, `id_empresa`, `no_factura`, `id_usuario`, `id_cliente`, `fecha`, `fecha_entrega`, `id_ruta`, `direccion`, `observaciones`, `total`, `flete`, `descuento`, `no_envio`, `estado`, `factura`, `porcentaje_pedido`, `porcentaje_factura`, `token`)
VALUES
	(12,1,NULL,8,1,'2018-11-27','2018-11-27',2,'Villa Nueva','Envío de mercadería a clienta',1130.00,50.00,0,'27/11/2018',1,'0',100,0,'sin_token'),
	(13,1,NULL,8,1,'2018-11-27','2018-11-28',1,'Villa Nueva','Envío 2 a clienta',8880.00,120.00,0,'27112018',1,'1',70,30,'sin_token'),
	(14,1,NULL,9,3,'2018-11-28','2018-11-28',3,'Villa Nueva','...',1105.00,25.00,0,'28112018',1,'1',60,40,'sin_token'),
	(15,1,NULL,8,2,'2018-12-18','2018-12-21',1,'Villa Hermosa','Observaciones',1130.00,50.00,0,'18122018',1,'1',80,20,'sin_token');

/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_marca` int(10) DEFAULT NULL,
  `id_talla` int(10) DEFAULT NULL,
  `id_color` int(10) DEFAULT NULL,
  `id_padre` int(10) DEFAULT NULL,
  `id_proveedor` int(10) DEFAULT NULL,
  `id_caja` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `cantidad` int(10) DEFAULT NULL,
  `codigo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` float(11,2) DEFAULT NULL,
  `liquidacion` float(11,2) DEFAULT '0.00',
  `fecha` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  `precio_neto` float(11,2) DEFAULT NULL,
  `porcentaje` int(10) DEFAULT NULL,
  `precio_original` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;

INSERT INTO `producto` (`id`, `id_empresa`, `id_marca`, `id_talla`, `id_color`, `id_padre`, `id_proveedor`, `id_caja`, `nombre`, `img`, `descripcion`, `cantidad`, `codigo`, `precio`, `liquidacion`, `fecha`, `estado`, `token`, `precio_neto`, `porcentaje`, `precio_original`)
VALUES
	(1,1,5,0,2,0,4,17,'Chapulines Rojos','../img/no_image.png','QA',99,'RED-CHAP-29903',50.00,0.00,'13-10-2018',1,'sin_token',100.00,35,65.00),
	(2,1,5,0,3,0,4,17,'Chapulines Blancos','../img/no_image.png','QA',94,'WHITE-CHAP-203948',40.00,0.00,'13-10-2018',1,'sin_token',100.00,35,65.00),
	(3,1,3,0,1,0,3,17,'Producto Nuevo','../img/no_image.png','Descripción',80,'NEW-PRODUCT',95.00,95.00,'10-12-2018',1,'sin_token',100.00,35,65.00);

/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table proveedor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;

INSERT INTO `proveedor` (`id`, `id_empresa`, `nombre`, `apellido`, `img`, `direccion`, `telefono`, `nit`, `estado`)
VALUES
	(1,1,'p','p','../img/K5nJl8cIaSnK.jpeg','p','343442','32313313',0),
	(2,1,'prueba','p','../img/NZN3AgACPJAl.jpeg','p','3333','333',0),
	(3,1,'CPC ARDOR COLLECTION','CPC','../img/mk5S2hml7td5.png','10175 Rush St, South El Monte, CA 91733, EE. UU','16264480000',' 900345655.',1),
	(4,1,'Generic','Provider','../img/3cOpCRSSP7C.png','Not Found','89389328','2893832',1);

/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;

INSERT INTO `rol` (`id`, `nombre`, `estado`)
VALUES
	(1,'Administrador',1),
	(2,'Vendedor',0),
	(3,'Vendedor',1);

/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ruta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ruta`;

CREATE TABLE `ruta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `ruta` WRITE;
/*!40000 ALTER TABLE `ruta` DISABLE KEYS */;

INSERT INTO `ruta` (`id`, `nombre`, `id_empresa`, `estado`)
VALUES
	(1,'Nor-Oriente',1,1),
	(2,'Metropolitana',1,1),
	(3,'Occidente',1,1),
	(4,'Periferica',1,1),
	(5,'Generic Route',1,1);

/*!40000 ALTER TABLE `ruta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table talla
# ------------------------------------------------------------

DROP TABLE IF EXISTS `talla`;

CREATE TABLE `talla` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `talla` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `talla` WRITE;
/*!40000 ALTER TABLE `talla` DISABLE KEYS */;

INSERT INTO `talla` (`id`, `id_empresa`, `talla`, `estado`)
VALUES
	(1,1,'23',0),
	(2,1,'5',1),
	(3,1,'6',1),
	(4,1,'6.5',1),
	(5,1,'7',1),
	(6,1,'7.5',1),
	(7,1,'8',1),
	(8,1,'8.5',1),
	(9,1,'9',1),
	(10,1,'9.5',1),
	(11,1,'10',0),
	(12,1,'10',0),
	(13,1,'10',0),
	(14,1,'10',1),
	(15,1,'10',0),
	(16,1,'10',0),
	(17,1,'10',0),
	(18,1,'10',0),
	(19,1,'11',1),
	(20,1,'12',1),
	(21,1,'13',1),
	(22,1,'13',0),
	(23,1,'1',1),
	(24,1,'2',1),
	(25,1,'3',1),
	(26,1,'4',1),
	(27,1,'4.5',1),
	(28,1,'5.5',1),
	(29,1,'massive_upload ',1),
	(30,1,'For Everybody',1);

/*!40000 ALTER TABLE `talla` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tipo_cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_cliente`;

CREATE TABLE `tipo_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tipo_cliente` WRITE;
/*!40000 ALTER TABLE `tipo_cliente` DISABLE KEYS */;

INSERT INTO `tipo_cliente` (`id`, `id_empresa`, `tipo`, `descripcion`, `estado`)
VALUES
	(1,1,'A','BUENO',0),
	(2,1,'A','Bueno',1),
	(3,1,'B','Intermedio',1),
	(4,1,'C','Regular',1),
	(5,1,'D','Migración',1),
	(6,1,'AA','Excelente',1);

/*!40000 ALTER TABLE `tipo_cliente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(10) DEFAULT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;

INSERT INTO `usuario` (`id`, `id_rol`, `id_empresa`, `nombre`, `apellido`, `img`, `correo`, `clave`, `telefono`, `direccion`, `nit`, `estado`)
VALUES
	(1,1,1,'MEGA','SHOES','../img/m8j7WsqxaAbT.png','megashoesg@gmail.com','junio2015shoes','31906644','GUATEMALA CITY','1756832-3',1),
	(8,1,1,'Kenny','Chavez','../img/ent1jL5dxvG7.png','kennychaveez@gmail.com','12345','58160858','Lote 9 manzana c sector 7 prados de villa hermosa','35533234',1),
	(9,1,1,'Jessica','Estrada','../img/khn5F0dT0JVA.png','jestrada@gmail.com','1234','12345678','Monserrath','12345',1),
	(10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL);

/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
