<?php
  include '../includes/config.php';
?>
<div class="detail_product col-md-12">
  <div class="form-group col-md-4">
    <label>Tipo de caja</label>
    <select class="form-control" name="caja_id[]" required="true">
      <option value="">- Seleccione un tipo de caja -</option>
      <?php
        $query_box = "SELECT * FROM caja WHERE estado=1 AND id_empresa=".$empresaid;
        $boxes = $db->getData($query_box);

        if($boxes){
          foreach ($boxes as $box) { ?>
            <option value="<?=$box['id']?>"><?=$box['nombre']?></option>
      <?php
          }
        }else{ ?>
            <option value="">No hay cajas disponibles. Por favor agregue antes de continuar</option>
      <?php
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label>Color</label>
    <select class="form-control" name="color_id[]" required="true">
      <?php
        $query_color = "SELECT * FROM color WHERE estado=1 AND id_empresa=".$empresaid;
        $colors = $db->getData($query_color);

        if($colors){
          foreach ($colors as $color) { ?>
            <option value="<?=$color['id']?>"><?=$color['nombre']?></option>
      <?php
          }
        }else{ ?>
            <option value="">No hay colores disponibles. Por favor agregue antes de continuar</option>
      <?php
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label>Cantidad (Cajas)</label>
    <input type="number" class="form-control" required="true" name="cantidad[]" placeholder="Cantidad">
  </div>
  <div class="form-group col-md-2">
    <button type="button" class="btn btn-success" onclick="addItem()" ><i class="fa fa-plus"></i></button>
    <!-- <button type="button" class="btn btn-danger" onclick="deleteItem()" ><i class="fa fa-minus"></i></button> -->
  </div>
</div>