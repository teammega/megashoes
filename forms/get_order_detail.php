<?php
    include '../includes/config.php';

    $order_id = $_POST['order_id'];

    $query_order = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id=".$order_id;
    $order = $db->getData($query_order)[0];

    $query_payments = "SELECT * FROM formas_pago WHERE id_pedido = ".$order_id;
    $payments = $db->getData($query_payments);

    $paid = getPartialPayment($payments);
    $order['total_pagado'] = $paid;
    $order['total_restante'] = $order['total'] - $paid;
    
    echo json_encode($order, true);

    function getPartialPayment($payments){
        $pay = 0;

        if(count($payments) > 0){
            foreach($payments as $payment){
                if($payment['tipo_pago']!='cheques'){
                    $pay = $pay + $payment['monto'];
                }
            }
        }

        return $pay;
    }