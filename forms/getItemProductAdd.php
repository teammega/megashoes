<?php
  include '../includes/config.php';
?>
<div class="col-md-12">
  <div class="form-group col-md-3">
    <label>Color</label>
    <select class="form-control" name="color_id[]" required="true">
      <?php
        $query_color = "SELECT * FROM color WHERE estado=1 AND id_empresa=".$empresaid;
        $colores = $db->getData($query_color);

        if($colores){
          foreach ($colores as $color) { ?>
            <option value="<?=$color['id']?>"><?=$color['nombre']?></option>
      <?php
          }
        }else{ ?>
            <option value="">No hay colores disponibles. Por favor agregue antes de continuar</option>
      <?php
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label>Talla</label>
    <select class="form-control" name="talla_id[]" required="true">
      <?php
        $query_talla = "SELECT * FROM talla WHERE estado=1 AND id_empresa=".$empresaid;
        $tallas = $db->getData($query_talla);

        if($tallas){
          foreach ($tallas as $talla) { ?>
            <option value="<?=$talla['id']?>"><?=$talla['talla']?></option>
      <?php
          }
        }else{ ?>
            <option value="">No hay tallas disponibles. Por favor agregue antes de continuar</option>
      <?php
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-2">
    <label>Cantidad</label>
    <input type="number" class="form-control" required="true" name="cantidad[]" placeholder="Cantidad">
  </div>
  <div class="form-group col-md-4" style="display: none;">
    <label>Agregar Imagen</label>
    <input type="file" name="img">
  </div>
</div>