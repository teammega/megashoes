<?php
	include '../includes/config.php';
?>
<div class="col-md-12">
	<div class="form-group col-md-5">
	    <label>Tallas</label>
	    <select class="form-control" name="talla[]" required="true">
	      <?php
	        $query_tallas = "SELECT * FROM talla WHERE estado=1 AND id_empresa=".$empresaid;
	        $tallas = $db->getData($query_tallas);

	        foreach ($tallas as $talla) { ?>
	          <option value="<?=$talla['id']?>"><?=$talla['talla']?></option>
	      <?php
	        }
	      ?>
	    </select>
	</div>
	<div class="form-group col-md-5">
		<label>Cantidad</label>
		<input class="form-control" required="true" type="number" name="cantidad[]" placeholder="Cantidad">
	</div>
	<div class="form-group col-md-2">
    	<button type="button" class="btn btn-success" onclick="addItem()" ><i class="fa fa-plus"></i></button>
    </div>
</div>