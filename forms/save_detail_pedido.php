<?php
	include '../includes/config.php';

	$data_envio = $_SESSION['data_envio'];

	$db->makeQuery($_SESSION['QUERY_create_sale']);

	$query_last = "SELECT * FROM pedido WHERE estado=1 AND id_empresa=".$empresaid." ORDER BY id DESC LIMIT 1";
	$last = $db->getData($query_last)[0];

	$last_id = $last['id'];

	/**
		Actualiza total del pedido
	*/
	$query_update_total = "UPDATE pedido SET total=".$_SESSION['subtotal_envio'].", descuento=".$_SESSION['discount']." WHERE id=".$last_id;
	$db->makeQuery($query_update_total);

	/**
		Trae el último cliente y actualiza límite de crédito
	*/
	$query_client = "SELECT * FROM cliente WHERE id=".$last['id_cliente'];
	$client = $db->getData($query_client)[0];

	$new_limit = $client['saldo_disponible'] - $_SESSION['subtotal_envio'];
	$query_update_client = "UPDATE cliente SET saldo_disponible=".$new_limit." WHERE id=".$client['id'];
	$db->makeQuery($query_update_client);

	foreach ($data_envio as $data) {
		$query_add = "INSERT INTO detalle_pedido (id_empresa, id_pedido, id_producto, cantidad, estado) VALUES (".$empresaid.", ".$last_id.", ".$data['id'].", ".$data['cantidad'].", 1)";
		$db->makeQuery($query_add);

		/**
			Actualiza el stock del producto
		*/
		$query_product = "SELECT * FROM producto WHERE id=".$data['id'];
		$product = $db->getData($query_product)[0];

		$new_stock = $product['cantidad'] - $data['cantidad'];

		$query_update_stock = "UPDATE producto SET cantidad=".$new_stock." WHERE id=".$data['id'];
		$db->makeQuery($query_update_stock);
	}