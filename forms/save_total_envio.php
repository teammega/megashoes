<?php
	include '../includes/config.php';

	$id = $_POST['id'];
	$subtotal = $_POST['subtotal'];
	$flete = $_POST['flete'];
	$cantidad = $_POST['cantidad'];
	$_SESSION['subtotal_envio'] = 0;
	$discount = $_POST['descuento'];
	$_SESSION['discount'] = $discount;

	$new_element = [
		'id' => $id,
		'subtotal' => $subtotal,
		'cantidad' => $cantidad,
	];

	$_SESSION['data_envio'][$id] = $new_element;

	foreach ($_SESSION['data_envio'] as $sub) {
		$_SESSION['subtotal_envio'] = $_SESSION['subtotal_envio'] + $sub['subtotal'];
	}

	$total_discount = (($_SESSION['subtotal_envio']) * ($discount / 100));

	$_SESSION['subtotal_envio'] = ($_SESSION['subtotal_envio'] + $flete) - $total_discount;

	$new_array = [
		'subtotal' => $_SESSION['subtotal_envio'],
		'limite' => $_SESSION['client_limit'],
		'total_discount' => $total_discount
	];

	echo json_encode($new_array);
