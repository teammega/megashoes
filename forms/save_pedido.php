<?php
	include '../includes/config.php';

	$user_id = $_POST['user_id'];
	$cliente_id = $_POST['cliente_id'];
	$ruta_id = $_POST['ruta_id'];
	$direccion = $_POST['direccion'];
	$observaciones = $_POST['observaciones'];
	$producto_id = $_POST['producto_id'];
	$today = date('Y-m-d');
	$fecha_entrega = $_POST['fecha_entrega'];
	$flete = $_POST['flete'];
	$porcentaje_factura = $_POST['porcentaje_factura'] == "" ? 0 : $_POST['porcentaje_factura'];
	$porcentaje_pedido = $_POST['porcentaje_envio'] == "" ? 100 : $_POST['porcentaje_envio'];

	$factura = 0;

	if($porcentaje_pedido < 100){
		$factura = true;
	}
	
	$no_factura = $_POST['no_factura'];

	$entrega = explode("/", $fecha_entrega);
	$new_format_day = $entrega[2]."-".$entrega[0]."-".$entrega[1];
	$new_id = date('d').date('m').date('Y');

	$add_pedido = "INSERT INTO pedido (id_empresa, no_factura, id_usuario, id_cliente, fecha, fecha_entrega, observaciones, 
	total, id_ruta, direccion, flete, descuento, no_envio, estado, factura, porcentaje_pedido, porcentaje_factura) 
	VALUES 
	(".$empresaid.", '".$no_factura."', ".$user_id.", ".$cliente_id.", '".$today."', '".$new_format_day."', 
	'".$observaciones."', 0, ".$ruta_id.", '".$direccion."', ".$flete.", 0, '".$no_factura."', 1, ".$factura.", ".$porcentaje_pedido.", 
	".$porcentaje_factura.")";
	$_SESSION['QUERY_create_sale'] = $add_pedido;
?>

	<table class="table ">
		<thead>
			<tr>
				<th>Código</th>
				<th>Producto</th>
				<th>Precio</th>
				<th>Cantidad</th>
				<th>Subtotal</th>
			</tr>
		</thead>
		<tbody>
		<?php
			for($i = 0; $i < count($producto_id); $i++){
				$query_product = "SELECT * FROM producto WHERE id_empresa=".$empresaid." AND id=".$producto_id[$i];
				$product = $db->getData($query_product)[0];
		?>
				<tr product_id="<?=$product['id']?>">
					<td><?=$product['codigo']?></td>
					<td><?=$product['nombre']?></td>
					<?php
						$precio = $product['precio'];
						if($product['liquidacion'] > 0){
							$precio = $product['precio'] - ($product['precio'] * (($product['liquidacion'] / 100)));
						}
					?>
					<td>
						<select name="precio" class="form-control precio-factura-<?=$product['id']?>">
							<option value="<?=$product['precio']?>">Q. <?=number_format($product['precio'], 2) ?></option>
							<option value="<?=$product['precio_neto']?>">Q. <?=number_format($product['precio_neto'], 2) ?></option>
						</select>
					</td>
					<td>
						<?php
							$query_box = "SELECT * FROM caja WHERE id=".$product['id_caja'];
							$box = $db->getData($query_box)[0];
						?>
						<input type="hidden" class="cant_box-<?=$product['id']?>" value="<?=$box['total']?>">
						<label><strong>Existencias: </strong> <?=$product['cantidad']?></label>
						<br>
						<input type="number" name="cantidad" onchange="calculateTotal(<?=$product['id']?>, this.value, <?=$flete?>)" min="0" max="<?=$product['cantidad']?>" >
					</td>
					<td><label class="subtotal-<?=$product['id']?>">Q 0.00</label> </td>
				</tr>
		<?php
			}
		?>
			<tr>
				<td colspan="4"><h4>Flete</h4></td>
				<td>
					<h4 class="flete">Q <?=number_format($flete, 2) ?></h4>
				</td>
			</tr>
			<tr>
				<td colspan="4"><h3>Total</h3></td>
				<td>
					<h3 class="total">Q 0.00</h3>
				</td>
			</tr>
		</tbody>
	</table>
	