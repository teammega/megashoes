<?php
	include '../includes/config.php';

	$no_doc_id = $_POST['no_doc_id'] == "" ? 0: $_POST['no_doc_id'];
	$monto = $_POST['monto'];
	// $monto = str_replace(',', '', $monto);
	// $monto = (int)$monto;
	$banco = $_POST['banco'];
	$pedido = $_POST['pedido'];
	$fecha_deposito = isset($_POST['fecha_deposito']) ? $_POST['fecha_deposito'] : "";
	$fecha_recibo = isset($_POST['fecha_recibo']) ? $_POST['fecha_recibo'] : "";
	$lugar_ingreso = !isset($_POST['lugar_ingreso']) ? $_POST['lugar_ingreso'] : "Original/Oficina";
	$descripcion = $_POST['descripcion'];
	$tipo = $_POST['tipo_pago'];

	$query_order = "SELECT * FROM pedido WHERE id=".$pedido;
	$order = $db->getData($query_order)[0];

	$no_recibo = $order['no_factura'];

	// Query que guarda el pago
	$query_pay = "INSERT INTO formas_pago (no_id_doc, no_recibo, monto, banco, id_pedido, fecha, fecha_deposito, fecha_recibo, lugar_ingreso, 
	no_doc, descripcion, tipo_pago, id_empresa, estado) VALUES ($no_doc_id, '$no_recibo', '$monto', '$banco', $pedido, '".date('Y-m-d')."', '$fecha_deposito',
	'$fecha_recibo', '$lugar_ingreso', '$no_recibo', '$descripcion', '$tipo', $empresaid, 1)";
	$db->makeQuery($query_pay);

	// Proceso para subir el saldo disponible al cliente
	$query_client = "SELECT * FROM cliente WHERE id=".$order['id_cliente'];
	$client = $db->getData($query_client)[0];
	$new_saldo = $client['saldo_disponible'] + $monto;
	$query_update_client = "UPDATE cliente SET saldo_disponible='$new_saldo' WHERE id=".$order['id_cliente'];
	$db->makeQuery($query_update_client);

	// Trae todos los pagos realizados a un pedido
	$query_payments = "SELECT * FROM formas_pago WHERE id_pedido = ".$pedido;
	$payments = $db->getData($query_payments);
	$paid = getPartialPayment($payments);
	$paid = (double)$paid;
	$total_order = (double)$order['total'];

	if($paid === $total_order && strtolower($tipo) != "cheques"){
		$query_update_order = "UPDATE pedido SET estado=2 WHERE id=".$pedido;
		$db->makeQuery($query_update_order);
	}

	function getPartialPayment($payments){
        $pay = 0;

        if(count($payments) > 0){
            foreach($payments as $payment){
                $pay = $pay + $payment['monto'];
            }
        }

        return $pay;
    }

	header('Location: ../pages/payments_methods.php?type='.$tipo);
