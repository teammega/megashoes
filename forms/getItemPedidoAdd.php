<?php
  include '../includes/config.php';
?>
<div class="product-container col-md-12">
  <div class="form-group col-md-5">
    <label>Producto</label>
    <select class="form-control select_product" name="producto_id[]" required="true" onChange="setLimit()">
      <?php
        $query_product = "SELECT * FROM producto WHERE estado = 1 AND id_empresa=".$empresaid;
        $products = $db->getData($query_product);

        foreach ($products as $product) { ?>
          <option value="<?=$product['id']?>"><?=$product['nombre']?></option>
      <?php
        }
      ?>
    </select>
  </div>
  <div class="form-group col-md-5">
    <label>Cantidad <strong class="cantidad_text"></strong> </label>
    <input type="number" class="form-control input_cantidad" required="true" name="cantidad[]" placeholder="Cantidad">
  </div>
  <div class="col-md-2">
    <button type="button" class="btn btn-success" onclick="add_item()"><i class="fa fa-plus"></i></button>
  </div>
</div>