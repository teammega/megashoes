<div class="col-md-12">
    <div class="form-group col-md-6" style="<?php echo $efectivoStyle; ?>">
        <label>No. de boleta</label>
        <input type="number" name="no_doc_id" class="form-control" placeholder="No Doc ID">
    </div>
    <input type="hidden" name="tipo_pago" value="<?=$_GET['type']?>" style="display:none; <?php echo $efectivoStyle; ?>" >

    <div class="form-group col-md-6" style="<?php echo $efectivoStyle?>">
        <label>Banco </label>
        <select class="form-control" name="banco" >
            <option value="G&T">G&T</option>
            <option value="BI">BI</option>
            <option value="Promerica">Promerica</option>
            <option value="Banrural">Banrural</option>
            <option value="Banco antigua">Banco antigua</option>
            <option value="BAC">BAC</option>
        </select>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="form-group col-md-6">
        <label>Fecha de Depósito </label>
        <input type="text" id="datepicker" class="form-control" name="fecha_deposito" autocomplete="off">
    </div>
    <div class="form-group col-md-6 fecha_recibo">
        <label>Fecha de recibo</label>
        <input type="text" id="datepicker2" class="form-control" name="fecha_recibo" autocomplete="off">
    </div>

    <div class="form-group col-md-6">
    <label>Entregado a: </label>
        <select name="lugar_ingreso" class="form-control" required="true">
            <option value="Original/Oficina">Original/Oficina</option>
            <option value="Visto en Linea">Visto en Linea</option>
            <option value="Pendiente">Pendiente</option>
        </select>
    </div>
    <div class="form-group col-md-6" style="display:none;<?php echo $efectivoStyle; ?>">
        <label>Estado</label>
        <select class="form-control state_control" name="estado" >
            <option value="1">No cambiado</option>
            <option value="2">Cambiado</option>
            <option value="3">Rechazado</option>
        </select>
    </div>
</div>