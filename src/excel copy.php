<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle("ReporteSaldos")
->setSubject("ReporteSaldos")
->setDescription("ReporteSaldos")
->setKeywords("Excel Office 2007 openxml php")
->setCategory("ReporteSaldos");
	
	/* Borders */
	$styleTitles = array(
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	    )
	);

	$styleFill = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "F4C724"
        )
    );

    $BStyle = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);

	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('test_img');
	$objDrawing->setDescription('test_img');
	$objDrawing->setPath('../img/logo_megashoes.png');
	$objDrawing->setCoordinates('A1');
	//setOffsetX works properly
	// $objDrawing->setOffsetX(5);
	// $objDrawing->setOffsetY(5);
	//set width, height
	$objDrawing->setWidth(40);
	$objDrawing->setHeight(80);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

	$sheet = $objPHPExcel->getActiveSheet();
	$setSheet = $objPHPExcel->setActiveSheetIndex(0);

	// Agregar Informacion
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'MEGASHOES');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C3', 'GUATEMALA');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C4', 'REPORTE DE SALDOS PENDIENTES');
	$sheet->getStyle("C4")->getFont()->setBold(true);
	$sheet->getStyle("C2:C4")->applyFromArray($styleTitles);

	$count = 6;

	$query_rol = "SELECT * FROM rol WHERE estado=1";
	$roles = $db->getData($query_rol);

	foreach ($roles as $rol) {
		if(trim(strtolower($rol['nombre'])) == "administrador" || trim(strtolower($rol['nombre'])) == "vendedor"){
			$query_part_access_user = $_SESSION['tipo_usuario'] == 1 ? "" : " AND id=".$_SESSION['id_usuario'];
			$query_vendedor = "SELECT * FROM usuario WHERE id_empresa=".$empresaid." AND id_rol=".$rol['id'].$query_part_access_user;
			$vendedores = $db->getData($query_vendedor);

			foreach ($vendedores as $vendedor) {
				$query_pedido = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id_usuario=".$vendedor['id']." AND estado=1 AND total!=0";
				$pedidos = $db->getData($query_pedido);
				$count_pedidos = count($pedidos);

				if($count_pedidos > 0){
					$setSheet->setCellValue("C".$count, "VENDEDOR: ".$vendedor['nombre']." ".$vendedor['apellido']);
					$sheet->getStyle("C".$count)->getFill()->applyFromArray($styleFill);

					$count = $count + 1;
					$count2 = $count;
					$sheet->getStyle("A".$count.":E".$count)->getFill()->applyFromArray($styleFill);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$count.':E'.$count)->applyFromArray($BStyle);

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count, 'Cliente');
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$count, 'Documento Factura');
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$count, 'Valor (Q)');
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$count, 'Saldo Factura');
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$count, 'Observaciones');

					foreach ($pedidos as $pedido) {
						$query_cliente = "SELECT * FROM cliente WHERE id_empresa=".$empresaid." AND id=".$pedido['id_cliente'];
						$cliente = $db->getData($query_cliente)[0];

						$monto = $pedido['total'] + $pedido['flete'];

						$count2 += 1;
						$setSheet->setCellValue('A'.$count2, $cliente['nombre']." ".$cliente['apellido']);
						$setSheet->setCellValue('B'.$count2, $pedido['id']);				
						$setSheet->setCellValue('C'.$count2, $monto);
						$setSheet->setCellValue('D'.$count2, "Q ".number_format($pedido['total'], 2));
						$setSheet->setCellValue('E'.$count2, $pedido['observaciones']);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$count2.':E'.$count2)->applyFromArray($BStyle);
					}

					$count = $count2 + 2;
				}
			}
		}
	}	


	$objPHPExcel->getActiveSheet(0)->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("G")->setAutoSize(true);
 

// Renombrar Hoja

$objPHPExcel->getActiveSheet()->setTitle('Reporte de Saldos');

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.

$objPHPExcel->setActiveSheetIndex(0);

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.ms-excel');
$filename = "Reports - Saldos".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>