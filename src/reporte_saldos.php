<?php

/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

/** Config */
$clients = array('cliente1');

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

$fecha_actualizacion = 'Sin Fecha';

// Establecer propiedades
	/* Borders */
	$styleTitles = array(
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	    )
	);

	$styleFill = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "ECFF41"
        )
	);
	
	$styleNameClient = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "B5B7B7"
        )
	);
	
	$styleBadClient = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "FA3F6A"
        )
    );

    $BStyle = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);

	$sheet = $objPHPExcel->getActiveSheet();

	$sheet_count = 0;
	//Start adding next sheets
	$query_user = "SELECT *  FROM usuario WHERE estado=1 AND id_empresa=".$empresaid;
	$users = $db->getData($query_user);

	foreach ($users as $user) {
		$count = 6;
		$objWorkSheet = $objPHPExcel->createSheet($sheet_count);

		$query_pedido = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id_usuario=".$user['id']." AND estado!=5 AND total!=0";
		$pedidos = $db->getData($query_pedido);
		$count_pedidos = count($pedidos);


		$query_ruta = "SELECT * FROM ruta WHERE id = ".$user['id_ruta'];
		$ruta = $db->getData($query_ruta)[0];
		
		if ($count_pedidos >0){
			$query_pedido_desc = "SELECT fecha FROM pedido WHERE id_empresa=".$empresaid." AND id_usuario=".$user['id']." AND estado!=5 AND total!=0 ORDER BY fecha DESC";			
			$pedido_desc = $db->getData($query_pedido_desc);			
			if (count($pedido_desc)>0){
				$fecha_actualizacion = $pedido_desc[0]['fecha'];
			}else{
				$fecha_actualizacion = 'Sin Fecha';
			}
		}
		

		

		$objWorkSheet->setCellValue("A1", "VENDEDOR:");

		$name=utf8_encode($user['nombre']);
		$last_name=utf8_encode($user['apellido']);

		$objWorkSheet->setCellValue("B1", "".$user['nombre']." ".$user['apellido']);

		$objWorkSheet->setCellValue("A2", "RUTA:");
		$objWorkSheet->setCellValue("B2", $ruta['nombre']);

		$objWorkSheet->setCellValue("A3", "FECHA DE ACTUALIZACIÓN:");
		$objWorkSheet->setCellValue("B3", $fecha_actualizacion);


		$objWorkSheet->setCellValue("A5", "REPORTE DE SALDOS PENDIENTES POR CLIENTE");

		

		$from = "A1";
		$to = "A5";
		
		$objWorkSheet->getStyle("$from:$to")->getFont()->setBold( true );

		

		if($count_pedidos > 0){
			

			
			$count = $count + 1;
			$count2 = $count;
			
			$objWorkSheet->getStyle('A'.$count.':G'.$count)->applyFromArray($BStyle);

			$objWorkSheet->setCellValue('A'.$count, 'Cliente');
			$objWorkSheet->setCellValue('B'.$count, 'Documento / Factura');
			$objWorkSheet->setCellValue('C'.$count, 'Valor (Q)');
			$objWorkSheet->setCellValue('D'.$count, 'Saldo Factura');
			$objWorkSheet->setCellValue('E'.$count, 'Envío (%)');			

			$objWorkSheet->setCellValue('F'.$count, 'Observaciones');
			$objWorkSheet->setCellValue('G'.$count, 'Días de crédito');

			$from = "A".$count;
			$to = "G".$count;
			$objWorkSheet->getStyle("$from:$to")->getFont()->setBold( true );

			foreach ($pedidos as $pedido) {
				$query_cliente = "SELECT * FROM cliente WHERE id_empresa=".$empresaid." AND id=".$pedido['id_cliente'];
				$cliente = $db->getData($query_cliente)[0];

				

				$query_ruta = "SELECT * FROM ruta WHERE id = ".$cliente['id_ruta'];
				$ruta = $db->getData($query_ruta)[0];

				$query_tipo = "SELECT * FROM tipo_cliente WHERE id = ".$cliente['tipo'];
				$client_type = $db->getData($query_tipo)[0];
				
				$name=utf8_encode($cliente['nombre']);
				$last_name=utf8_encode($cliente['apellido']);

				$full_name = "".$name."-".$last_name;

				if (in_array($full_name, $clients)) {
					$filter_client = True;
				}else{
					$filter_client = False;
					array_push($clients,$full_name);
				}

				$monto_envio = $pedido['total'] - ($pedido['total'] * ($pedido['porcentaje_pedido'] / 100));
				$monto_factura = $pedido['total'] - ($pedido['total'] * ($pedido['porcentaje_factura'] / 100));

				$count2 += 2;
				$count3 = $count2 + 1;

				if ($filter_client == False){
					
					$objWorkSheet->setCellValue('A'.($count2-1), "Loc: ".$ruta['nombre']);
					$objWorkSheet->getStyle("A".($count2-1))->getFill()->applyFromArray($styleFill);

					

					$objWorkSheet->setCellValue('A'.$count2, "#".$cliente['id']." - ".$name." ".$last_name);
					$objWorkSheet->getStyle("A".$count2)->getFill()->applyFromArray($styleNameClient);
					
					

					if ($client_type['tipo']!='F'){
						$objWorkSheet->setCellValue('B'.$count2, " Tipo de Cliente: ".$client_type['tipo']." - Q ".number_format($cliente['saldo_disponible']),2);
					
					}else{
						$objWorkSheet->setCellValue('B'.$count2, " Tipo de Cliente: ".$client_type['tipo']);
					
					}

					
				}
				
				$objWorkSheet->setCellValue('A'.$count3, $pedido['fecha']);				
				$objWorkSheet->setCellValue('C'.$count3, $pedido['total']);
				$objWorkSheet->setCellValue('D'.$count3, "Q ".number_format($pedido['total'], 2));
				$objWorkSheet->setCellValue('E'.$count3, $pedido['porcentaje_pedido']);
				
								
				$observations=utf8_encode($pedido['observaciones']);
				$objWorkSheet->setCellValue('F'.$count3, $observations);

				if ($filter_client == False){
					$objWorkSheet->setCellValue('G'.$count2, $cliente['dias_credito']);					
				}

				
			}
			
			$clients = array('cliente1');
			$count = $count2 + 2;
		}

		$objWorkSheet->getColumnDimension("A")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("B")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("C")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("D")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("E")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("F")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("G")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("H")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("I")->setAutoSize(true);

		$sheet_count = $sheet_count + 1;
		$objWorkSheet->setTitle($user['nombre']." ".$user['apellido']);
		
	}
	


// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.


header('Content-Type: application/vnd.ms-excel');
$filename = "Reports - Saldos".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>