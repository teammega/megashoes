<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle("Inventario")
->setSubject("Inventario")
->setDescription("Inventario")
->setKeywords("Excel Office 2007 openxml php")
->setCategory("Inventario");

// Agregar Informacion

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Codigo');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Fecha de Creación');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Nombre producto');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Corrida');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Opcion');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Precio Unitario');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'Cajas');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'Pares');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', 'Subtotal');

	$count_prod = 1; //contador de prodcuto
	$query_product = "SELECT * FROM producto WHERE id_empresa = $empresaid AND estado = 1"; //consulta de productos

	$totals = [
		'pairs' => 0,
		'amount' => 0,
		'boxes' => 0
	];

	if ($db->getData($query_product)) { // si funciona la consulta
		$products = $db->getData($query_product); //se almacenan los productos el array
		$count_producs = count($products); //conteo segun el array
		if ($count_producs > 0) {// si hay mas de un producto
			$subtotal = 0;

			foreach ($products as $product) { //recorrido del leng del arreglo
				
				/*CONSULTAS EXTERNAS*/
					/*CAJAS*/
					$query_box = "SELECT * FROM detalle_caja_producto WHERE estado = 1 AND id_producto = ".$product['id'];
					$boxes = $db->getData($query_box);
					$count_boxes_detail = count($boxes);
					if ($count_boxes_detail>0) {
						$total_boxes = $count_boxes_detail;
						$peers_count = 0;
						$list_box= "";
						$list_box2= "";

						foreach ($boxes as $box) {
							$query_total_box = "SELECT * FROM caja WHERE estado = 1 AND id = ".$box['id_caja'];
							$boxes_total = $db->getData($query_total_box)[0];
							$count_boxes_total = count($boxes_total);
							if ($count_boxes_total > 0 ) {
								$peers_count = $peers_count + $boxes_total['total'];		
								$name = $boxes_total['nombre'];
								$explode_name = explode("(",$name);
								$list_box = $list_box."".$explode_name[0];

								$name2 = $boxes_total['nombre'];
								$explode_name2 = explode(" ",$name);
								$list_box2 = $list_box2." ".$explode_name2[1];

							}
						}
						$peers = 0;
						$peers = $product['cantidad'] * $peers_count;
						$subtotal = $peers * $product['precio'];
						
						// Fill all Totals
						$totals['boxes'] = $totals['boxes'] + $product['cantidad'];
						$totals['pairs'] = $totals['pairs'] + $peers;
						$totals['amount'] = $totals['amount'] + $subtotal;
					}else{
						$total_boxes = "No tiene cajas";
					}
					/*CAJAS*/

				/*CONSULTAS EXTERNAS*/

				$count_prod += 1;  // le sumo uno al contador
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count_prod, $product['codigo']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$count_prod, $product['fecha']);				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$count_prod, $product['nombre']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$count_prod, $list_box);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$count_prod, $list_box2);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$count_prod, "Q. ".number_format($product['precio'], 2));
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$count_prod, $product['cantidad']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$count_prod, $peers);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$count_prod, "Q. ".number_format($subtotal, 2));
			}
		}
	}

	$count_prod = $count_prod + 2;
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$count_prod, $totals['boxes']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$count_prod, $totals['pairs']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$count_prod, "Q. ".number_format($totals['amount'], 2));


	$objPHPExcel->getActiveSheet(0)->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("G")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("H")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("I")->setAutoSize(true);
 

// Renombrar Hoja

$objPHPExcel->getActiveSheet()->setTitle('Inventario');
$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.

$objPHPExcel->setActiveSheetIndex(0);

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.ms-excel');
$filename = "Inventory - ".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>