<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle(ucfirst("Liquidacion"))
->setSubject(ucfirst("Liquidacion"))
->setDescription("Liquidacion")
->setKeywords("Excel Office 2007 openxml php")
->setCategory(ucfirst("Liquidacion"));

	/* Borders */
	$styleTitles = array(
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	    )
	);

	$styleFill = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "F4C724"
        )
    );

    $BStyle = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);

	$sheet = $objPHPExcel->getActiveSheet();
	$setSheet = $objPHPExcel->setActiveSheetIndex(0);

// Agregar Informacion
	$setCellValueExcel = $objPHPExcel->setActiveSheetIndex(0);

	$setCellValueExcel->setCellValue('A3', 'Fecha Despacho');
	$setCellValueExcel->setCellValue('B3', 'No. Envío');
	$setCellValueExcel->setCellValue('C3', 'Nombre del Cliente');
	$setCellValueExcel->setCellValue('D3', 'Monto Envío');
	$setCellValueExcel->setCellValue('E3', 'Envío menos descuento');
	$setCellValueExcel->setCellValue('F3', 'Días crédito');
	$setCellValueExcel->setCellValue('G3', 'Efectivo / Boleta');
	$setCellValueExcel->setCellValue('H3', 'Cheques Liq. 1');
	$setCellValueExcel->setCellValue('I3', 'Inicio Mes');
	$setCellValueExcel->setCellValue('J3', 'Descuentos');
	$setCellValueExcel->setCellValue('K3', 'Total Abonado');
	$setCellValueExcel->setCellValue('L3', 'Saldo');
	$setCellValueExcel->setCellValue('M3', 'Cantidad CHQ');
	$setCellValueExcel->setCellValue('N3', 'No. Recibo');
	$setCellValueExcel->setCellValue('O3', 'Fecha Ingreso');

	$sheet->getStyle("A3:H3")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A3:O3')->applyFromArray($BStyle);
	$count_line = 4;

	$query_payment_methods = "SELECT * FROM formas_pago WHERE id_empresa=".$empresaid;
	$payment_methods = $db->getData($query_payment_methods);

	$total_monto_envio = 0;
	$total_envio_descuento = 0;
	$total_efectivo_boleta = 0;
	$total_cheques = 0;
	$total_descuentos = 0;
	$total_monto = 0;
	$total_saldos = 0;

	foreach ($payment_methods as $method) {
		$query_pedido = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id=".$method['id_pedido']." AND estado!=5 AND total!=0";
		$pedido = $db->getData($query_pedido)[0];

		$query_client = "SELECT * FROM cliente WHERE estado=1 AND id_empresa=".$empresaid." AND id=".$pedido['id_cliente'];
		$client = $db->getData($query_client)[0];

		$client_name = $client['nombre']." ".$client['apellido'];
		$discount = $pedido['total'] - ($pedido['total'] * ($pedido['descuento'] / 100));

		$boleta_efectivo = $method['tipo_pago'] != "cheque" ? $method['monto'] : 0;
		$cheque_liquidacion = $method['tipo_pago'] == "cheque" ? $method['monto'] : 0;
		$descuento = $pedido['total'] * ($pedido['descuento'] / 100);

		$total_monto_envio = $total_monto_envio + $pedido['total'];
		$total_envio_descuento = $total_monto_envio + $discount;
		$total_efectivo_boleta = $total_monto_envio + $boleta_efectivo;
		$total_cheques = $total_monto_envio + $cheque_liquidacion;
		$total_descuentos = $total_descuentos + $descuento;
		$total_monto = $total_monto + $method['monto'];
		$total_saldos = $total_saldos + $client['saldo_disponible'];

		$setCellValueExcel->setCellValue('A'.$count_line, $method['fecha_cambio']);
		$setCellValueExcel->setCellValue('B'.$count_line, $pedido['no_envio']);
		$setCellValueExcel->setCellValue('C'.$count_line, $client_name);
		$setCellValueExcel->setCellValue('D'.$count_line, "Q ".$pedido['total']);
		$setCellValueExcel->setCellValue('E'.$count_line, "Q ".$discount);
		$setCellValueExcel->setCellValue('F'.$count_line, $client['dias_credito']);
		$setCellValueExcel->setCellValue('G'.$count_line, "Q ".$boleta_efectivo);
		$setCellValueExcel->setCellValue('G'.$count_line, "Q ".$cheque_liquidacion);
		$setCellValueExcel->setCellValue('I'.$count_line, "-");
		$setCellValueExcel->setCellValue('J'.$count_line, "Q ".$descuento);
		$setCellValueExcel->setCellValue('K'.$count_line, "Q ".$method['monto']);
		$setCellValueExcel->setCellValue('L'.$count_line, "Q ".$client['saldo_disponible']);
		$setCellValueExcel->setCellValue('M'.$count_line, "1");
		$setCellValueExcel->setCellValue('N'.$count_line, $pedido['no_envio']);
		$setCellValueExcel->setCellValue('O'.$count_line, $method['fecha_ingreso']);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$count_line.':O'.$count_line)->applyFromArray($BStyle);

		$count_line += 1;
	}

	$count_line = $count_line + 2;
	
	$setCellValueExcel->setCellValue('A'.$count_line, "TOTAL");
	$setCellValueExcel->setCellValue('D'.$count_line, "Q ".number_format($total_monto_envio, 2));
	$setCellValueExcel->setCellValue('E'.$count_line, "Q ".number_format($total_envio_descuento, 2));
	$setCellValueExcel->setCellValue('G'.$count_line, "Q ".number_format($total_efectivo_boleta, 2));
	$setCellValueExcel->setCellValue('H'.$count_line, "Q ".number_format($total_cheques, 2));
	$setCellValueExcel->setCellValue('J'.$count_line, "Q ".number_format($total_descuentos, 2));
	$setCellValueExcel->setCellValue('K'.$count_line, "Q ".number_format($total_monto, 2));
	$setCellValueExcel->setCellValue('L'.$count_line, "Q ".number_format($total_saldos, 2));
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$count_line.':C'.$count_line);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$count_line.':O'.$count_line)->applyFromArray($BStyle);

	$objPHPExcel->getActiveSheet(0)->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("G")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("H")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("I")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("J")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("K")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("L")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("M")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("N")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("O")->setAutoSize(true);
 

// Renombrar Hoja

$objPHPExcel->getActiveSheet()->setTitle("Liquidacion");

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.

$objPHPExcel->setActiveSheetIndex(0);

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.ms-excel');
$filename = "Reports".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>