<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle(ucfirst($_GET['type']))
->setSubject(ucfirst($_GET['type']))
->setDescription($_GET['type'])
->setKeywords("Excel Office 2007 openxml php")
->setCategory(ucfirst($_GET['type']));

	/* Borders */
	$styleTitles = array(
	    'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	    )
	);

	$styleFill = array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
             'rgb' => "F4C724"
        )
    );

    $BStyle = array(
	  'borders' => array(
	    'allborders' => array(
	      'style' => PHPExcel_Style_Border::BORDER_THIN
	    )
	  )
	);

	$sheet = $objPHPExcel->getActiveSheet();
	$setSheet = $objPHPExcel->setActiveSheetIndex(0);

	for ($i=0; $i <3 ; $i++) {

		if ($i==0) {
			$meth = "depositos";
		}elseif ($i==1) {
			$meth = "cheques";
		}elseif ($i==2) {
			$meth = "efectivo";
		}

		$objWorkSheet = $objPHPExcel->createSheet($i);
		$objWorkSheet->setTitle(ucwords($meth));
		$setCellValueExcel = $objPHPExcel->setActiveSheetIndex($i);
		$setCellValueExcel->setCellValue('A3', 'No.');
		$setCellValueExcel->setCellValue('B3', 'Nombre de Cliente');
		$setCellValueExcel->setCellValue('C3', 'Monto');
		$setCellValueExcel->setCellValue('D3', 'Banco');
		$setCellValueExcel->setCellValue('E3', 'Fecha de Ingreso');
		$setCellValueExcel->setCellValue('F3', 'Lugar');
		$setCellValueExcel->setCellValue('G3', 'No. de Documento');
		$setCellValueExcel->setCellValue('H3', 'Fecha de Cambio');

		$sheet->getStyle("A3:H3")->getFont()->setBold(true);
		$objWorkSheet->getStyle('A3:H3')->applyFromArray($BStyle);
		$count_line = 4;
		$payment_method = $_GET['type'];
		$query_payment_methods = "SELECT * FROM formas_pago WHERE id_empresa=".$empresaid." AND tipo_pago='$meth'";
		$payment_methods = $db->getData($query_payment_methods);
		foreach ($payment_methods as $method) {
			$query_pedido = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id=".$method['id_pedido'];
			$pedido = $db->getData($query_pedido)[0];

			$query_client = "SELECT * FROM cliente WHERE estado=1 AND id_empresa=".$empresaid." AND id=".$pedido['id_cliente'];
			$client = $db->getData($query_client)[0];

			$client_name = $client['nombre']." ".$client['apellido'];

			$setCellValueExcel->setCellValue('A'.$count_line, $method['no_id_doc']);
			$setCellValueExcel->setCellValue('B'.$count_line, $client_name);
			$setCellValueExcel->setCellValue('C'.$count_line, $method['monto']);
			$setCellValueExcel->setCellValue('D'.$count_line, $method['banco']);
			$setCellValueExcel->setCellValue('E'.$count_line, $method['fecha_ingreso']);
			// $setCellValueExcel->setCellValue('F'.$count_line, "");
			$setCellValueExcel->setCellValue('F'.$count_line, $method['lugar_ingreso']);
			$setCellValueExcel->setCellValue('G'.$count_line, $method['no_doc']);
			$setCellValueExcel->setCellValue('H'.$count_line, $method['fecha_cambio']);

			$objWorkSheet->getStyle('A'.$count_line.':H'.$count_line)->applyFromArray($BStyle);

			$count_line += 1;
		}
		$objWorkSheet->getColumnDimension("A")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("B")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("C")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("D")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("E")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("F")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("G")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("H")->setAutoSize(true);
		$objWorkSheet->getColumnDimension("I")->setAutoSize(true);

	}



// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.

$objPHPExcel->setActiveSheetIndex(0);

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.ms-excel');
$filename = "Reports".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>
