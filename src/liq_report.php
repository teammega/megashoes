<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle("Liquidacione")
->setSubject("Liquidaciones")
->setDescription("Liquidaciones")
->setKeywords("Excel Office 2007 openxml php")
->setCategory("Liquidaciones");



// FORMATOS 

$BStyle = array( 'borders' => 	array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

$styleFill = array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => "8BC34A"));

// Agregar Informacion

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Fecha de despacho');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'Envio');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Nombre Cliente');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Monto Envío');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Envío menos descuento');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Días crédito');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'Efectivo / Boleta');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'Cheques Liq. 1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', 'Liq. 2');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1', 'Cheques Liq. 2');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K1', 'Inicio mes');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L1', 'Descuentos');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M1', 'Total Abonado');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N1', 'Saldo');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O1', 'Cantidad CHQ');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P1', 'NUMERO RECIBO');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q1', 'Fecha de Ingreso');
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($BStyle);
	$objPHPExcel->getActiveSheet()->getStyle('K1')->getFill()->applyFromArray($styleFill);

	$count_prod = 1; //contador de prodcuto
	$query_payments =  "SELECT * FROM formas_pago WHERE id_empresa=$empresaid ORDER BY id_pedido ASC";
	

	if ($db->getData($query_payments)) { // si funciona la consulta
		$payments = $db->getData($query_payments); //se almacenan los productos el array
		$count_payments = count($payments); //conteo segun el array
		
		// print_r($payments);
        
		if ($count_payments > 0) {// si hay mas de un producto
			$subtotal = 0;
			$pagado = 0;
			$acumulado = 0;
			foreach ($payments as $payment) { //recorrido del leng del arreglo
				
				
				/*CONSULTAS EXTERNAS*/
                    $query_order = "SELECT * FROM pedido WHERE id=".$payment['id_pedido'];
                    $order = $db->getData($query_order)[0];
                    $query_client = "SELECT * FROM cliente WHERE id=".$order['id_cliente'];
                    $client = $db->getData($query_client)[0];					
					$total = $order['total'] - (($order['total'] - $order['flete']) * ($order['descuento'] / 100));
                    
					$saldo = $order['total'];
					
					if($payment['tipo_pago']!='cheques'){
						$acumulado = $acumulado + $payment['monto'];
					}

					if($payment['tipo_pago']!='cheques'){
						$saldo = $saldo - $acumulado;
					}
					
                    

                    $total_pedido = $total * ($order['porcentaje_pedido'] / 100);
                    $total_factura = $total * ($order['porcentaje_pedido'] / 100);
				/*CONSULTAS EXTERNAS*/

				$count_prod += 1;  // le sumo uno al contador
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count_prod, $payment['fecha']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$count_prod, $order['no_envio']);				
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$count_prod, $client['nombre']. " " .$client['apellido']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$count_prod, "Q. ".number_format($order['total'] - $pagado, 2));
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$count_prod, "Q. ".number_format($total_pedido - $order['descuento'],2));
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$count_prod, $client['dias_credito']);
				
				

                $boleta = "";
                $cheque = "";

                if($payment['tipo_pago'] == "depositos"){
                    $boleta = "Q. ".$payment['monto'];
                }

                if($payment['tipo_pago'] == "cheques"){
                    $cheque = "Q. " . $payment['monto'];
				}
				
				if($payment['tipo_pago']!='cheques'){
                    $pagado = $pagado + $payment['monto'];
				}

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$count_prod, $boleta);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$count_prod, $cheque);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$count_prod, "");
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$count_prod, "");
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$count_prod, "");
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$count_prod, "");
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$count_prod, "Q. ".number_format($payment['monto'], 2));
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$count_prod, "Q. ".number_format($saldo,2));
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$count_prod, "EFTVO");
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$count_prod, $payment['no_id_doc']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$count_prod, $payment['fecha_recibo']);
                
                
				
				$objPHPExcel->getActiveSheet()->getStyle('A'.$count_prod.':Q'.$count_prod)->applyFromArray($BStyle);

				$objPHPExcel->getActiveSheet()->getStyle('K'.$count_prod)->getFill()->applyFromArray($styleFill);
			}
			
		}
	}


	$objPHPExcel->getActiveSheet(0)->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("E")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("F")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("G")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("H")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("I")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("J")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("K")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("L")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("M")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("N")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("O")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("P")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("Q")->setAutoSize(true);
 

// Renombrar Hoja

$objPHPExcel->getActiveSheet()->setTitle('Inventario');
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.

$objPHPExcel->setActiveSheetIndex(0);

// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.

header('Content-Type: application/vnd.ms-excel');
$filename = "Inventory - ".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>