<?php
/** Incluir la libreria PHPExcel */
include '../includes/config.php';
require_once '../Classes/PHPExcel.php';

// Crea un nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();

// Establecer propiedades

$objPHPExcel->getProperties()
->setCreator("Cattivo")
->setLastModifiedBy("Cattivo")
->setTitle("Inventario")
->setSubject("Inventario")
->setDescription("Inventario")
->setKeywords("Excel Office 2007 openxml php")
->setCategory("Inventario");

/*DATOS VENDEDOR*/
	$id_seller = $_GET['id'];
	$query_seller = "SELECT * FROM usuario WHERE id =$id_seller";
	$seller = $db->getData($query_seller)[0];
/*DATOS VENDEDOR*/

// Agregar Informacion

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A14', 'CLIENTE');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B14', 'DOCUMENTO');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C14', 'VALOR Q');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D14', 'ENVIO SALDO');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'MEGA-SHOES');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', 'GUATEMALA');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B5', 'REPORTE DE SALDOS PENDIENTES POR CLIENTE');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B7', 'VENDEDOR: '. strtoupper($seller['nombre'])." ". strtoupper($seller['apellido']));
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B9', 'AREA PERIFERICA Y CIUDAD CAPITAL GUATEMALA');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B12', 'CARTERA GENERAL');

	$objPHPExcel->getActiveSheet()->getStyle('A14')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFB92C');
	$objPHPExcel->getActiveSheet()->getStyle('B14')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFB92C');
	$objPHPExcel->getActiveSheet()->getStyle('C14')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFB92C');
	$objPHPExcel->getActiveSheet()->getStyle('D14')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFB92C');	


	$count = 16; //contador
	$count_client; //contador clientes
	$count_order; //contador ordenes


	/*CONSULTA PARA LLENAR DATOS*/

	$query_client = "SELECT * FROM cliente WHERE estado = 1 AND id_usuario = $id_seller GROUP BY id_ruta";
	$clients = $db->getData($query_client); 
	if ($clients) {
		foreach ($clients as $client) {
			$coun_clients = 0;
			$query_rout = "SELECT * FROM ruta WHERE estado = 1 AND id = ".$client['id_ruta'];	
			$rout = $db->getData($query_rout);
			if ($rout) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count, $rout[0]['nombre']);		
				$query_client = "SELECT * FROM cliente WHERE estado = 1 AND id_usuario = $id_seller AND id_ruta = ".$rout[0]['id'];
				$clients = $db->getData($query_client);
				if ($clients) {
					$count_client = $count +1;
					foreach ($clients as $client) {
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count_client, $client['nombre']." - ".$client['id']);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$count_client, "LIMITE CREDITO    Q ".number_format($client['limite_credito'],2));	
						$objPHPExcel->getActiveSheet()->getStyle('A'.$count_client)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFB92C');
						$objPHPExcel->getActiveSheet()->getStyle('B'.$count_client)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FE2E');
						$count_client +=1;
						$query_order = "SELECT * FROM pedido WHERE  id_usuario = $id_seller AND id_cliente = ".$client['id'];
						$orders = $db->getData($query_order);
						if ($orders) {
							$count_order = $count_client;
							$total = count($orders);
							foreach ($orders as $order) {
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count_order,$order['fecha']);
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$count_order, "P#".$order['id']);
								$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$count_order, "Q ".number_format($order['total'],2));
								$count_order +=1;
							}
							$count_client = 0;
							$count_client = $count_client + $count_order;
						}
					}
					$count = 0;
					$count = $count + $count_client;
				}
				
			}

			$count +=1;		
		}
	}
	

	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
$objPHPExcel->getActiveSheet()->getStyle('A14:D14')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('A14:A'.$count)->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('B14:B'.$count)->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('C14:C'.$count)->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('D14:D'.$count)->applyFromArray($borders);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count,"Total");
$objPHPExcel->getActiveSheet()->getStyle('A'.$count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FE2E');
$objPHPExcel->getActiveSheet()->getStyle('B'.$count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FE2E');
$objPHPExcel->getActiveSheet()->getStyle('C'.$count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FE2E');
$objPHPExcel->getActiveSheet()->getStyle('D'.$count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F7FE2E');

	

	$objPHPExcel->getActiveSheet(0)->getColumnDimension("A")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("B")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("C")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("D")->setAutoSize(true);
	$objPHPExcel->getActiveSheet(0)->getColumnDimension("E")->setAutoSize(true);



	

// Renombrar Hoja

$objPHPExcel->getActiveSheet()->setTitle('General');

// FINALIZA LA PRIMER HOJA


$objPHPExcel->setActiveSheetIndex(0);


// HOJA DE PEDIDOS FINALIZADOS

$objPHPExcel->getSheetCount();
$positionInExcel=0; 
$objPHPExcel->createSheet($positionInExcel); 
$objPHPExcel->setActiveSheetIndex(0); 
$objPHPExcel->getActiveSheet()->setTitle("Finalizados");

// FINALIZA HOJA DE PEDIDOS FINALIZADOS



// HOJA DE PEDIDOS ENVIADOS

$objPHPExcel->getSheetCount();
$positionInExcel=0; 
$objPHPExcel->createSheet($positionInExcel); 
$objPHPExcel->setActiveSheetIndex(0); 
$objPHPExcel->getActiveSheet()->setTitle("Enviados");

// FINALIZA HOJA DE PEDIDOS ENVIADOS


// HOJA DE PEDIDOS PENDIENTES

$objPHPExcel->getSheetCount();
$positionInExcel=0; 
$objPHPExcel->createSheet($positionInExcel); 
$objPHPExcel->setActiveSheetIndex(0); 
$objPHPExcel->getActiveSheet()->setTitle("Pendientes");


// FINALIZA HOJA DE PEDIDOS PENDIENTES












// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
header('Content-Type: application/vnd.ms-excel');
$filename = "Reports".date("d-m-Y-His").".xls";
header('Content-Disposition: attachment;filename='.$filename .' ');
header('Cache-Control: max-age=0');
$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');






?>