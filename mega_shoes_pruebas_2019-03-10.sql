# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.21)
# Database: mega_shoes_pruebas
# Generation Time: 2019-03-10 16:34:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `caja`;

CREATE TABLE `caja` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `codigo` varchar(95) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limite_credito` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dias_credito` int(10) DEFAULT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `negocio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  `saldo_disponible` float(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `color`;

CREATE TABLE `color` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table detalle_caja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja`;

CREATE TABLE `detalle_caja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caja_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table detalle_caja_producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_caja_producto`;

CREATE TABLE `detalle_caja_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table detalle_pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detalle_pedido`;

CREATE TABLE `detalle_pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_pedido` int(10) DEFAULT NULL,
  `id_producto` int(10) DEFAULT NULL,
  `cantidad` int(250) DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table empresa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `empresa`;

CREATE TABLE `empresa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_flotante2` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_flotante` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto_inicio` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  `cuenta` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table formas_pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `formas_pago`;

CREATE TABLE `formas_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_id_doc` int(11) DEFAULT NULL,
  `no_recibo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monto` float(11,2) DEFAULT NULL,
  `banco` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_pedido` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_deposito` date DEFAULT NULL,
  `fecha_recibo` date DEFAULT NULL,
  `lugar_ingreso` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_doc` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_pago` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `cheque_procesado` int(11) DEFAULT '0',
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table marca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `marca`;

CREATE TABLE `marca` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table modulo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modulo`;

CREATE TABLE `modulo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table pedido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pedido`;

CREATE TABLE `pedido` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `no_factura` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_usuario` int(10) DEFAULT NULL,
  `id_cliente` int(10) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `id_ruta` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observaciones` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` float(11,2) DEFAULT NULL,
  `flete` float(11,2) NOT NULL,
  `descuento` int(11) NOT NULL,
  `no_envio` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(10) DEFAULT NULL,
  `factura` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `porcentaje_pedido` int(11) NOT NULL,
  `porcentaje_factura` int(11) NOT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table producto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `id_marca` int(10) DEFAULT NULL,
  `id_talla` int(10) DEFAULT NULL,
  `id_color` int(10) DEFAULT NULL,
  `id_padre` int(10) DEFAULT NULL,
  `id_proveedor` int(10) DEFAULT NULL,
  `id_caja` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `cantidad` int(10) DEFAULT NULL,
  `codigo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` float(11,2) DEFAULT NULL,
  `liquidacion` float(11,2) DEFAULT '0.00',
  `fecha` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `estado` int(10) DEFAULT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'sin_token',
  `precio_neto` float(11,2) DEFAULT NULL,
  `porcentaje` int(10) DEFAULT NULL,
  `precio_original` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table proveedor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table rol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table ruta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ruta`;

CREATE TABLE `ruta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table talla
# ------------------------------------------------------------

DROP TABLE IF EXISTS `talla`;

CREATE TABLE `talla` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(10) DEFAULT NULL,
  `talla` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tipo_cliente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_cliente`;

CREATE TABLE `tipo_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) DEFAULT NULL,
  `tipo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(10) DEFAULT NULL,
  `id_empresa` int(10) DEFAULT NULL,
  `id_ruta` int(11) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nit` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
