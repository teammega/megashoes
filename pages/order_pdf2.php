<?php
  include '../includes/config.php';

  // $id_order = $_POST['id_order'];
  $id_order = 1;
  $query_order = "SELECT * FROM pedido WHERE id_empresa = $empresaid AND id = $id_order";
  $order = $db->getData($query_order)[0];
  if ($order) {
    
  }else{
   // header('Location: index.php'); 
  }

  $observaciones = $order['observaciones'];

  /*CONSULTAS EXTERNAS*/
    /*VENDEDOR*/
      $query_user = "SELECT * FROM usuario WHERE id_empresa = $empresaid AND id = ".$order['id_usuario'];
      $users = $db->getData($query_user)[0];

      if ($users) {
        $user = $users['nombre']." ".$users['apellido'];
      }else{
        $user = "No asignado";
      }
      /*VENDEDOR*/

       /*CLIENTE*/
        $query_client = "SELECT * FROM cliente WHERE id_empresa = $empresaid AND id =  ".$order['id_cliente'];
        $clients = $db->getData($query_client)[0];
        if ($clients) {
          $client = $clients['nombre']."".$clients['apellido'];
          $code_client = $clients['codigo'];
          $direc_client = $clients['direccion'];
          $nit_client = $clients['nit'];
          $phone_client = $clients['telefono'];

          $query_type = "SELECT * FROM tipo_cliente WHERE id =".$clients['tipo'];
          $type = $db->getData($query_type)[0];
        }else{
          $client = "No asignado";
        }
        /*CLIENTE*/
  /*CONSULTAS EXTERNAS*/

  /*ESTADO*/
    switch ($order['estado']) {
      case 1:
        $estado = "Pendiente";
        $estado_color = "#ff5722";
      break;

      case 2:
        $estado = "Completado";
        $estado_color = "#8bc34a";
      break;

      case 3:
        $estado = "Enviado";
        $estado_color = "#ff5722";
      break;

      case 4:
        $estado = "Cancelado";
        $estado_color = "#673ab7";
      break;
      
      default:
        $estado = "Error";    
        $estado_color = "red";
      break;
    }
  /*ESTADO*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>PEDIDO PDF</title>
    <link rel="stylesheet" href="../css/style_pdf.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <table>
        <tr>
          <td style="display: inline-block; background-color: #FFF;text-align: left;">
            <img src="../img/logo_empresa.png" style="height: 70px; width: 70px">
            <div id="project2" style="display: inline-block;">
              <div><span>Correo: </span><?=$empresa_correo?></div>
              <div><span>Cuenta: </span><?=$empresa_cuenta?></div>        
            </div>
          </td>
          <td style="background-color: #FFF;vertical-align: unset;">
            <h1>Pedido #<?=$order['id']?></h1>
          </td>
        </tr>
      </table>
      <!-- <div id="logo" style="display: inline-block;"></div> -->
      
    <table style="margin: 0px;">
      <tr>
        <td style="text-align: left;">
          <strong>Vendedor: </strong><?=$user?><br>
          <strong>Cliente: </strong><?=$client?><br>
          <strong>Código de cliente: </strong><?=$code_client?><br>
          <strong>Nit de cliente: </strong><?=$nit_client?>
        </td>
        <td style="text-align: left;">
          <strong>Dirección de cliente: </strong><?=$direc_client?><br>
          <strong>Teléfono cliente: </strong><?=$phone_client?><br>
          <strong>Fecha: </strong><?=$order['fecha']?>
        </td>
      </tr>
    </table>
    
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th class="service">CODIGO</th>
            <th class="desc">NOMBRE</th>
            <th>PRECIO</th>
            <th>CANTIDAD</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $subtotal = 0;
            $query_order = "SELECT * FROM detalle_pedido WHERE id_empresa = $empresaid AND estado = 1 AND id_pedido = ".$id_order;
            if ($db->getData($query_order)) {
              $orders = $db->getData($query_order);
              $count_orders = count($orders);
              if ($count_orders > 0) {
                foreach ($orders as $order) {
                  /*CONSULTAS EXTERNAS*/
                    $query_product = "SELECT * FROM producto WHERE id_empresa = $empresaid AND id=".$order['id_producto'];
                    $product = $db->getData($query_product)[0];
                    if ($product) {
                      $total = $product['precio']*$order['cantidad'];
                      $subtotal = $subtotal + $total
                      ?>
                        <tr>
                          <td class="service"><?=$product['codigo']?></td>
                          <td class="desc"><?=$product['nombre']?></td>
                          <td class="unit">Q <?=number_format($product['precio'],2)?></td>
                          <td class="qty"><?=$order['cantidad']?></td>
                          <td class="total">Q <?=number_format($total,2 )?></td>
                        </tr>                      
                      <?php
                    }
                  /*CONSULTAS EXTERNAS*/
                }
              }
            }
          ?>
          
          <tr>
            <td colspan="4" class="grand total">SUB TOTAL</td>
            <td class="grand total">Q <?=number_format($subtotal,2)?></td>
          </tr>
        </tbody>
      </table>
      <?php

        if ($observaciones != "") {?>
            <div id="notices">
              <div>OBSERVACION:</div>
              <div class="notice"><?=$observaciones?></div>
              <div>Tipo Cliente:</div>
              <div class="notice"><?=$type['descripcion']?></div>
            </div>          
          <?php
        }
      ?>
    </main>    
  </body>
</html>
<?