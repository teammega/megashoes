<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }

  $product_id = $_GET['id'];
  $query_product = "SELECT * FROM producto WHERE id=".$product_id;
  $product = $db->getData($query_product)[0];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title><?=$product['nombre']?></title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>
<!-- loader -->
<?php include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side --> 
    
  <!-- Page Content -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
   
      <div class="row">
        <div class=" col-sm-12">
            <div class="panel panel-default userlist">
              <div class="panel-body" style="padding: 40px !important;">
                <div class="product_item">
                    <div class="row">
                        <div class="col-md-4"> <img src="<?=$product['img']?>" alt="" class="productpic"></div>
                        <div class="col-md-8">
                          <h2><?=$product['nombre']?></h2>
                          <h5><strong>Descripción: </strong><?=$product['descripcion']?></h5>
                          <h5><strong>Precio: </strong>Q<?=$product['precio']?></h5>
                          <?php
                            //Cajas
                            $query_box = "SELECT * FROM caja WHERE id=".$product['id_caja'];
                            $box = $db->getData($query_box)[0];

                            //Marca
                            $query_mark = "SELECT * FROM marca WHERE id=".$product['id_marca'];
                            $mark = $db->getData($query_mark)[0];

                            //Proveedor
                            $query_provider = "SELECT * FROM proveedor WHERE id=".$product['id_proveedor'];
                            $provider = $db->getData($query_provider)[0];
                          ?>
                          <h5><strong>Tipos de caja: </strong><?=$box['nombre']?></h5>
                          <h5><strong>Cantidad: </strong><?=$box['total']?> pares</h5>
                          <h5><strong>Marca: </strong><?=$mark['nombre']?></h5>
                          <h5><strong>Proveedor: </strong><?=$provider['nombre']?></h5>
                          <?php
                            $query_color = "SELECT * FROM color WHERE id=".$product['id_color'];
                            $colors = $db->getData($query_color)[0];

                            $color1 = "background-color: ".$colors['color']."; ";
                            if($colors['color2'] != ""){
                              $color_title = "Colores";
                              $color2 = "background-color: ".$colors['color2'];
                            }else{
                              $color_title = "Color";
                              $color2 = "";
                            }
                          ?>
                          <h5><strong>Colores: </strong> 
                                <a class="btn btn-circle" style="<?=$color1 ?>"></a>
                            <?php
                              if($color2 != ""){ ?>
                                <a class="btn btn-circle" style="<?=$color2 ?>"></a> 
                            <?php
                              }
                            ?>
                          </h5>
                          <table class="table table-striped table-bordered table-hover">
                            <thead>
                              <tr>
                            <?php
                              $query_box_detail = "SELECT * FROM detalle_caja WHERE caja_id=".$box['id'];
                              $box_detail = $db->getData($query_box_detail);

                              foreach ($box_detail as $detail) {
                                $query_tallas = "SELECT * FROM talla WHERE id=".$detail['talla_id'];
                                $tallas = $db->getData($query_tallas);

                                foreach ($tallas as $talla) { ?>
                                  <th><?=$talla['talla']?></th>
                            <?php
                                }
                              }
                            ?>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                              <?php
                                foreach ($box_detail as $detail) { ?>
                                    <td><?=$detail['cantidad']?></td>
                              <?php
                                }
                              ?>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            
           
            
        </div>
      <!-- /.col-sm-12 -->
        
    </div>
    <!-- /.row -->     
      
      
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
 <?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 


<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 
</body>
</html>
