<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title><?=$empresa_nombre?></title>

<!-- font link  -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="../vendor/morrisjs/morris.css" rel="stylesheet">

<!-- jvectormap CSS -->
<link href="../vendor/jquery-jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Personalizado -->
<link href="../css/personalizacion.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->        
    
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Hello <?=$empresa_nombre?></h1>
        <p class="page-subtitle"><?=$texto_inicio?></p>
      </div>
      
    </div>

    <?php
      $query_order1 = "SELECT * FROM pedido WHERE estado = 3";
      if ($db->getData($query_order1)) {
        $orders1 = $db->getData($query_order1);
        $count_orders1 = count($orders1);
      }else{
        $count_orders1 = 0;
      }

      $query_order2 = "SELECT * FROM pedido WHERE estado = 2";
      if ($db->getData($query_order2)) {
        $orders2 = $db->getData($query_order2);
        $count_orders2 = count($orders2);
      }else{
        $count_orders2 = 0;
      }

      $query_order3 = "SELECT * FROM pedido WHERE estado = 1";
      if ($db->getData($query_order3)) {
        $orders3 = $db->getData($query_order3);
        $count_orders3 = count($orders3);
      }else{
        $count_orders3 = 0;
      }

      $query_order4 = "SELECT * FROM pedido WHERE estado = 4";
      if ($db->getData($query_order4)) {
        $orders4 = $db->getData($query_order4);
        $count_orders4 = count($orders4);
      }else{
        $count_orders4 = 0;
      }
    ?>
    
      <div class="row">
        <div class="col-lg-3 col-sm-6">
          <div class="panel panel-blue">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3"> <i class="fa fa-truck fa-2x"></i> </div>
                <div class="col-xs-9 text-right">
                  <div class="huge"><?=$count_orders1?></div>
                  <div>Enviados</div>
                </div>
              </div>
            </div>
           <?php
            if ($count_orders1 > 0 && $_SESSION['tipo_usuario'] == 1) {?>
              <a href="pedidos.php?estado=3">
                <div class="panel-footer"> <span class="pull-left">Ver detalles</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a> 
            <?php            
            }
           ?>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="panel panel-green">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3"> <i class="fa fa-check fa-2x"></i> </div>                
                <div class="col-xs-9 text-right">
                  <div class="huge"><?=$count_orders2?></div>
                  <div>Finalizados</div>
                </div>
              </div>
            </div>
           <?php
            if ($count_orders2 > 0 && $_SESSION['tipo_usuario'] == 1) {?>
              <a href="pedidos.php?estado=2">
                <div class="panel-footer"> <span class="pull-left">Ver detalles</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a> 
            <?php            
            }
           ?>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="panel panel-yellow">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3"> <i class="fa fa-clock-o fa-2x"></i> </div>
                <div class="col-xs-9 text-right">
                  <div class="huge"><?=$count_orders3?></div>
                  <div>Pendientes</div>
                </div>
              </div>
            </div>
            <?php
            if ($count_orders3 > 0 && $_SESSION['tipo_usuario'] == 1) {?>
              <a href="pedidos.php?estado=1">
                <div class="panel-footer"> <span class="pull-left">Ver detalles</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a> 
            <?php            
            }
           ?>
             </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="panel panel-red">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3"> <i class="fa fa-times fa-2x"></i> </div>
                <div class="col-xs-9 text-right">
                  <div class="huge"><?=$count_orders4?></div>
                  <div>Cancelados</div>
                </div>
              </div>
            </div>
            <?php
            if ($count_orders4 > 0 && $_SESSION['tipo_usuario'] == 1) {?>
              <a href="pedidos.php?estado=4">
                <div class="panel-footer"> <span class="pull-left">Ver detalles</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a> 
            <?php            
            }
           ?>
            </div>
        </div> 
      </div>
    </div>
  </div>
</div>

<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 


<!-- Buscar Modulo --> 
<script src="../js/buscarModulo.js"></script> 



<!-- jvectormap JavaScript --> 
<script src="../vendor/jquery-jvectormap/jquery-jvectormap.js"></script> 
<script src="../vendor/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 

</body>
</html>
