<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }

  unset($_SESSION['data_envio']);
  $_SESSION['discount'] = 0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Agregar Envío</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="../js/multi-search/fSelect.css" rel="stylesheet">

</head>

<body>
  <div class="modal fade" id="unlock_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title " id="myModalLabel3">Desbloquear</h4>
        </div>
        <div class="modal-body" style="height: 182px"> 
          <div class="col-md-12" >
            <div class="col-md-12 text-center">
              <label>Contraseña</label>
              <input type="password" class="form-control password_unlock" placeholder="Password">
              <label class="text_error" style="color: red; display: none;">La contraseña es incorrecta</label>
              <p> <a  class="btn btn_desbloquear"  style="margin-top: 20px;background-color: #8bc34a; color:white" ><span class="fa fa-check" style="margin-right: 10px"></span>Desbloquear</a> </p>
            
            </div>
          </div>
          <div class="col-md-12 text-center">
            <h2 class="texto_alerta_cantidad"></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Agregar Envío</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body detalle-main">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" class="envio-form" action="../forms/save_pedido.php" role="form" enctype="multipart/form-data">
                  <?php
                    $select_hide = $_SESSION['tipo_usuario'] != 1 ? "display: none;" : "";
                  ?>
                  <div class="form-group col-md-4" style="<?=$select_hide?>">
                    <label>Vendedor</label>
                    <select class="form-control select-seller" name="user_id" required="true" >
                      <option value="">- Seleccione vendedor -</option>
                      <?php
                        $query_users = "SELECT * FROM usuario WHERE estado = 1 AND id_empresa=".$empresaid;
                        $users = $db->getData($query_users);

                        foreach ($users as $user) { 
                          if($_SESSION['tipo_usuario'] != 1){
                      ?>
                          <option value="<?=$user['id']?>" selected ><?=$user['nombre']?></option>    
                      <?php
                          }else{ ?>
                          <option value="<?=$user['id']?>" ><?=$user['nombre']?></option>    
                      <?php
                          }
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label>Cliente <strong class="limite_credito_text"></strong></label>
                    <select class="form-control select-client" name="cliente_id" required="true" onchange="getLimitClient()">
                      <option value="">- Seleccione cliente -</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                    <label>Flete </label>
                    <input type="number" class="form-control" required="true" name="flete" placeholder="Flete" autocomplete="off" >
                  </div>
                  <div class="form-group col-md-4">
                    <label>Ruta</label>
                    <select class="form-control" name="ruta_id" required="true">
                      <?php
                        $query_ruta = "SELECT * FROM ruta WHERE estado = 1 AND id_empresa=".$empresaid;
                        $rutas = $db->getData($query_ruta);

                        foreach ($rutas as $ruta) { ?>
                          <option value="<?=$ruta['id']?>"><?=$ruta['nombre']?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-5">
                    <label>Dirección</label>
                    <input type="text" class="form-control direction-input" required="true" name="direccion" placeholder="Dirección">
                  </div>
                  <div class="form-group col-md-3">
                    <label>Fecha de envío</label>
                    <input type="text" id="datepicker" class="form-control" name="fecha_entrega" autocomplete="off">
                  </div>
                  <div class="form-group col-md-12">
                    <label>Observaciones</label>
                    <textarea class="form-control" name="observaciones" required="true" placeholder="Observaciones">Observaciones</textarea>
                  </div>
                  <div class="row main-container">
                    <div class="product-container col-md-12">
                      <div class="form-group col-md-5">
                        <label>Productos</label><br>
                        <select class="demo" name="producto_id[]" multiple="multiple">
                          <?php
                            $query_product = "SELECT * FROM producto WHERE estado = 1 AND id_empresa=".$empresaid." AND cantidad > 0";
                            $products = $db->getData($query_product);

                            foreach ($products as $product) { ?>
                              <option value="<?=$product['id']?>"><?=$product['codigo']." - ".$product['nombre']?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                      <div class="col-md-2 porcentaje">
                        <label>Envío (%)</label>
                        <input type="number" class="form-control envio-input" value="100" onkeyup="calcDifference(this.value)" name="porcentaje_envio">
                      </div>
                      <div class="col-md-2 porcentaje">
                        <label>Factura (%)</label>
                        <input type="number" class="form-control invoice-input" value="0" readonly="readonly" name="porcentaje_factura">
                      </div>
                      <div class="col-md-3">
                        <label>Identificador de Envío</label>
                        <input type="text" class="form-control id-invoice" required="true" readonly="readonly" name="no_factura" date="<?=date('dmY-Hi')?>" value="<?=date('dmY-Hi')?>" >
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success">Siguiente</button>
                  <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
          <div class="panel-body detalle-envio" style="display: none;">
            <div class="row">
              <form method="POST" class="envio-form" role="form" enctype="multipart/form-data">
                <div class="col-lg-12">
                  <h4 class="limite_credito_text" style="float: right;padding-right: 2%;"></h4>
                  <div class="col-lg-12 detail-envio">
                    
                  </div>
                  <button type="button" class="btn btn-danger unlock_button" style="display: none;">Desbloquear</button>
                  <button type="button" class="btn btn-success btn_save" onclick="save()">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.row (nested) --> 
          </div>
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->

  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<!-- <script src="../vendor/jquery/jquery.min.js"></script>  -->

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 
<script src="../js/multi-search/fSelect.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
<script>
  function calcDifference(value){
    var invoiceVal = 0;

    if(value <= 100){
      invoiceVal = 100 - value;

      $('.invoice-input').val(invoiceVal);

      if(invoiceVal > 0){
        $('.id-invoice').attr('readonly', false);
        $('.id-invoice').val('');
      }else{
        $('.id-invoice').attr('readonly', true);
        $('.id-invoice').val($('.id-invoice').attr('date'));
      }
    }else{
      $('.id-invoice').val('');
      $('.envio-input').val('');
      $('.invoice-input').val('');
    }
  }

  $(document).ready(function() {
    $('#dataTables-example').DataTable({
        responsive: true,
        pageLength:10,
        sPaginationType: "full_numbers",
        oLanguage: {
            oPaginate: {
                sFirst: "<<",
                sPrevious: "<",
                sNext: ">", 
                sLast: ">>" 
            }
        }
    });

    $('.demo').fSelect();

    $('.select-client').change(function(){
        var element = $(this).find('option:selected'); 
        var direction = element.attr("direction"); 

        $('.direction-input').val(direction); 
    });
    
    $('.envio-form').submit(function() {  
      $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: $(this).serialize(),            
          success: function(data) {
              $('.detalle-main').hide('slow');
              $('.detalle-envio').show('slow');
              $('.detail-envio').html(data);
              
          }
        })
        return false;           
      });

      $('.unlock_button').click(function (){
        $("#unlock_modal").modal("show");
        
      });

      $('.select-seller').change(function (){
        var client_id  = $(this).val();

        $.ajax({
          type: 'POST',
          url: '../forms/filter_clients.php',
          data: { 'id': client_id },            
          success: function(data) {
            $('.select-client').html(data);
          }
        })
      })

      $('.btn_desbloquear').click(function (){
        var pass = $('.password_unlock').val();

        $.ajax({
          type: 'POST',
          url: '../forms/password_unlock.php',
          data: {'pass': pass},            
          success: function(data) {
            if(data == 1){
              $("#unlock_modal").modal("hide");
              $('.btn_save').prop('disabled', false);
              $('.unlock_button').hide('slow');
              $('.text_error').hide('slow');
            }else{
              $('.text_error').show('slow');
            }
          }
        })
      });

    });

    function getLimitClient(){
      var cliente_id = $('.select-client').val();

      $.ajax({
          type: 'POST',
          url: '../forms/get_limit_client.php',
          data: {'id': cliente_id},            
          success: function(data) {
            var dataParsed = JSON.parse(data);
            var saldo_disponible = dataParsed.saldo_disponible;
  
            $(".limite_credito_text").text(" (Limite de crédito: Q." + saldo_disponible + ")");
          }
      })
    }

    function calculateTotal(id, value, flete, descuento = 0){
      var quantity_box = $('.cant_box-' + id).val();
      var precio = $('.precio-factura-' + id).val();
      console.log(precio);
      var subtotal = precio * value * quantity_box;

      $('.subtotal-' + id).text('Q '+subtotal);

      $.ajax({
        type: 'POST',
        url: '../forms/save_total_envio.php',
        data: {'id': id, 'subtotal': subtotal, 'flete': flete, 'cantidad': value, 'descuento': descuento},            
        success: function(data) {
          var dataParsed = JSON.parse(data);

          var subtotal = dataParsed.subtotal;
          var limite = dataParsed.limite;

          $('.total').text('Q ' + subtotal);

          if(subtotal > limite){
            $('.unlock_button').show('slow');
            $('.btn_save').prop('disabled', true);
          }else{
            $('.unlock_button').hide('slow');
            $('.btn_save').prop('disabled', false);
          }
        }
      });
    }

    function save(){
      $.ajax({
        type: 'POST',
        url: '../forms/save_detail_pedido.php',
        data: {},            
        success: function(data) {
          console.log(data);
          window.location = "pedidos.php";
        }
      });
    }

</script>
</body>
</html>
