<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
  	if ($_GET['id']) {

  	   }else{
  	   	header('Location: index.php');
  	   }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/factura.css">
	<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
	<title>Factura (Pedido #<?=$_GET['id']?>)</title>
</head>
<body>

	<div class="container col-md-12 " style="visibility:hidden">
		<div class="head col-md-12" >
			<table style="width:100%">
			  <tr>
			    <th >
			    	<img src="../img/logo_empresa.png" class="logo_empresa ">
			    </th>
			    <th  style="text-align: center;">
			    	<div style="width: 50%">
			    		<h3><?=strtoupper($empresa_nombre)?></h3>
			    		<p style="font-size: 14px;margin-top: -10px"><?=$empresa_direccion?> </p>
			    		<p style="font-size: 14px;margin-top: -10px">Tel: <?=$empresa_telefono?></p>
			    	</div>
			    </th>
			    <th>
			    	<div style="width: 100%">
			    		<h3>FACTURA CAMBIARIA</h4>
			    		<p style="font-size: 14px;margin-top: -10px">Girada libre de protesto</p>
			    		<p style="font-size: 14px;margin-top: -10px">Serie A1</p>
			    	</div>
			    </th>
			  </tr>

			</table>
			<table style="width:100%">
			  <tr>
			    <th style="border: black 1px solid;border;width: 50%">
			    	<p style="margin-top: 50px;">Nit.:</p>
			    </th>
			    <th>
			    	<p style="text-align: left;margin-top: -5px">NIT: 1756832-3</p>
			    	<p style="text-align: center;font-size: 14px">SUJETO A PAGOS TRIMESTRALES</p>
			    	<table style="width:100%">
					  <tr style="border: black 1px solid">
					    <th style="border: black 1px solid">D&iacute;a</th>
					    <th style="border: black 1px solid">Mes</th>
					    <th style="border: black 1px solid">A&ntilde;o</th>
					  </tr>
					  <tr>
					  	<th style="border: black 1px solid;height: 20px"></th>
					  	<th style="border: black 1px solid;height: 20px"></th>
					  	<th style="border: black 1px solid;height: 20px"></th>
					  </tr>

					</table>
			    </th>

			  </tr>

			</table>
		</div>

		<div class="body_factura">
			<table style="width: 100%">
				<tr>
					<th style="background-color: #cbcbcb; border: black 1px solid;">CANT.</th>
					<th style="background-color: #cbcbcb; border: black 1px solid;">D E S C R I P C I O N</th>
					<th style="background-color: #cbcbcb; border: black 1px solid;"><p>PRECIO</p><p>UNITARIO</p></th>
					<th style="background-color: #cbcbcb; border: black 1px solid;">VALOR</th>
				</tr>
				<?php
					$query_detail = "SELECT * FROM detalle_pedido WHERE estado = 1 AND id_pedido = ".$_GET['id'];
					if ($db->getData($query_detail)) {
						$orders = $db->getData($query_detail);
						$total = 0;
						$descuento = 0;
						$resultado = 0;
						foreach ($orders as $order) {
							$query_product = "SELECT * FROM producto WHERE id = ".$order['id_producto'];
							$producto = $db->getData($query_product)[0];
							?>
								<tr>
									<th><?=$order['cantidad']?></th>
									<th><?=$producto['nombre']?></th>
									<th>Q <?=number_format($producto['precio'],2)?></th>
									<th><?php
										$cantidad = $order['cantidad'];
										$precio = $producto['precio'];

										if ($producto['liquidacion']>0) {
											$resultado = ($precio * $producto['liquidacion'])/100;
											$resultado = $precio - $resultado;
											$resultado = $resultado * $cantidad;
											$descuento = $descuento + $resultado;
										}else{
											$resultado = 0;
										}
										$sub_total = $cantidad * $precio;


										echo " Q ".number_format($sub_total,2);
									?></th>
								</tr>
							<?php
							$total = $total + $sub_total;
						}
					}
				?>
			</table>
			<table style="width: 100%" >
				<tr >
					<th style="border : black 1px solid;width: 575px;height: 60px">
						<p style="font-size:10px;text-align: left;margin-top: -30px;margin-left: 10px">TOTALES EN LETRAS</p>
					</th>
					<th style="border : black 1px solid;width: 198px;"><p style="font-size: 10px; margin-top: -30px">No.CAJAS</p></th>
					<th style="border : black 1px solid; width: 312px"><p style="font-size: 10px">SUB-TOTAL</p></th>
					<th style="border : black 1px solid">
						<p style="font-size: 14px; text-align: center;"> Q <?=number_format($total,2)?></p>
					</th>
				</tr>
			</table>
			<table style="width: 100%">
				<th style="border : black 1px solid;">
					<p style="font-size: 10px; text-align: justify;">LA MERCADERIA VIAJA POR CUENTA Y RIESGO DEL</p>
					<p style="font-size: 10px; text-align: justify;">COMPRADOR. ILDNMIK..SESTA FACTURA NO</p>
					<p style="font-size: 10px; text-align: justify;">CONSTITUYE</p>
					<p style="font-size: 10px; text-align: justify;">COMPROBANTE DE PAGO. AL CANCELAR EXIJA SU</p>
					<p style="font-size: 10px; text-align: justify;">RECIBO DE CAJA</p>
					<p style="font-size: 10px; text-align: justify;">NO SE ACEPTAN DEVOLUCIONES</p>
					<p style="margin-top: 30px">_________________________</p>
				</th>
				<th style="border : black 1px solid;width: 771px">
					<div style="height: 190px;width: 80%;float: left;">
					<p style="font-size: 10px;text-align: justify;">EL IMPORTE DE ESTA FACTURA DEBERA SER CANCELADA AL VENCIMIENTO ACEPTADO. EN</p>
					<p style="font-size: 10px;text-align: justify;">LAS OFICINAS DE MEGA SHOES. O A TERCERA PERSONA QUE &Eacute;STA NOMBRE,</p>
					<p style="font-size: 10px;text-align: justify;">LOS OBLIGJMIDISLLSILSIITA FACTURA RENUNCIAN AL FUERO DE SU DOMICILIO Y SE SOMETEN</p>
					<p style="font-size: 10px;text-align: justify;">A LOS TRIBUNALES DE LA CIUDAD DE GUATEMALA</p>
					<p style="font-size: 10px;text-align: justify;">EL CLIENTE ACEPTA Y RECONOCE COMO OBLIGATORIAS LAS CONDICIONES ESPEIFICADAS</p>
					<p style="font-size: 10px;text-align: justify;">EN ESTA FACTURA Y DECLARA RECIBIDA LAS MERCADERIAS CONFORME Y ACEPADAS LIBRE</p>
					<p style="font-size: 10px;text-align: justify;">DE PROTESRO.</p>

					</div>
					<div style="height: 180px;margin-top: 5px;width: 17%;float: right;margin-right: 5px">
						<p style="font-size: 14px;text-align: center;margin-top: 45px">DESC.</p>
						<p style="font-size: 14px;font-family: arial;margin-top: 70px">TOTAL Q</p>

					</div>

				</th>
				<th style="border : black 1px solid;width: 220px;">
					<p style="font-size: 14px;text-align: center;margin-top: 45px"><?php
					if ($descuento > 0) {
						echo "Q ".number_format($descuento,2);
					}else{
						echo "---";
					}
					?></p>
						<p style="font-size: 14px;font-family: arial;margin-top: 70px">
							<?php
								echo "Q ".number_format(($total - $descuento),2);
							?>
						</p>
				</th>
			</table>
		</div>
	</div>
	<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../js/factura.js"></script>
</body>
</html>
