<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Usuarios</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Usuarios</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> Listado de usuarios 
            <a href="add_user.php" class="btn btn-success btn-circle pull-right"><i class="fa fa-plus"></i></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
                <thead>
                  <tr>
                    <th>IMG</th>
                    <th>Nombre</th>
                    <th>Correo Electrónico</th>
                    <th>Tipo de Usuario</th>
                    <th>Contraseña</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    /* listado de proveedores */
                    $query_users = "SELECT * FROM usuario WHERE id_empresa=".$empresaid." AND estado = 1";
                    $users = $db->getData($query_users);

                    if($users){
                      foreach ($users as $user) { ?>
                      <tr class="item-<?=$user['id']?>">
                        <?php
                          if ($user['id_rol']==2) {
                            $link = "vendedor.php?id=".$user['id'];
                          }else{
                            $link = "vendedor.php?id=".$user['id'];
                          }
                        ?>
                        <td><a href="<?=$link?>"><img src="<?=$user['img']?>" style="max-width: 75px !important;"></a></td>
                        <td><?=$user['nombre']." ".$user['apellido']?></td>
                        <td><?=$user['correo']?></td>
                        <td>
                          <?php
                            $query_rol = "SELECT * FROM rol WHERE id=".$user['id_rol'];
                            $rol = $db->getData($query_rol)[0];
                            echo $rol['nombre'];
                          ?>
                        </td>
                        <td><?=$user['clave']?></td>
                        <td>
                          <a href="edit_user.php?id=<?=$user['id']?>" class="btn btn-warning btn-circle"><i class="fa fa-pencil"></i> </a>
                          <button type="button" onClick="deleteItem(<?=$user['id']?>, 'usuario')" class="btn btn-danger btn-circle"><i class="fa fa-times"></i> </button>
                        </td>
                      </tr>
                  <?php
                      }
                    }
                  ?>
                  
                
                </tbody>
              </table><!-- /.table-responsive -->           
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });
    </script>
</body>
</html>
