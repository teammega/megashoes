<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {

  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Clientes</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Clientes</h1>

      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <?php
      $user_access_style = $_SESSION['tipo_usuario'] == 1 ? "" : "display:none;";
    ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="../src/reporte_saldos.php"><i class="fa fa-file-excel-o"></i> Generar Reporte</a>
            <a href="add_client.php" class="btn btn-success btn-circle pull-right" style="<?=$user_access_style?>"><i class="fa fa-plus"></i></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
                <thead>
                  <tr>
                    <th>Vendedor</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Saldo disponible</th>
                    <th>Días de Crédito</th>
                    <th>Negocio</th>
                    <th>Tipo</th>
                    <th>Ruta</th>
                    <th style="<?=$user_access_style?>">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $query_part_user_access = $_SESSION['tipo_usuario'] == 1 ? "" : " AND id_usuario=".$_SESSION['id_usuario'];
                    /* listado de proveedores */
                    $query_clients = "SELECT * FROM cliente WHERE id_empresa=".$empresaid." AND estado=1".$query_part_user_access;

                    /* FILTROS */
                      if (isset($_GET['idU'])) {
                        $query_clients .=" AND id_usuario = ".$_GET['idU'];
                      }
                    /* FILTROS */
                    $clients = $db->getData($query_clients);

                    if($clients){
                      foreach ($clients as $client) { ?>
                      <tr class="item-<?=$client['id']?>">
                        <td>
                          <?php
                            $consulta_usuario = "SELECT * FROM usuario WHERE id_empresa = $empresaid AND estado = 1 AND id = ".$client['id_usuario'];

                            if ($db->getData($consulta_usuario)) {
                              $usuario = $db->getData($consulta_usuario);
                              $cantidad_usuario = count($usuario);
                              if ($cantidad_usuario > 0) {?>
                                <a href="edit_user.php?id=<?=$usuario[0]['id']?>"><?=$usuario[0]['nombre']." ".$usuario[0]['apellido']?></a>
                                <?php
                              }
                            }

                          ?>

                        </td>
                        <td>
                          <?php
                            if($_SESSION['tipo_usuario'] == 1){ ?>
                              <a href="historial.php?id=<?=$client['id']?>&type=all"> <?=$client['nombre']." ".$client['apellido']?></a>
                          <?php
                            }else{ ?>
                              <?=$client['nombre']." ".$client['apellido']?>
                          <?php
                            }
                          ?>
                        </td>
                        <td><?=$client['direccion']?></td>
                        <td><?=$client['telefono']?></td>
                        <?php
                          $saldo_disponible = $client['saldo_disponible'];
                        ?>
                        <td>Q.<?=$saldo_disponible?></td>
                        <td><?=$client['dias_credito']?></td>
                        <td><?=$client['negocio']?></td>
                        <td>
                          <?php
                            $query_types = "SELECT * FROM tipo_cliente WHERE id = ".$client['tipo'];
                            $type = $db->getData($query_types)[0];

                            echo $type['descripcion']." (" . $type['tipo'] . ")";
                          ?>
                        </td>
                        <td>
                          <?php
                            $query_ruta = "SELECT * FROM ruta WHERE id =".$client['id_ruta'];
                            $ruta = $db->getData($query_ruta);

                            if ($ruta) {
                              echo $ruta[0]['nombre'];
                            }else{
                              echo "No tiene ruta";
                            }
                          ?>
                        </td>
                        <td style="<?=$user_access_style?>">
                          <a href="edit_client.php?id=<?=$client['id']?>" class="btn btn-warning btn-circle"><i class="fa fa-pencil"></i> </a>
                          <button type="button" onClick="deleteItem(<?=$client['id']?>, 'cliente')" class="btn btn-danger btn-circle"><i class="fa fa-times"></i> </button>
                        </td>
                      </tr>
                  <?php
                      }
                    }
                  ?>


                </tbody>
              </table><!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->



  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">",
                        sLast: ">>"
                    }
                }
            });
    });
    </script>
</body>
</html>
