<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Liquidaciones</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../css/personalizacion.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Liquidaciones</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="../src/liq_report.php"><button type="button" class="btn btn-success "><i class="fa fa-download"></i> Reporte</button></a>
                </div>
            <!-- /.panel-heading -->
                <div class="panel-body">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th>Fecha de despacho</th>
                                <th>Envío</th>
                                <th>Nombre Cliente</th>
                                <th>Monto envío</th>
                                <th>Envío menos descuento</th>
                                <th>Días crédito</th>
                                <th>Efectivo/Boleta</th>
                                <th>Cheques liq. 1</th>
                                <th>Total Abonado</th>
                                <th>Saldo</th>
                                <th>Número de recibo</th>
                                <th>Fecha Ingreso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $pagado = 0;
                                $acumulado = 0;

                                $query_payments = "SELECT * FROM formas_pago WHERE id_empresa=$empresaid ORDER BY id_pedido ASC";
                                $payments = $db->getData($query_payments);

                                foreach ($payments as $payment) { 
                                    $query_order = "SELECT * FROM pedido WHERE id=".$payment['id_pedido'];
                                    $order = $db->getData($query_order)[0];
                                    $query_client = "SELECT * FROM cliente WHERE id=".$order['id_cliente'];
                                    $client = $db->getData($query_client)[0];                                    

                                    $total = $order['total'] - (($order['total'] - $order['flete']) * ($order['descuento'] / 100));

                                    $total_pedido = $total * ($order['porcentaje_pedido'] / 100);

                                    $saldo = $order['total'];
					
                                    if($payment['tipo_pago']!='cheques'){
                                        $acumulado = $acumulado + $payment['monto'];
                                    }

                                    if($payment['tipo_pago']!='cheques'){
                                        $saldo = $saldo - $acumulado;
                                    }
                                    // getExtraData($payment['id_pedido']);
                            ?>
                                    <tr>
                                        <td><?=$payment['fecha']?></td>
                                        <td><?=$order['no_envio']?></td>
                                        <td><?=$client['nombre']. " " .$client['apellido']?></td>
                                        <td>Q. <?=number_format($order['total'] - $pagado, 2) ?></td>
                                        <td>Q. <?=number_format($total_pedido - $order['descuento'],2) ?></td>
                                        <td><?=$client['dias_credito'] ?></td>
                                        <?php
                                            $boleta = "";
                                            $cheque = "";

                                            if($payment['tipo_pago'] == "depositos"){
                                                $boleta = "Q. ".$payment['monto'];
                                            }

                                            if($payment['tipo_pago'] == "cheques"){
                                                $cheque = "Q. " . $payment['monto'];
                                            }
                                        ?>
                                        <td><?=$boleta?></td>
                                        <td><?=$cheque?></td>
                                        <td>Q. <?=number_format($payment['monto'])?></td>
                                        <td>Q. <?=number_format($saldo,2)?></td>
                                        <td><?=$payment['no_id_doc']?></td>
                                        <td><?=$payment['fecha_recibo']?></td>
                                    </tr>
                            <?php
                                if($payment['tipo_pago']!='cheques'){
                                    $pagado = $pagado + $payment['monto'];
                                }
                                }
                            ?>
                        </tbody>
                    </table><!-- /.table-responsive -->           
                </div>  
            <!-- /.panel-body --> 
            </div>
        <!-- /.panel --> 
        </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- excel -->
<script src="../js/excel.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });

    function goToDetail(id){
      window.location = 'producto.php?id=' + id;
    }
</script>

<?php
    function getExtraData($pedido_id){
        // $query_order = "SELECT * FROM pedido WHERE id = ".$pedido_id;
        // $query_order = "SELECT * FROM pedido";
        // $order = $db->getData($query_order);
        echo "<pre>";
        print_r($pedido_id);
        echo "</pre>";

        // $query_client = "SELECT * FROM cliente WHERE id = ".$order['id_cliente'];
        // $client = $db->getData($query_client);

        // $extra_data = ['client_name' => $client['nombre'] . " " . $client['apellido']];

        // return $extra_data;
    }
?>
</body>
</html>
