<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Agregar Cliente</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Agregar Cliente</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" action="../forms/save_client.php" role="form" enctype="multipart/form-data">
                  <div class="form-group col-lg-6">
                    <label>Vendedor</label>
                    <select class="form-control " name="id_usuario" required="true">
                      <?php
                        $query_marca = "SELECT * FROM usuario WHERE id_empresa=".$empresaid." AND (id_rol=2 OR id_rol=1 OR id_rol=3) AND estado=1";
                        $marcas = $db->getData($query_marca);

                        if($marcas){                          
                          foreach ($marcas as $marca) { ?>
                            <option value="<?=$marca['id']?>"><?=$marca['nombre']?></option>
                      <?php
                          }
                        }else{ ?>
                            <option value="">No hay marcas disponibles. Por favor agregue antes de continuar</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Código</label>
                    <input class="form-control" required="true" type="text" name="codigo" placeholder="Código">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Nombre</label>
                    <input class="form-control" required="true" type="text" name="nombre" placeholder="Nombre">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Apellido</label>
                    <input class="form-control" required="true" name="apellido" placeholder="Apellido">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Direccion</label>
                    <input class="form-control"  name="direccion" placeholder="Direccion">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Telefono</label>
                    <input class="form-control" name="telefono" placeholder="Telefono">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Nit</label>
                    <input class="form-control"  name="nit" placeholder="Nit">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Límite de crédito</label>
                    <input class="form-control" required="true" type="number" step="any" name="limite_credito" placeholder="Limite de credito">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Dias credito</label>
                    <input class="form-control" required="true" type="number" name="dias_credito" placeholder="Dias credito">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Saldo disponible</label>
                    <input class="form-control" required="true" type="number" step="any" name="saldo_disponible" placeholder="Saldo disponible">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Observaciones</label>
                    <input class="form-control"  name="observaciones" placeholder="Observaciones">
                  </div>
                  <div class="form-group col-lg-6">
                    <label>Nombre del negocio</label>
                    <input class="form-control"  name="negocio" placeholder="Nombre del negocio">
                  </div>
                  
                  <div class="form-group col-lg-6">
                    <label>Tipo cliente</label>
                    <select class="form-control " name="tipo_cliente" required="true">
                      <?php
                        $query_marca = "SELECT * FROM tipo_cliente WHERE id_empresa= $empresaid AND estado=1";
                        $marcas = $db->getData($query_marca);

                        if($marcas){                          
                          foreach ($marcas as $marca) { ?>
                            <option value="<?=$marca['id']?>"><?=$marca['tipo']?></option>
                      <?php
                          }
                        }else{ ?>
                            <option value="">No hay tipos disponibles, agregue algun tipo de cliente</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Ruta</label>
                    <select class="form-control" name="ruta_id" required="true">
                      <?php
                        $query_ruta = "SELECT * FROM ruta WHERE estado = 1 AND id_empresa=".$empresaid;
                        $rutas = $db->getData($query_ruta);

                        foreach ($rutas as $ruta) { ?>
                          <option value="<?=$ruta['id']?>"><?=$ruta['nombre']?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  
                            
                  <button type="submit" class="btn btn-default">Agregar</button>
                  <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 

</body>
</html>
