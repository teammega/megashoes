<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Perfil de Usuario</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Perfil de Usuario</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" action="../forms/save_changes_user.php" role="form" enctype="multipart/form-data">
                  <?php
                    $id_login = $_SESSION['id_usuario'];
                    $query_profile = "SELECT * FROM  usuario WHERE id=".$id_login;
                    $profile = $db->getData($query_profile)[0];
                  ?>
                  <div class="features-container row">
                    <div class="col-md-12">
                      <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <img src="<?=$profile['img']?>" style="width: 90%;">
                        </div>
                        <div class="col-md-12">
                          <input type="file" name="img">
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group col-md-6">
                          <label>Nombre</label>
                          <input class="form-control" type="text" name="nombre" placeholder="Nombre" value="<?=$profile['nombre']?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label>Apellido</label>
                          <input class="form-control" type="text" name="apellido" placeholder="Apellido" value="<?=$profile['apellido']?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label>Correo Eléctronico</label>
                          <input class="form-control" type="text" name="correo" placeholder="Correo Eléctronico" value="<?=$profile['correo']?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label>Teléfono</label>
                          <input class="form-control" type="text" name="telefono" placeholder="Teléfono" value="<?=$profile['telefono']?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label>Dirección</label>
                          <input class="form-control" type="text" name="direccion" placeholder="Dirección" value="<?=$profile['direccion']?>">
                        </div>
                        <div class="form-group col-md-6">
                          <label>Contraseña</label>
                          <input class="form-control" type="text" name="clave" placeholder="Contraseña" value="<?=$profile['clave']?>">
                        </div>
                          <button type="submit" class="btn btn-default">Editar</button>
                          <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                        </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });

    function addItem(){
      $.ajax({
          type: 'POST',
          url: '../forms/getItemBoxAdd.php',
          data: {},            
          success: function(data) {
            console.log(data);
            $('.features-container').append(data);
          }
      })
    }
    </script>
</body>
</html>
