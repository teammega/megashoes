<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {

    }else{
     header('Location: index.php');
    }
  }else{
    header('Location: login.php');
  }

  if (isset($_GET['type'])) {
    $title = ucwords($_GET['type']);
  }else {
    $title ="";
  }

  $efectivoStyle = "";
  if($_GET['type'] == 'efectivo'){
    $efectivoStyle = " display: none; ";
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
  <title>Agregar <?=$title?></title>

  <!-- Bootstrap Core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- DataTables CSS -->
  <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

  <!-- DataTables Responsive CSS -->
  <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../css/adminnine.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link href="../js/multi-search/fSelect.css" rel="stylesheet">
</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Agregar <?=$title?></h1>

      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body detalle-main">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" class="envio-form" action="../forms/save_payment.php" role="form" enctype="multipart/form-data">
                  <div class="form-group col-md-6">
                    <label>No. de Pedido </label><br>
                    <select class="pedidos" name="pedido">
                      <option value="">Seleccione una opción</option>
                      <?php
                        $query_pedido = "SELECT * FROM pedido WHERE estado = 1";
                        $pedidos = $db->getData($query_pedido);

                        foreach ($pedidos as $pedido) {
                          $query_client = "SELECT * FROM cliente WHERE id =".$pedido['id_cliente'];
                          $client = $db->getData($query_client)[0];
                          if ($client) {
                            $client_name = $client['nombre'];
                          }else{
                            $client_name = "";
                          }
                           ?>
                          <option value="<?=$pedido['id']?>"><?=$pedido['no_envio']." - ".$client_name?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label class="amount">Monto </label>
                    <input type="text" class="form-control amount-input" required="true" name="monto" placeholder="Monto">
                  </div>
                  <?php
                    $typeOfPayment = strtolower($_GET['type']);

                    if($typeOfPayment == "depositos"){
                      include '../modulos/deposits.php';
                    }else if($typeOfPayment == "cheques"){
                      include '../modulos/checks.php';
                    }
                  ?>
                  <div id="description_payment" class="form-group col-md-12">
                    <label>Descripcion </label>
                    <textarea class="form-control" name="descripcion" required="true" placeholder="Descripcion"></textarea>
                  </div>

                  <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-success">Siguiente</button>
                    <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.row (nested) -->
          </div>
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->



  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery -->
<!-- <script src="../vendor/jquery/jquery.min.js"></script>  -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/adminnine.js"></script>
<script src="../js/multi-search/fSelect.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
  } );
</script>

<script>
  $( function() {
    $( "#datepicker2" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
  } );
</script>
<script>
  // Multi select and search
  $('.pedidos').fSelect();

  $(document).ready(function() {
    $('.pedidos').change(function(){
      var order_id = $(this).val();
      
      $.ajax({
        type: 'POST',
        url: '../forms/get_order_detail.php',
        data: { order_id: order_id },            
        success: function(data) {
          var order = JSON.parse(data);
          
          $('.amount').html("Monto (<strong>Total: Q " + order.total + " / Pagado: Q " + order.total_pagado + " / Restante: Q " + order.total_restante + "</strong>)");
          $('.amount-input').val(order.total_restante);
        }
      });
    });

    $('#dataTables-example').DataTable({
      responsive: true,
      pageLength:10,
      sPaginationType: "full_numbers",
      oLanguage: {
        oPaginate: {
          sFirst: "<<",
          sPrevious: "<",
          sNext: ">",
          sLast: ">>"
        }
      }
    });
  }); 
</script>
</body>
</html>
