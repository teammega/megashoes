<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Editar Proveedor</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Editar Proveedor</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" action="../forms/save_edit_provider.php" role="form" enctype="multipart/form-data">
                  <?php
                    $provider_id = $_GET['id'];

                    $query_provider = "SELECT * FROM proveedor WHERE id=".$provider_id;
                    $provider = $db->getData($query_provider)[0];
                  ?>
                  <input class="form-control" type="hidden" name="id" value="<?=$provider['id']?>">
                  <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" required="true" name="nombre" placeholder="Nombre" value="<?=$provider['nombre']?>">
                  </div>
                  <div class="form-group">
                    <label>Apellido</label>
                    <input class="form-control" required="true" name="apellido" placeholder="Apellido" value="<?=$provider['apellido']?>">
                  </div>
                  <div class="form-group">
                    <label>Dirección</label>
                    <input class="form-control" required="true" name="direccion" placeholder="Dirección" value="<?=$provider['direccion']?>">
                  </div>
                  <div class="form-group">
                    <label>Teléfono</label>
                    <input type="number" class="form-control" required="true" name="telefono" placeholder="Teléfono" value="<?=$provider['telefono']?>">
                  </div>
                  <div class="form-group">
                    <label>NIT</label>
                    <input class="form-control" required="true" name="nit" placeholder="NIT" value="<?=$provider['nit']?>">
                  </div>
                  <div class="form-group">
                    <label>Imagen</label>
                    <img style="width: 250px;" src="<?=$provider['img']?>">
                    <input type="file" name="img">
                  </div>
                  <button type="submit" class="btn btn-default">Editar</button>
                  <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });
    </script>
</body>
</html>
