<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Inventario</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" type="text/css" href="../css/personalizacion.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Inventario</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="../src/excel.php"><button type="button" class="btn btn-success ">Excel</button></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
          <thead>
            <tr>
              <th>Imagen</th>
              <th>Código</th>
              <th>Nombre </th>
              <th>Marca</th>
              <th>Colores</th>
              <th>Tipo de caja</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $query_product = "SELECT * FROM producto WHERE estado=1 AND id_empresa=".$empresaid;
              $products = $db->getData($query_product);

              foreach ($products as $product) { ?>
                <tr>
                  <td><img src="<?=$product['img']?>" style="border-radius: 100%;width: 60px; height: 60px;"></td>
                  <td><?=$product['codigo']?></td>
                  <td><?=$product['nombre']?></td>
                  <td>
                    <?php
                      $query_mark = "SELECT * FROM marca WHERE estado=1 AND id=".$product['id_marca'];
                      $mark = $db->getData($query_mark)[0];

                      echo $mark['nombre'];
                    ?>
                  </td>
                  <td>
                    <?php
                      $query_color = "SELECT * FROM color WHERE estado=1 AND id_empresa = $empresaid AND id=".$product['id_color'];
                      $colors = $db->getData($query_color)[0];                      
                    ?>
                    <div style="height: 50px; width: 50px; border-radius: 100%;background-color: <?=$colors['color2']?>;border-left: 25px solid <?=$colors['color']?>; box-shadow: 0px 0px 5px #000" />
                      
                    
                    
                  </td>
                  <td>
                    <?php
                      $query_box = "SELECT * FROM caja WHERE id=".$product['id_caja'];
                      $box = $db->getData($query_box)[0];

                      echo $box['nombre'];
                    ?>
                  </td>
                  <td><?=$product['cantidad']?></td>
                </tr>
            <?php
              }
            ?>
          </tbody>
        </table><!-- /.table-responsive -->           
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- excel -->
<script src="../js/excel.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });

    function goToDetail(id){
      window.location = 'producto.php?id=' + id;
    }
    </script>
</body>
</html>
