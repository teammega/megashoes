<?php
  include '../includes/config.php';
  require '../lib/conversor.php';

  $id_order = $_POST['id_order'];
  // $id_order = $_POST['id_order'];
  $gran_total = 0;
?>
<!DOCTYPE html>
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

    <title>PEDIDO PDF</title>
    <!-- <link rel="stylesheet" href="../css/style_pdf.css" media="all" /> -->
  </head>
  <body>
    <?php
      $query_pedido = "SELECT * FROM pedido WHERE id_empresa=".$empresaid." AND id=".$id_order;
      $pedido = $db->getData($query_pedido)[0];
    ?>
    <table width="100%">
      <tr>
        <td style="text-align: left;">
          <label>Guatemala, Ciudad</label>
        </td>
        <td width="25%"></td>
      </tr>
      <tr width="100%">
        <td></td>
        <td width="25%" style="text-align: 25%;">
          <?php
            $date_pedido = date_create($pedido['fecha']);
          ?>
          <label><strong>Fecha: </strong><?=date_format($date_pedido, 'd/m/Y')?></label>
        </td>
      </tr>
      <tr>
        <?php
          $query_user = "SELECT * FROM usuario WHERE id_empresa=".$empresaid." AND id=".$pedido['id_usuario'];
          $user = $db->getData($query_user)[0];

          $query_cliente = "SELECT * FROM cliente WHERE estado=1 AND id_empresa=".$empresaid." AND id=".$pedido['id_cliente'];
          $cliente = $db->getData($query_cliente)[0];
        ?>
        <td>
          <label><strong>Nombre: </strong><?=$cliente['nombre']." ".$cliente['apellido']?></label>
        </td>
        <td width="25%"></td>
      </tr>
      <tr>
        <?php

        ?>
        <td>
          <label><strong>Negocio: </strong> <?=$cliente['negocio'] ?></label>
        </td>
        <td width="25%">
          <label><strong>Nit: </strong><?=$cliente['nit']?></label>
        </td>
      </tr>
      <tr>
        <td>
          <label><strong>Direccion: </strong><?=$pedido['direccion']?></label>
        </td>
        <td width="25%">
          <label><strong>Telefono: </strong><?=$cliente['telefono']?></label>
        </td>
      </tr>
      <tr>
        <td>
          <label>Vendedor #<?=$pedido['id_usuario']?></label>
        </td>
        <td width="25%">
          <label>CLIENTE #<?=$pedido['id_cliente']?></label>
        </td>
      </tr>
    </table>

    <table width="100%" style="border-collapse: collapse;">
      <tr>
        <th style="text-align: left;border: 1px solid black;width: 18%;">CANTIDAD PARES</th>
        <th style="text-align: left;border: 1px solid black;"></th>
        <th style="text-align: left;border: 1px solid black;">Descripcion</th>
        <th style="text-align: left;border: 1px solid black;width: 10%;">Unitario</th>
        <th style="text-align: left;border: 1px solid black;width: 5%;"></th>
        <th style="text-align: left;border: 1px solid black;">Total</th>
      </tr>

      <?php
        $query_detalle_pedido = "SELECT * FROM detalle_pedido WHERE id_pedido=".$id_order;
        $detalle_pedido = $db->getData($query_detalle_pedido);

        foreach ($detalle_pedido as $envio) {
          $query_producto = "SELECT * FROM producto WHERE estado=1 AND id_empresa=".$empresaid." AND id=".$envio['id_producto'];
          $producto = $db->getData($query_producto)[0];

          $query_caja = "SELECT * FROM caja WHERE estado=1 AND id=".$producto['id_caja'];
          $caja = $db->getData($query_caja)[0];

          $pares = $envio['cantidad'] * $caja['total'];

          $unitario = $producto['precio'] * 0.15;
          $unitario += $producto['precio'];
      ?>
        <tr>
          <td style="border: 1px solid black;width: 18%;"><?=$pares ?></td>
          <td style="border: 1px solid black;"><?=$caja['nombre'] ?></td>
          <td style="border: 1px solid black;"><?=$producto['codigo']." ".$producto['nombre']?></td>
          <td style="border: 1px solid black;width: 10%;">Q <?=number_format($unitario, 2) ?></td>
          <td style="border: 1px solid black;width: 5%;"></td>
          <?php
            $total_producto = ($unitario * $envio['cantidad'] * $caja['total']);
            $total_producto = $total_producto * ($pedido['porcentaje_pedido']/100);
            $gran_total += $total_producto;
          ?>
          <td style="border: 1px solid black;">Q <?=number_format($total_producto, 2) ?></td>
        </tr>
      <?php
        }
        $gran_total += $pedido['flete'];
      ?>
        <tr>
          <td colspan="5" style="border: 1px solid black;text-align: left;"><strong>Flete</strong></td>
          <td style="border: 1px solid black;">Q <?=number_format($pedido['flete'], 2) ?></td>
        </tr>
        <tr>
          <td colspan="5" style="border: 1px solid black;text-align: left;"><strong>TOTAL</strong></td>
          <td style="border: 1px solid black;">Q <?=number_format($gran_total, 2) ?></td>
        </tr>
    </table>

    <table width="100%">
      <tr>
        <?php
          $en_letras = convertir($gran_total);
        ?>
        <td>Total en letras: <?=$en_letras ?></td>
      </tr>
      <tr>
        <td></td>
      </tr>
      <tr>
        <td>Este documento es exclusivamente para el transporte de mercaderia, no es un documento contable</td>
      </tr>
    </table>
  </body>
</html>
