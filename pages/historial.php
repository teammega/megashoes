<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {

  }else{
    header('Location: login.php');
  }

  if (isset($_GET['id']) ) {

  }else{
    header('Location: index.php');
  }
  $id_client = $_GET['id'];
  $query_client = "SELECT * FROM cliente WHERE id = $id_client";
  $client = $db->getData($query_client)[0];


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Historial - <?=$client['nombre']." ".$client['apellido']?></title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->

  <!-- Page Content -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Historial -  <?=$client['nombre']." ".$client['apellido'] ?></h1>
        <ol class="breadcrumb prueba">
          <a href="historial.php?id=<?=$id_client?>&type=all" ><button type="button" class="btn btn-primary">Todos</button></a>
          <a href="historial.php?id=<?=$id_client?>&type=efectivo" ><button type="button" class="btn btn-primary">Efectivo</button></a>
          <a href="historial.php?id=<?=$id_client?>&type=cheques" ><button type="button" class="btn btn-primary">Cheques</button></a>
          <a href="historial.php?id=<?=$id_client?>&type=depositos" ><button type="button" class="btn btn-primary">Depositos</button></a>
        </ol>
      </div>
    </div>
    <div class="row">
      <?php

        $query_orders = "SELECT * FROM pedido WHERE id_cliente = $id_client AND estado!=5 ";
        $orders = $db->getData($query_orders);

        if ($orders) {
          foreach ($orders as $order) {

            switch ($order['estado']) {
              case 1:
              $state = "Pendiente";
              $color_state = "yellow";
              break;

              case 2:
              $state = "Finalizado";
              $color_state = "green";
              break;

              case 3:
              $state = "Enviado";
              $color_state = "blue";
              break;

              case 4:
              $state = "Cancelado";
              $color_state = "red";
              break;

              default:
                $state = "Error de estado";
                $color_state = "red";
              break;
            }

            ?>
            <div class="col-md-12">
              <div class="panel panel-<?=$color_state?> userlist">
                <div class="panel-body text-center">
                  <div class="userprofile">
                    <h3 class="username">P#<?=$order['id']?></h3>
                    <p><?=$state?></p>
                  </div>
                  <p>Q <?=number_format($order['total'],2)?></p>
                </div>
                <div class="panel-footer"> <a href="pedido.php?id=<?=$order['id']?>" class="btn btn-link">Ver pedido</a> <a href="pedido.php?id=<?=$order['id']?>"
                  class="btn btn-link pull-right favorite"><i class="fa  fa-mail-forward"></i></a> </div>
              </div>
            </div>

            <?php
              $type = $_GET['type'] != "" ? strtolower($_GET['type']) : "";
              if($type != ""){
                $query_payment = "SELECT * FROM formas_pago WHERE id_empresa=".$empresaid." AND id_pedido=".$order['id'];

                if($type != ""){
                  if($type != "all"){
                    $query_payment = $query_payment . " AND tipo_pago='".$type."'";
                  }
                }

                $payments = $db->getData($query_payment);

                foreach ($payments as $payment) { ?>
                  <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="panel panel-danger userlist">
                      <div class="panel-heading">
                        <h3 class="page-header small"><?=ucfirst($payment['tipo_pago']) ?></h3>
                        <a href="" class="availablity btn btn-circle btn-success"><i class="fa fa-check"></i></a> </div>
                      <div class="panel-body text-center">
                        <div class="userprofile">

                          <h3 class="username"><?=$payment['banco']?></h3>
                          <p><?=$payment['lugar_ingreso']?></p>
                        </div>
                        <strong>Ingreso</strong><br>
                        <p><?=$payment['fecha_ingreso']?></p>
                        <strong>Cambio</strong><br>
                        <p><?=$payment['fecha_cambio']?></p>

                      </div>
                      <div class="panel-footer"> <a href="#" class="btn btn-link">Q <?=number_format($payment['monto'],2)?></a> </div>
                    </div>
                  </div>
              <?php
                }
              }
            }
          }
      ?>

    </div>
  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

<?php // include '../includes/chat2.php'; ?>
<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/adminnine.js"></script>
<script>
        $(document).ready(function() {
            $('#dataTables-userlist').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">",
                        sLast: ">>"
                    }
                }
            });
        });
    </script>
</body>
</html>
