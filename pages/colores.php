<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Colores</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Colores</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> Listado de Colores 
            <a href="add_color.php" class="btn btn-success btn-circle pull-right"><i class="fa fa-plus"></i></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
                <thead>
                  <tr>                    
                    <th>Nombre</th>
                    <th>Color</th>
                    <th>Color 2</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    /* listado de proveedores */
                    $query_color = "SELECT * FROM color WHERE id_empresa=".$empresaid." AND estado = 1";
                    $colors = $db->getData($query_color);

                    if($colors){
                      foreach ($colors as $color) { ?>
                      <tr class="item-<?=$color['id']?>">                        
                        <td ><?=$color['nombre']?></td>
                        <td ><a href="#" class="btn btn-circle  " style="background-color: <?=$color['color']?>; border:solid 1px #f0f3f5;"></a></td>
                        <td ><a href="#" class="btn btn-circle  " style="background-color: <?=$color['color2']?>; border:solid 1px #f0f3f5;"></a></td>
                        <td >
                          <a href="edit_color.php?id=<?=$color['id']?>" class="btn btn-warning btn-circle"><i class="fa fa-pencil"></i> </a>
                          <button type="button" onClick="deleteItem(<?=$color['id']?>, 'color')" class="btn btn-danger btn-circle"><i class="fa fa-times"></i> </button>
                        </td>
                      </tr>
                  <?php
                      }
                    }else{

                    }
                  ?>
                  
                
                </tbody>
              </table><!-- /.table-responsive -->           
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });
    </script>
</body>
</html>
