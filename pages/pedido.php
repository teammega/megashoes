<?php
  include '../includes/config.php';

  $query_payments = "SELECT * FROM formas_pago WHERE id_pedido = ".$_GET['id'];
  $payments = $db->getData($query_payments);

  $paid = getPartialPayment($payments);
  
  function getPartialPayment($payments){
      $pay = 0;

      if(count($payments) > 0){
          foreach($payments as $payment){
            if($payment['tipo_pago']!='cheques'){
              $pay = $pay + $payment['monto'];
            }
          }
      }

      return $pay;
  }

  if (isset($_SESSION['usuario'])) {

  }else{
    header('Location: login.php');
  }
  $id_order = $_GET['id'];
  $query_order = "SELECT * FROM pedido WHERE id_empresa = $empresaid AND id = $id_order";
  $order = $db->getData($query_order)[0];
  $TOTAL = $order['total'] - (($order['total'] - $order['flete']) * ($order['descuento'] / 100));
  $porcentaje_pedido = $order['porcentaje_pedido'];
  $porcentaje_factura = $order['porcentaje_factura'];

  $flete = $order['flete'];
  $subtotal = $order['total'] - $flete;

  if ($order) {
    $observaciones = $order['observaciones'];
  }else{
   header('Location: index.php');
  }

  /*CONSULTAS EXTERNAS*/
    /*VENDEDOR*/
      $query_user = "SELECT * FROM usuario WHERE id_empresa = $empresaid AND id = ".$order['id_usuario'];
      $users = $db->getData($query_user)[0];

      if ($users) {
        $user = $users['nombre']." ".$users['apellido'];
      }else{
        $user = "No asignado";
      }
      /*VENDEDOR*/

       /*CLIENTE*/
        $query_client = "SELECT * FROM cliente WHERE id_empresa = $empresaid AND id =  ".$order['id_cliente'];
        $clients = $db->getData($query_client)[0];
        if ($clients) {
          $client = $clients['nombre']." ".$clients['apellido'];
          /*OBSERVACION TIPO CLIENTE*/

            $query_type = "SELECT * FROM tipo_cliente WHERE id =".$clients['tipo'];
            $type = $db->getData($query_type)[0];

            // $type = $db->getData($query_type)[0];
          /*OBSERVACION TIPO CLIENTE*/
        }else{
          $client = "No asignado";
        }
        /*CLIENTE*/
  /*CONSULTAS EXTERNAS*/

  /*ESTADO*/
    switch ($order['estado']) {
      case 1:
        $estado = "Pendiente";
        $estado_color = "#ff5722";
      break;

      case 2:
        $estado = "Completado";
        $estado_color = "#8bc34a";
      break;

      case 3:
        $estado = "Enviado";
        $estado_color = "#ff5722";
      break;

      case 4:
        $estado = "Cancelado";
        $estado_color = "#673ab7";
      break;

      default:
        $estado = "Error";
        $estado_color = "red";
      break;
    }
  /*ESTADO*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Envío #<?=$order['no_envio']?></title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">




<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">

      .bt_float_order{
        cursor: pointer;
        height: 70px;
        width: 70px;
        border-radius: 100%;
        background-color: #F44336;
        box-shadow: 0px 0px 5px #000;
        position: fixed;
        right: 15px;
        z-index: 10;
        bottom: 30px;
        -webkit-transition: box-shadow 0.5s;
      }

      table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 10px;
        color: black;
      }
      
      th {
        background-color: #93deff;
        padding: 10px 12px 0px 2px !important;
      }

      .client-text {
        font-size: 15px !important;
      }

      .bt_float_order p{
        color:white;
         margin-left:35% ;
         margin-top: 10%;
         font-size: 40px;
      }

      .bt_float_order:hover{
        box-shadow: 0px 0px 15px #000;
      }

      .container_producst_order{
        height: 300px;
        width: 100%;
        margin-top: 20px;
        overflow: auto;
      }

      .product_modal{
        width: 95%;
        margin-left: 10px;
        margin-top: 10px;
        cursor: pointer;
        box-shadow: 0px 0px 1px #000;
        -webkit-transition: box-shadow 0.5s;
      }

      .product_modal:hover{
       box-shadow: 0px 0px 6px #000;
      }

      .container_quantity{
        height: 300px;
        width: 100%;
      }

    </style>

</head>

<body>

<div class="modal fade" id="update_quantity_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title " id="myModalLabel3">Actualizar</h4>
      </div>
      <div class="modal-body" style="height: 250px">
        <div class="col-md-12" >
          <div class="col-md-4"></div>
          <div class="col-md-4 text-center">
            <input type="number" class="input_id_product" value="0" style="visibility: hidden;">
            <input type="number" class="form-control quantity_product_input" max="" min="1"  placeholder="Cantidad">
             <!-- en este <a> se puede agregar en la class btn_update_quantity para actualizar cantidad -->
            <p> <a  class="btn "  style="margin-top: 20px;background-color: #8bc34a; color:white" ><span class="fa fa-check" style="margin-right: 10px"></span>Agregar</a> </p>

          </div>
          <div class="col-md-4"></div>
        </div>
        <div class="col-md-12 text-center">
          <h2 class="texto_alerta_cantidad"></h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_save_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title " id="myModalLabel3">Boleta</h4>
      </div>
      <div class="modal-body" style="height: 250px">
        <div class="col-md-12" >
          <div class=""></div>
          <div class="col-md-12 text-center">

          <form method="POST" action="../forms/add_new_payment_method.php" role="form" enctype="multipart/form-data">
            <input name="boleta"  class="form-control input_boleta" placeholder="Boleta..."  />
            
            <input name="id_payment" class="form-control input_id_payment"  style="visibility: hidden;" />

            <input name="id_order"  class="form-control input_id_order"  style="visibility: hidden;" />

            <button type="submit" class="btn btn-default">Confirmar</button>
          </form>
          </div>
          <div class=""></div>
        </div>
        <div class="col-md-12 text-center">
          <h2 class="texto_alerta_cantidad"></h2>
        </div>
      </div>
    </div>
  </div>
</div>


 <div class="modal fade" id="add_product_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title " id="myModalLabel3">Productos</h4>
      </div>
      <div class="modal-body">

      <div class="container_producst_order">
        <?php
          $query_product = "SELECT * FROM producto WHERE id_empresa = $empresaid AND estado = 1 AND cantidad > 0 ORDER BY nombre ASC";
          $products = $db->getData($query_product);
          if ($products) {
            foreach ($products as $product) {?>
                <div class="col-md-12 product_modal" id_product = "<?=$product['id']?>">
                  <div class="col-md-3">
                    <img src="<?=$product['img']?>" style="border-radius: 100%;width: 60px; height: 60px;"  alt="" class="media-object">
                  </div>
                  <div class="col-md-3">
                    <h3><?=$product['nombre']?></h3>
                  </div>
                  <div class="col-md-3">
                    <h3>Q <?=number_format($product['precio'],2)?></h3>
                  </div>
                  <div class="col-md-3">
                    <h3><?=$product['cantidad']?></h3>
                  </div>
                </div>
              <?php
            }
          }
        ?>
      </div>

   

      <div class="container_quantity" >
      <input type="text" class="form-control id_product_select" value=""  style="visibility: hidden;">
        <input type="number" class="form-control quantity_product" max=""  placeholder="Cantidad">
        <div class="col-md-12">
          <div class="col-md-6 text-center">
            <p> <a  class="btn back_order_add_product" style="margin-top: 20px;background-color: #2196f3; color:white"><span class="fa fa-automobile" style="margin-right: 10px"></span>Regresar</a> </p>
          </div>
          <div class="col-md-6 text-center">
            <p> <a  class="btn add_product"  style="margin-top: 20px;background-color: #8bc34a; color:white" ><span class="fa fa-check" style="margin-right: 10px"></span>Agregar Producto</a> </p>
          </div>

        </div>
        <div class="col-md-12 text-center">
          <h2 class="texto_alerta"></h2>
        </div>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->

  <!-- Page Content -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header id_orden" id_order="<?=$id_order?>">Envío - <?=$order['no_envio']?></h1>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
      <?php
        if ($order['estado']==1) {?>
          <div class="bt_float_order"  style="display: none;">
            <p style="">+</p>
          </div>
          <?php
        }
      ?>

      <div class="row">
        <div class=" col-sm-12">
            <div class="panel panel-default userlist">
              <div class="panel-body ">
                <div class="product_item">
                    <div class="row">
                        <div class="col-md-4"> <img src="../img/pedido.jpg" style="box-shadow: 0px 0px 4px #000;" alt="" class="productpic"></div>
                        <div class="col-md-4 text-left">
                            <h3 class="username">Vendedor</h3>
                            <p class="client-text"><?=$user?></p>
                            <h3 class="username">Cliente</h3>
                            <p class="client-text"><?=$client?></p>
                            <h3 class="username">Tipo de cliente</h3>
                            <p class="client-text"><?=$type['descripcion']?></p>
                        </div>
                        <div class="col-md-4 text-left">
                            <h3 class="username">Fecha</h3>
                            <p class="client-text"><?=$order['fecha']?></p>
                            <h3 class="username">Fecha entrega</h3>
                            <p class="client-text"><?=$order['fecha_entrega']?></p>
                        </div>
                    </div>
                    <?php
                      $user_access_style = $_SESSION['tipo_usuario'] == 1 ? "display: block;" : "display: none;";
                    ?>
                    <div class="row col-md-12" style="margin-top: 25px; <?=$user_access_style ?>">
                      <?php
                        if ($order['estado']==4) { ?>
                          <div class="col-md-3 text-left" style="display: none;">
                            <p> <a  class="btn btn_action" estado = "1" style="background-color: #673ab7; color:white" id_order = "<?=$order['id']?>"><span class="fa fa-times" style="margin-right: 10px"></span>Restaurar</a> </p>
                          </div>
                      <?php
                        }
                      ?>
                      <div class="col-md-3 text-left">
                            <p> <a  class="btn generate_pdf"  style="background-color: #ff5722; color:white" id_order = "<?=$order['id']?>"><span class="fa fa-file-pdf-o" style="margin-right: 10px"></span>Envío</a> </p>
                          </div>
                          <div class="col-md-3 text-left">
                            <p> <a  class="btn generate_factura" href="#" style="background-color: #03A9F4; color:white" id_order = "<?=$order['id']?>"><span class="fa fa-file-text-o" style="margin-right: 10px"></span>FACTURA</a> </p>
                          </div>
                    </div>
                  </div>
                    <!-- /.row -->
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h1 class="page-header small">Factura </h1>
              </div>
              <?php
                  $total = 0;
                $query_order_detail = "SELECT * FROM detalle_pedido WHERE id_empresa = $empresaid AND id_pedido = $id_order AND estado = 1";
                $orders_detail = $db->getData($query_order_detail);
                if ($orders_detail) {
                 foreach ($orders_detail as $order) {
                    /*CONSULTAS EXTERNAS*/
                      /*PRODUCTO*/
                        $query_product = "SELECT * FROM producto WHERE id_empresa = $empresaid AND id = ".$order['id_producto'];
                        $product = $db->getData($query_product)[0];
                        $query_box = "SELECT * FROM caja WHERE estado=1 AND id=".$product['id_caja'];
                        $box = $db->getData($query_box)[0];

                        if ($product) {
                          $total_prod = $product['precio'] * $order['cantidad'] * $box['total'];
                          $total = $total + $total_prod;
                          ?>
                            <div class="col-md-12" id="cont_<?=$order['id']?>" style="margin-top: 15px;">
                              <div class="col-md-4  ">
                                <img src="<?=$product['img']?>" style="border-radius: 100%;width: 60px; height: 60px;"  alt="" class="media-object"><p><?=$product['nombre']?></p>
                              </div>
                              <div class="col-md-2 text-center">
                                <p>Q <?=number_format($product['precio'],2)?></p>
                              </div>
                              <!-- se le puede agregar esto para actualizar la cantidad update_quantity  -->
                              <div class="col-md-2 text-center " quantity =<?=$order['cantidad']?>  id_product=<?=$product['id']?>>
                                <p><strong>Cajas: </strong><?=$order['cantidad']?></p>
                                <p><strong>Pares: </strong><?=$box['total']?></p>
                              </div>
                              <div class="col-md-2 text-center">
                                Q <?=number_format($total_prod,2)?>
                              </div>
                            </div>
                          <?php
                        }
                      /*PRODUCTO*/
                    /*CONSULTAS EXTERNAS*/
                  }
                }
              ?>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="text-left">
                  <?php
                    $total_pedido = $TOTAL * ($porcentaje_pedido / 100);
                    $total_factura = $TOTAL * ($porcentaje_factura / 100);
                  ?>
                  <p style="color: black; margin: 0px;"><strong>Total Envio: </strong>Q <?=number_format($total_pedido, 2) ?></p>
                  <p style="color: black; margin: 0px;"><strong>Total Factura: </strong>Q <?=number_format($total_factura, 2) ?></p>
                  <hr>
                  <p style="color: black; margin: 0px;"><strong>Subtotal: </strong>Q <?=number_format($subtotal, 2) ?></p>
                  <p style="color: black; margin: 0px;"><strong>Flete: </strong>Q <?=number_format($flete, 2) ?></p>
                  <?php
                    $restante = number_format($TOTAL - $paid, 2);
                  ?>
                  <p style="color: black; margin: 0px;font-size: 20px;"><strong>TOTAL: </strong>Q <?=number_format($TOTAL,2) ?> - <strong>RESTANTE: </strong> Q <?=$restante?></p>
                </div>
              </div>
            </div>
            <?php
              if ($observaciones != "") {?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="text-left">
                      <h1 class="page-header small">Observaciones</h1>
                      <p class="page-subtitle small"><?=$observaciones?></p>
                    </div>
                  </div>
                </div>
                <?php
              }
            ?>

            <?php
              $query_payment = "SELECT * FROM formas_pago WHERE id_pedido=".$_GET['id'];
              $payments = $db->getData($query_payment);

              $count_payments = count($payments);
              if($count_payments > 0){ ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="text-left">
                  <h1 class="page-header small">Abonos</h1>
                  <table>
                    <thead>
                      <tr>
                        <th>Saldo Inicial</th>
                        <th>Abono</th>
                        <th>Saldo</th>
                        <th>Fecha de Recibo</th>
                        <th>Fecha de Depósito</th>
                        <th>Entregado a:</th>
                        <th>Banco</th>
                        <th>No. Boleta / Cheque</th>
                        <th>Tipo de Pago</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $query_order = "SELECT * FROM pedido WHERE id=".$_GET['id'];
                        $order = $db->getData($query_order)[0];
                        $saldo = $order['total'];
                        $pagado = 0;

                        foreach($payments as $payment){ 
                          if($payment['tipo_pago']!='cheques'){
                            $saldo = $saldo - $payment['monto'];
                          }
                      ?>
                          <tr>
                            <td>Q <?=number_format($order['total'] - $pagado, 2)?></td>
                            <td>Q <?=number_format($payment['monto'], 2) ?></td>
                            <td>Q <?=number_format($saldo, 2)?></td>
                            <td><?=$payment['fecha_recibo']?></td>
                            <td><?=$payment['fecha_deposito']?></td>
                            <td><?=$payment['lugar_ingreso']?></td>
                            <td><?=$payment['banco']?></td>
                            <td><?=$payment['no_doc']?></td>                            
                        <?php
                          $title = "Boleta";

                          if($payment['tipo_pago'] == "cheques"){
                            $title = "Cheque Post-Fechado";
                          }else if($payment['tipo_pago'] == "efectivo"){
                            $title = "Efectivo";
                          }
                        ?>
                            <td><?=$title?></td>
                            <td><?=$payment['descripcion']?></td>
                            <?php
                              if($payment['tipo_pago']=='cheques' and $payment['cheque_procesado'] == 0 ){?>
                                <td><p> <a  class="btn transform_payment" id_payment="<?=$payment['id']?>" id_order = "<?=$_GET['id']?>" style="background-color: #03A9F4; color:white">Boleta</a> </p></td>
                                <?php
                              }else{
                                if($payment['tipo_pago']=='cheques'){
                                  ?><td>Cambiado</td><?php                                  
                                }else{
                                  ?><td>Sin accion</td><?php
                                }
                              }  
                            ?>
                            
                          </tr>
                      <?php
                          if($payment['tipo_pago']!='cheques'){
                            $pagado = $pagado + $payment['monto'];
                          }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <?php
              }
            ?>
              <div class="clearfix"></div>
            </div>


        </div>
      <!-- /.col-sm-12 -->

    </div>
    <!-- /.row -->


  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<script src = "../js/actions_order.js"></script>
<!-- Custom Theme JavaScript -->
<script src="../js/adminnine.js"></script>
</body>
</html>
