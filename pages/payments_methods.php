<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {

    }else{
     header('Location: index.php');
    }
  }else{
    header('Location: login.php');
  }
  $method = strtolower($_GET['type']);

  if($method == "depositos" || $method == "cheques" || $method == "efectivo"){
    $method = ucfirst($method);

    $styleEfectivo = "";
    if($method == "efectivo"){
      $styleEfectivo = " display: none; ";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title><?=$method?></title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header"><?=$method?></h1>

      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="../src/excel_payment_methods.php?type=<?=strtolower($method) ?>">
              <button type="button" class="btn btn-success ">Excel</button></a>
            <a href="add_payment.php?type=<?=$_GET['type']?>" class="btn btn-success btn-circle pull-right"><i class="fa fa-plus"></i></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
                <thead>
                  <tr>
                    <th>No. Envio</th>
                    <th>Cliente</th>
                    <th>Monto (Q)</th>
                    <th>Fecha de Recibo</th>
                    <th>Fecha de Depósito</th>
                    <th>Entregado a:</th>
                    <th>Banco</th>
                    <?php
                      $typeOfPayment = $_GET['type'];
                      $title = "cheques";

                      if($typeOfPayment == "depositos"){
                        $title = "boleta";
                      }
                    ?>
                    <th>No. <?=$title?></th>
                    <th style="display:none;">No. Doc</th>
                    <th style="display:none;">Estado</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $query_payments_methods = "SELECT * FROM formas_pago WHERE tipo_pago='".strtolower($method)."' AND estado !=4  AND id_empresa=".$empresaid;
                    $payments_methods = $db->getData($query_payments_methods);

                    foreach ($payments_methods as $payment_method) { ?>
                    <tr style="cursor:pointer;" onclick="goto('pedido.php?id=<?=$payment_method['id_pedido']?>')">
                      <td><span class="badge blue" style="font-size: 13px;"><?=$payment_method['no_doc']?></span></td>
                      <?php
                        $query_pedido = "SELECT * FROM pedido WHERE id=".$payment_method['id_pedido'];                        
                        $pedido = $db->getData($query_pedido)[0];
                        
                        $query_cliente = "SELECT * FROM cliente WHERE id=".$pedido['id_cliente'];
                        $cliente = $db->getData($query_cliente)[0];
                      ?>
                      <td><?=$cliente['nombre']." ".$cliente['apellido']?></td>
                      <td><?=$payment_method['monto'] ?></td>
                      <td><?=$payment_method['fecha_recibo'] ?></td>
                      <td><?=$payment_method['fecha_deposito'] ?></td>
                      <td><?=$payment_method['lugar_ingreso'] ?></td>
                      <td><?=$payment_method['banco'] ?></td>
                      <td><?=$payment_method['no_id_doc'] ?></td>
                      <td style="display:none;"><?=$payment_method['no_doc'] ?></td>
                      <td style="display:none;">
                        <?php
                          switch ($payment_method['estado']) {
                            case 1:
                              $state = "No cambiado";
                              $color ="yellow";
                            break;
                            case 2:
                              $state = "Cambiado";
                              $color = "green";
                            break;
                            case 3:
                              $state = "Rechazado";
                              $color = "red";
                            break;
                            default:
                              $state = "Error de estado";
                              $color = "red";
                            break;

                          }
                         ?>
                         <span class="badge <?=$color?>" ><?=$state?> </span>
                      </td>
                    </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table><!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->



  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>


<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  function goto(to){
    window.location.replace(to);
  }
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">",
                        sLast: ">>"
                    }
                }
            });
    });
    </script>
</body>
</html>
<?php
  }else{
    header('location: index.php');
  }
?>
