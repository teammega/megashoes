<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {

  }else{
    header('Location: login.php');
  }

  $delete_garbage = "DELETE FROM pedido WHERE estado!=5 AND total=0.00";
  $db->makeQuery($delete_garbage);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Envíos</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/personalizacion.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Envíos</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> Listado de envíos 
            <?php
            	$href = $_SESSION['tipo_usuario'] != 1 ? 'style="display:none"' : '';
            ?>
            <a href="add_pedido.php" <?=$href?> class="btn btn-success btn-circle pull-right"><i class="fa fa-plus"></i></a>
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
              <table class="table " id="dataTables-example">
                <thead>
                  <tr>                    
                    <th>Codigo</th>
                    <th>Vendedor</th>
                    <th>Cliente</th>
                    <th>Fecha</th>
                    <th>F.Entrega</th>
                    <?php
                      if($_SESSION['tipo_usuario'] == 1){
                    ?>
                    <th>Acciones</th>
                    <?php
                      }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    /* listado de pedidos */
                    $query_order = "SELECT id, id_empresa, no_factura, id_usuario, id_cliente, fecha, fecha_entrega, 
                    id_ruta, direccion, observaciones, total, flete, descuento, no_envio, estado, factura, porcentaje_pedido,
                    porcentaje_factura, token FROM pedido WHERE id_empresa=".$empresaid." AND estado!=5 AND total!=0";

                    if (isset($_GET['estado'])) {
                      $estado = $_GET['estado'];
                      $query_order = "SELECT * FROM pedido WHERE estado = $estado AND id_empresa=".$empresaid;                      
                    }

                    if ($_SESSION['tipo_usuario'] == 2) {
                      $query_order = "SELECT * FROM pedido WHERE id_empresa = $empresaid AND id_usuario =".$_SESSION['id_usuario'];
                    }

                    /* FILTROS */
                      if (isset($_GET['idU'])) {
                        $query_order .= " AND id_usuario = ".$_GET['idU'];                        
                      }


                      if (isset($_GET['idC'])) {
                       $query_order .= " AND id_cliente = ".$_GET['idC'];
                      }
                    /* FILTROS */

                    $orders = $db->getData($query_order);

                    if($orders){
                      foreach ($orders as $order) { 
                        /*CONSULTAS EXTERNAS*/
                          /*VENDEDOR*/
                          $query_user = "SELECT * FROM usuario WHERE id_empresa = $empresaid AND id = ".$order['id_usuario'];
                          $users = $db->getData($query_user)[0];

                          if ($users) {
                            $user = $users['nombre']." ".$users['apellido'];
                          }else{
                            $user = "No asignado";
                          }
                          /*VENDEDOR*/

                          /*CLIENTE*/
                          $query_client = "SELECT * FROM cliente WHERE id_empresa = $empresaid AND id =  ".$order['id_cliente'];
                          $clients = $db->getData($query_client)[0];
                          if ($clients) {
                            $client = $clients['nombre']." ".$clients['apellido'];
                          }else{
                            $client = "No asignado";
                          }
                          /*CLIENTE*/
                        /*CONSULTAS EXTERNAS*/

                        /*ESTADO*/
                          switch ($order['estado']) {
                            case 1:
                              $color_font = "#FFFFFF";
                              $estado_color = "#03A9F4";
                            break;

                            case 2:
                              $color_font = "#000000";
                              $estado_color = "#f8ff06";
                            break;
                            
                            default:
                              $estado = "Error";    
                              $estado_color = "red";
                            break;
                          }
                        /*ESTADO*/
                        ?>
                        <tr class="item-<?=$order['id']?>">                        
                          
                          <td onclick="goTo(<?=$order['id']?>)" style="background-color: <?=$estado_color?>; color: <?=$color_font?>;cursor: pointer;" class="text-center td_redirection"><a style="text-decoration: none; color: <?=$color_font?>" href="pedido.php?id=<?=$order['id']?>"><?=$order['no_envio']?></a></td>
                          <td ><?=$user?></td>
                          <td ><?=$client?></td>
                          <?php
                            $date = explode("-", $order['fecha']);
                            $new_date = $date[2]."/".$date[1]."/".$date[0];
                          ?>
                          <td ><?=$new_date?></td>
                          <?php
                            $date2 = explode("-", $order['fecha_entrega']);
                            $new_date2 = $date2[2]."/".$date2[1]."/".$date2[0];
                          ?>
                          <td ><?=$new_date2?></td>

                          <?php
                            if($_SESSION['tipo_usuario'] == 1){
                          ?>
                          <td >
                            <!-- <a href="edit_box.php?id=<?php //$order['id']?>" class="btn btn-warning btn-circle"><i class="fa fa-pencil"></i> </a> -->
                            <button type="button" onClick="deleteItem(<?=$order['id']?>, 'pedido')" class="btn btn-danger btn-circle"><i class="fa fa-times"></i> </button>
                          </td>
                          <?php
                            }
                          ?>
                        </tr>
                        <?php
                      }
                    }else{

                    }
                  ?>
                  
                
                </tbody>
              </table><!-- /.table-responsive -->           
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });

    function goTo(id){
      window.location = 'pedido.php?id='+id;
    }
    </script>
</body>
</html>
