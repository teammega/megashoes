<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Editar Producto</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Editar Producto</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row main-card">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" action="../forms/save_edit_product.php" class="save_product_form" role="form" enctype="multipart/form-data">
                  <?php
                    $id_product = $_GET['id'];
                    $query_product = "SELECT * FROM producto WHERE id_empresa=$empresaid AND id = $id_product AND estado=1";
                    $product = $db->getData($query_product)[0];
                  ?>
                  <input type="hidden" name="id" value="<?=$product['id']?>">
                  <div class="form-group col-md-12">
                    <label>Marca</label>
                    <select class="form-control" name="marca_id" required="true">
                      <option value="">- Seleccione una Marca -</option>
                      <?php
                        $query_marca = "SELECT * FROM marca WHERE id_empresa=".$empresaid." AND estado=1";
                        $marcas = $db->getData($query_marca);

                        if($marcas){
                          foreach ($marcas as $marca) {
                            if($marca['id'] == $product['id_marca']){ ?>
                            <option value="<?=$marca['id']?>" selected><?=$marca['nombre']?></option>
                      <?php
                            }else{ ?>
                            <option value="<?=$marca['id']?>"><?=$marca['nombre']?></option>
                      <?php
                            }
                          }
                        }else{ ?>
                            <option value="">No hay marcas disponibles. Por favor agregue antes de continuar</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Nombre</label>
                    <input class="form-control" required="true" name="nombre" placeholder="Nombre" value="<?=$product['nombre']?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Código</label>
                    <input class="form-control" required="true" name="codigo" placeholder="Código" value="<?=$product['codigo']?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Proveedor</label>
                    <select class="form-control" name="proveedor_id" required="true">
                      <option value="">- Seleccione un Proveedor -</option>
                      <?php
                        $query_proveedor = "SELECT * FROM proveedor WHERE estado=1 AND id_empresa=".$empresaid;
                        $proveedores = $db->getData($query_proveedor);

                        if($proveedores){
                          foreach ($proveedores as $proveedor) { 
                            if($proveedor['id'] == $product['id_proveedor']){ ?>
                              <option value="<?=$proveedor['id']?>" selected ><?=$proveedor['nombre']?></option>
                        <?php
                            }else{ ?>
                              <option value="<?=$proveedor['id']?>"><?=$proveedor['nombre']?></option>
                        <?php
                            }
                          }
                        }else{ ?>
                            <option value="">No hay proveedores disponibles. Por favor agregue antes de continuar</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Tipo de caja</label>
                    <select class="form-control" name="caja_id" required="true">
                      <option value="">- Seleccione un tipo de caja -</option>
                      <?php
                        $query_box = "SELECT * FROM caja WHERE estado=1 AND id_empresa=".$empresaid;
                        $boxes = $db->getData($query_box);

                        if($boxes){
                          foreach ($boxes as $box) {
                            if($box['id'] == $product['id_caja']){ ?>
                              <option value="<?=$box['id']?>" selected><?=$box['nombre']?></option>
                        <?php
                            }else{ ?>
                              <option value="<?=$box['id']?>"><?=$box['nombre']?></option>
                        <?php
                            }
                          }
                        }else{ ?>
                            <option value="">No hay cajas disponibles. Por favor agregue antes de continuar</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Precio Neto</label>
                    <input value="<?=$product['precio_neto']?>" type="number" class="form-control unique_price" required="true" name="precio_neto" placeholder="Precio Neto" >
                  </div>
                  <div class="form-group col-md-6">
                    <label>Porcentaje (%)</label>
                    <input value="<?=$product['porcentaje']?>" type="number" class="form-control percent_detail" required="true" name="porcentaje" placeholder="Porcentaje" >
                  </div>
                  <div class="form-group col-md-6">
                    <label>Precio Unitario</label>
                    <input type="number" class="form-control unit_price" required="true" name="unit_price" placeholder="Precio" value="<?=$product['precio']?>" readonly>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Liquidacion</label>
                    <input type="number" class="form-control" required="true" name="liquidacion" placeholder="Cantidad" value="<?=$product['liquidacion']?>">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Cantidad (Cajas)</label>
                    <input type="number" class="form-control" required="true" name="cantidad" placeholder="Cantidad" value="<?=$product['cantidad']?>">
                  </div>
                  
                  
                  
                  <div class="form-group col-md-6">
                    <label>Color</label>
                    <select class="form-control" name="color_id[]" multiple required="true">
                      <?php
                        $query_color = "SELECT * FROM color WHERE estado=1 AND id_empresa=".$empresaid;
                        $colors = $db->getData($query_color);

                        if($colors){
                          foreach ($colors as $color) {
                            if($color['id'] == $product['id_color']){ ?>
                              <option value="<?=$color['id']?>" selected><?=$color['nombre']?></option>
                        <?php
                            }else{ ?>
                              <option value="<?=$color['id']?>"><?=$color['nombre']?></option>
                        <?php
                            }
                          }
                        }else{ ?>
                            <option value="">No hay colores disponibles. Por favor agregue antes de continuar</option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-12">
                    <label>Descripción</label>
                    <textarea class="form-control" name="descripcion" required="true" placeholder="Descripción"><?=$product['descripcion']?></textarea>
                  </div>
                  <input type="text" name="id" style="visibility: hidden;" value="<?=$product['id']?>" />
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-default">Guardar</button>
                    <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 
<script src="../js/action_detail_product.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });
    </script>
</body>
</html>
