<?php
  include '../includes/config.php';
  $id = $_POST['id'];
  $query_order = "SELECT * FROM pedido WHERE id = $id";
  $order = $db->getData($query_order)[0];
  $query_client = "SELECT * FROM cliente WHERE id = ".$order['id_cliente'];
  $client = $db->getData($query_client)[0];


  /*DATE ORDER */
  $date = explode("-",$order['fecha']);


?>
<!DOCTYPE html>
<html>
  <head>
    <style>
      table{
        /*margin-left: 25px;*/
      }

      th {
        font-weight: initial !important;
      }

      .client_info {
        text-align: left !important;
      }

      .nit{
          margin-left: 180px;
          display: inline-block;
      }
      p.dia{
          margin-left: 200px;
          color:red
      }

      .super_container{
        width: 100%;
        /* background-color: red; */
        /*padding-top: 117px;*/
        /*padding-top: 117px;*/
         padding-top: 96px;
      }

      .super_container_products{
        width: 100%;
        padding-top: 38px;
      }

      .container_products{
        width: 100%;
        padding-top: 8px;
      }

      .container_products_subtotal{
        width: 100%;
        padding-top: 50px;
      }

      .container_products_desc{
        width: 100%;
        padding-top: 15px;
      }
      .container_products_total{
        width: 100%;
        padding-top: 35px;
        /*padding-top: 65px;*/
      }

      .container_tabs_products{
        height: 375px;
        width: 100%;
        /* background-color: red */
      }



    </style>
  </head>
  <body>

      <table class="super_container">
        <tr>
          <th style="width:307px; text-align: left;">
            <p class="client_info"><?=$client['nombre']." ".$client['apellido']?></p>
            <p class="client_info"><?=$client['direccion']?></p>
            <p class="client_info"><?=$client['negocio']?></p>
          </th>
          <th style="width:160px"></th>
          <th></th>
          <th></th>
        </tr>
        <tr>
          <td style="text-align: right;"><p class="nit"><?=$client['nit']?></p></td>
          <td style="text-align: right;"><?=$date[2]?></td>
          <td style="text-align: right;"><?=$date[1]?></td>
          <td style="text-align: right;"><?=$date[0]?></td>
        </tr>

      </table>

      <table class="super_container_products">
        <tr>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </table>

      <div class="container_tabs_products">
        <?php
          $sub_total = 0;
          $query_orders = "SELECT * FROM detalle_pedido WHERE estado = 1 AND id_pedido = ".$order['id'];
          $orders = $db->getData($query_orders);
          if ($orders) {
            foreach ($orders as $order_detail) {
              $query_product = "SELECT * FROM producto WHERE estado = 1 AND id = ".$order_detail['id_producto'];
              $product = $db->getData($query_product)[0];
              $query_box = "SELECT * FROM caja WHERE id=".$product['id_caja'];
              $box = $db->getData($query_box)[0];
              // $total_product = $order_detail['cantidad'] * $product['precio'] * $box['total'];


              $unitario = (15 * $product['precio'] / 100);
              $unitario += $product['precio'];
              $total_product = ($unitario * $order_detail['cantidad'] * $box['total']);
              $total_product = $total_product * ($order['porcentaje_factura']/100);
              $sub_total += $total_product;
              ?>
              <table class="container_products">
                <tr>
                  <th style="width:35px;"></th>
                  <th style="width:160px"></th>
                  <th style="width:385px"></th>
                  <th style=""></th>
                </tr>
                <tr>
                  <td style="text-align: left;"><p><?=$order_detail['cantidad']?></p></td>
                    <td style="text-align: left;"><?=$product['codigo']." ".$product['nombre'] ?></td>
                  <td style="text-align: right;">Q <?=$unitario?></td>
                  <td style="text-align: right;">Q <?= number_format(($total_product),2)  ?></td>
                </tr>

              </table>
              <?php
            }
            $sub_total += $order['flete'];
          }
        ?>
      </div>

      <table class="container_products_subtotal">
        <tr>
          <th style="width:300px;"></th>
          <th style="width:300px"></th>
          <th ></th>

        </tr>
        <tr>
          <td style="text-align: right;"><p></p></td>
          <td style="text-align: right;">Q</td>
          <td style="text-align: right;"><?=number_format($sub_total,2)?></td>
        </tr>

      </table>
      <table class="container_products_desc">
        <tr>
          <th style="width:300px;"></th>
          <th style="width:300px"></th>
          <th ></th>

        </tr>
        <tr>
          <td style="text-align: right;"></td>
          <td style="text-align: right;">Q</td>
          <td style="text-align: right;"><?=number_format($order['descuento'],2)?></td>
        </tr>

      </table>
      <table class="container_products_total">
        <tr>
          <th style="width:300px;"></th>
          <th style="width:300px"></th>
          <th ></th>

        </tr>
        <tr>
          <td style="text-align: right;"></td>
          <td style="text-align: right;">Q</td>
          <td style="text-align: right;"><?=number_format(($sub_total-$order['descuento']),2)?></td>
        </tr>

      </table>



  </body>
</html>
