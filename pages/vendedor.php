<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {
    
  }else{
    header('Location: login.php');
  }

  if ($_SESSION['tipo_usuario']==1) {
    $id_seller = $_GET['id'];  
  }else{
    $id_seller = $_SESSION['id_usuario'];
  }

  $query_seller = "SELECT * FROM usuario WHERE id=$id_seller";
  $seller = $db->getData($query_seller)[0];

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Vendedor</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- swipebox CSS -->
<link href="../vendor/masonry_swipebox/swipebox.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?> 
  <!-- /.navbar-static-side --> 
  
  <!-- Page Content -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Vendedor</h1>
        <!-- <p class="page-subtitle">This is amazing template for all type of admin interfaces.</p> -->
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-md-12 text-center ">
        <div class="panel panel-default">
          <div class="userprofile social ">
            <div class="userpic"> <img style="height: 100px;width: 100px" src="<?=$seller['img']?>"  alt="" class="userpicimg"> </div>
            <h3 class="username"><?=$seller['nombre']." ".$seller['apellido']?></h3>
            <p><?=$seller['correo']?></p>
            <!-- <div class="socials tex-center"> <a href="" class="btn btn-circle btn-primary "><i class="fa fa-facebook"></i></a> <a href="" class="btn btn-circle btn-danger "><i class="fa fa-google-plus"></i></a> <a href="" class="btn btn-circle btn-info "><i class="fa fa-twitter"></i></a> <a href="" class="btn btn-circle btn-warning "><i class="fa fa-envelope"></i></a> </div> -->
          </div>

          <?php
            // CONSULTAS PARA CONTADORES

            $query_client = "SELECT * FROM cliente WHERE estado = 1 AND id_usuario = $id_seller";
            $clients = $db->getData($query_client);            
            $total_clients = count($clients);              


            $query_orde2 = "SELECT * FROM pedido WHERE estado = 1 AND id_usuario = $id_seller AND total!=0";
            $order2 = $db->getData($query_orde2);
            $total_order2 = count($order2);

           

         

            if ($total_order2 > 0) {
              $display="";
              $mostrar = "col-lg-8";
            }else{
              $display = "none";
              $mostrar="col-lg-12";
            }

          ?>



          <div class="col-md-12 border-top border-bottom">
            <ul class="nav nav-pills pull-left countlist" role="tablist">
              <li role="presentation"  >
                <h3><?=$total_clients?><br>
                  <?php
                    if ($total_clients > 0) {
                      $link = "clientes.php?idU=$id_seller";
                    }else{
                      $link = "#";
                    }
                  ?>
                  <small><a style="text-decoration: none;" href="<?=$link?>">Clientes</a></small> </h3>
              </li>
             
              <li role="presentation" >
                <h3><?=$total_order2?><br>
                  <?php
                    if ($total_order2 > 0) {
                      $link = "pedidos.php?idU=$id_seller&estado=1";
                    }else{
                      $link = "#";
                    }
                  ?>
                  <small><a style="text-decoration: none;" href="<?=$link?>">Pedidos pendientes</a></small> </h3>
              </li>
              

            </ul>
            <a href="../src/excel_liquidacion.php?id=<?=$id_seller?>">
              <button class="btn btn-primary followbtn">Generar Reporte</button>
            </a>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- /.col-lg-12 -->
      <div class="col-lg-4 col-sm-12 pull-right" style="display: <?=$display?>">
        <?php
          $query_orde = "SELECT * FROM pedido WHERE estado = 1 AND id_usuario = $id_seller AND total!=0 LIMIT 5";
          $orders = $db->getData($query_orde);
          if ($orders) {
            foreach ($orders as $order) {
                $query_client = "SELECT * FROM usuario WHERE id=".$order['id_usuario'];
                $client = $db->getData($query_client)[0];
                
                $query_client = "SELECT * FROM cliente WHERE estado = 1 AND id_usuario = $id_seller";
                $cliente = $db->getData($query_client)[0];
              ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h1 class="page-header small">Pedido #<?=$order['id']?></h1>
                    <p class="page-subtitle small"><?=$order['observaciones']?> </p>

                  </div>
                  <div class="col-md-12">
                    <dl class="dl-horizontal">
                      <dt>Cliente</dt>
                      <dd><?=$cliente['nombre']." ".$cliente['apellido']?></dd>
                      <dt>Telefono</dt>
                      <dd><?=$client['telefono']?></dd>
                      <dt>Entrega</dt>
                      <dd><?=$order['fecha_entrega']?></dd>
                      <dt>Estado</dt>
                      <?php
                          switch ($order['estado']) {
                            case 1:
                              $orderS = "Pendiente";
                            break;

                            case 2:
                              $orderS = "Finalizado";
                            break;

                            case 1:
                              $orderS = "Enviado";
                            break;

                            case 1:
                              $orderS = "Cancelado";
                            break;
                            
                            default:
                              $orderS = "---";
                            break;
                          }
                      ?>
                      <dd><?=$orderS?></dd>
                      
                    </dl>
                  </div>
                  <div class="col-md-12 photolist">
                    <div class="row">
                      <?php
                        $query_order_detail = "SELECT * FROM detalle_pedido WHERE estado = 1 AND id_pedido = ".$order['id']." Limit 3";

                        $details = $db->getData($query_order_detail);
                        
                        if ($details) {
                          foreach ($details as $detail) {
                            $query_product = "SELECT * FROM producto WHERE estado = 1 AND id =".$detail['id_producto'];
                            $product = $db->getData($query_product)[0];
                            if ($product) {?>
                                <div class="col-sm-3 col-xs-3"><img src="<?=$product['img']?>" class="" alt="" style="height: 40px;width: 40px;"> </div>
                              <?php                  
                            }
                          }
                        }
                      ?>
                      <a href="pedido.php?id=<?=$order['id']?>"><button type="button" class="btn btn-info btn-circle btn-xl" style="height: 45px;width: 45px;"><i class="fa fa-check"></i></button></a> 
                      
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>            
              <?php
            }
          }
        ?>
      </div>

      <div class="<?=$mostrar?> col-sm-12 pull-left posttimeline">
        
        <!-- CUERPO DEL DOCUMENTO -->

        <?php
          $query_client = "SELECT * FROM cliente WHERE estado = 1 AND id_usuario = $id_seller";
          $clients = $db->getData($query_client);
          if ($clients) {
            foreach ($clients as $client) {?>
              <div class="panel panel-default">
                <div class="btn-group pull-right postbtn">
                  <button type="button" class="dotbtn dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <span class="dots"></span> </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="edit_client.php?id=<?=$client['id']?>">Ver / editar</a></li>
                  </ul>
                </div>
                <div class="col-md-12">
                  <div class="media">
                    <div class="media-left"> <a href="javascript:void(0)"> <img src="../img/seller.png" alt="" class="media-object"> </a> </div>
                    <div class="media-body">
                      <h4 class="media-heading"> <?=$client['nombre']." ".$client['apellido']?><br></h4>
                      <br>                     
                      <button class="btn btn-primary " title="Codigo"><?=$client['codigo']?></button>
                      <button class="btn btn-primary " title="Limite credito">Q <?=$client['limite_credito']?></button>
                      <button class="btn btn-primary "><?=$client['dias_credito']?> dias</button>
                      <button class="btn btn-primary " title="Saldo disponible">Q <?=$client['saldo_disponible']?></button>
                      

                      <?php
                        if($client['tipo']=='1'){
                          ?>
                            <button class="btn btn-primary " title="Tipo de cliente">A</button>
                          <?php
                        }elseif($client['tipo']=='2'){
                          ?>
                            <button class="btn btn-warning " title="Tipo de cliente">B</button>
                          <?php
                        }else{
                          ?>
                            <button class="btn btn-danger " title="Tipo de cliente">C</button>
                          <?php
                        }
                      
                      ?>

                      <a class="btn btn-warning " href="historial.php?id=<?=$client['id']?>&type=all">Historial</a>  
                      <br>                    
                      <ul class="nav nav-pills pull-left ">
                        <?php
                          $query_count_orders = "SELECT * FROM pedido WHERE id_cliente =".$client['id'];
                          $result = $db->getData($query_count_orders);
                          if ($result) {
                            $count_orders = count($result);
                          }else{
                            $count_orders = 0;
                          }
                          if ($count_orders > 0 ) {?>
                            <li><a href="pedidos.php?idC=<?=$client['id']?>" title="" ><i class="glyphicon glyphicon-share-alt"></i><?=$count_orders?></a></li>
                            <?php
                          }
                        ?>                        
                      </ul>
                    </div>
                  </div>
                </div>
              </div>            
              <?php
            }
          }
        ?>

      </div>
    </div>
    <!-- /.row --> 
    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->


  <!-- /.panel-heading -->
  
  
  <?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
  <!-- /.panel-body -->
  <!-- /.panel-footer --> 
</div>
<!-- /.panel .chat-panel -->


<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Gallery and swipebox JavaScript --> 
<script src="../vendor/masonry_swipebox/jquery.swipebox.js"></script> 
<script src="../vendor/masonry_swipebox/masonry.pkgd.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 



<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 
<script>
        $('.grid').masonry({
          itemSelector: '.grid-item'
        });
            
        $('.swipebox').swipebox();
    </script>
</body>
</html>
