<?php
  include '../includes/config.php';
  if (isset($_SESSION['usuario'])) {
    if ($_SESSION['tipo_usuario']==1) {
      
    }else{
     header('Location: index.php'); 
    }
  }else{
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Editar Usuario</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Editar Usuario</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="POST" action="../forms/save_edit_user.php" role="form" enctype="multipart/form-data">
                  <?php
                    $user_id = $_GET['id'];
                    $query_user = "SELECT * FROM usuario WHERE id_empresa=".$empresaid." AND id=".$user_id;
                    $user = $db->getData($query_user)[0];
                  ?>
                  <input type="hidden" name="id" value="<?=$user['id']?>">
                  <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" required="true" name="nombre" placeholder="Nombre" value="<?=$user['nombre']?>">
                  </div>
                  <div class="form-group">
                    <label>Apellido</label>
                    <input class="form-control" required="true" name="apellido" placeholder="Apellido" value="<?=$user['apellido']?>">
                  </div>
                  <div class="form-group">
                    <label>Correo Electrónico</label>
                    <input class="form-control" required="true" name="correo" placeholder="Correo Electrónico" value="<?=$user['correo']?>">
                  </div>
                  <div class="form-group">
                    <label>Contraseña</label>
                    <input type="password" class="form-control" required="true" name="clave" placeholder="Contraseña" value="<?=$user['clave']?>">
                  </div>
                  <div class="form-group">
                    <label>Dirección</label>
                    <input class="form-control" required="true" name="direccion" placeholder="Dirección" value="<?=$user['direccion']?>">
                  </div>
                  <div class="form-group ">
                    <label>Ruta</label>
                    <select class="form-control" name="ruta_id" required="true">
                      <?php
                        $query_ruta = "SELECT * FROM ruta WHERE estado = 1 AND id_empresa=".$empresaid;
                        $rutas = $db->getData($query_ruta);

                        foreach ($rutas as $ruta) { 
                            
                            if ($ruta['id'] == $user['id_ruta']) {?>
                              <option value="<?=$ruta['id']?>" selected><?=$ruta['nombre']?></option>
                              <?php
                            }else{?>
                                <option value="<?=$ruta['id']?>"><?=$ruta['nombre']?></option>
                              <?php
                            }
                          ?>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Teléfono</label>
                    <input type="number" class="form-control" required="true" name="telefono" placeholder="Teléfono" value="<?=$user['telefono']?>">
                  </div>
                  <div class="form-group">
                    <label>NIT</label>
                    <input class="form-control" required="true" name="nit" placeholder="NIT" value="<?=$user['nit']?>">
                  </div>
                  <div class="form-group">
                    <label>Rol</label>
                    <select class="form-control" name="rol" required="true">
                      <?php
                        $query_roles = "SELECT * FROM rol WHERE estado = 1";
                        $roles = $db->getData($query_roles);
                        $selected = "";

                        foreach ($roles as $rol) { 
                          if($rol['id'] == $user['id_rol']){
                            $selected = "selected";
                          }else{
                            $selected = "";
                          }
                      ?>
                          <option value="<?=$rol['id']?>" <?=$selected?> ><?=$rol['nombre']?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Imagen</label>
                    <img src="<?=$user['img']?>" style="width: 350px;">
                    <input type="file" name="img">
                  </div>
                  <button type="submit" class="btn btn-default">Editar</button>
                  <button type="reset" class="btn btn-default">Vaciar Formulario</button>
                </form>
              </div>
            </div>
            <!-- /.row (nested) --> 
          </div>
          <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    

    
  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script> 

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });
    </script>
</body>
</html>
