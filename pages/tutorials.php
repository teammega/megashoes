<?php
  include '../includes/config.php';     
  if (isset($_SESSION['usuario'])) {

  }else{
    header('Location: login.php');
  }

  $delete_garbage = "DELETE FROM pedido WHERE estado!=5 AND total=0.00";
  $db->makeQuery($delete_garbage);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=$empresa_favicon?>" type="image/x-icon">
<title>Envíos</title>

<!-- Bootstrap Core CSS -->
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="../css/adminnine.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/personalizacion.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- loader -->
<?php //include '../includes/modal_inicial.php'; ?>
<!-- loader ends -->
<div id="wrapper">
  <?php include '../includes/menu.php'; ?>
  <!-- /.navbar-static-side -->
  <div id="page-wrapper">
    <?php include '../includes/head.php'; ?>
    <div class="row">
      <div class="col-md-12  header-wrapper" >
        <h1 class="page-header">Tutoriales</h1>
        
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> ¿Cómo usar el Inventario?
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/g3Xzk5OMe-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading"> Envíos, Abonos y Liquidaciones
          </div>
          <!-- /.panel-heading -->
            <div class="panel-body">
                <iframe width="853" height="480" src="https://www.youtube.com/embed/WP9IN-372aY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>  
            <!-- /.panel-body --> 
        </div>
        <!-- /.panel --> 
      </div>
      <!-- /.col-lg-12 --> 
    </div>

  </div>
  <!-- /#page-wrapper --> 
  
</div>
<!-- /#wrapper -->
<?php // include '../includes/chat.php'; ?>

  <?php // include '../includes/chat2.php'; ?>
<!-- jQuery --> 
<script src="../vendor/jquery/jquery.min.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script> 

<!-- DataTables JavaScript --> 
<script src="../vendor/datatables/js/jquery.dataTables.min.js"></script> 
<script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script> 
<script src="../vendor/datatables-responsive/dataTables.responsive.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="../js/adminnine.js"></script>
<script src="../js/deleteItem.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference --> 
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                pageLength:10,
                sPaginationType: "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sFirst: "<<",
                        sPrevious: "<",
                        sNext: ">", 
                        sLast: ">>" 
                    }
                }
            });
    });

    function goTo(id){
      window.location = 'pedido.php?id='+id;
    }
    </script>
</body>
</html>
