$(document).ready(function() {
  $('.fecha_recibo').hide()

	$('.state_control').change(function() {
    if ($(this).val()==1) {
      $('.fecha_recibo').hide('slow')
      $('#description_payment').removeClass('col-md-6').addClass('col-md-12');      
    }else{
      $('.fecha_recibo').show('slow')
      $('#description_payment').removeClass('col-md-12').addClass('col-md-6');
    }
  })
});
