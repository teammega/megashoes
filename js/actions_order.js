$(document).ready(function($) {
	
	$(".container_quantity").hide();
	var id_pedido = $(".id_orden").attr("id_order");
	/*UPDATE STATE*/
		$(".btn_action").click(function(event) {
			$.ajax({
				url: '../forms/update_state.php',
				type: 'POST',
				data: {"state" : $(this).attr("estado"),"id_order" : $(this).attr("id_order")},
			})
			.done(function(data) {
				$(".input_observation").val(data);
				$("#update_observations").modal("show");
				update_total(id_pedido);

			});
		});
	/*UPDATE STATE*/

	/*UPDATE OBSERVATIONS*/
		$(".btn_update_observation").click(function(event) {
			var observation = $(".input_observation").val();
			$.ajax({
				url: '../forms/update_observation_order.php',
				type: 'POST',
				data: {id: id_pedido,observacion: observation},
			})
			.done(function(data) {
				$("#update_observations").modal("hide");
				location.reload();
			})

		});
	/*UPDATE OBSERVATIONS*/

	/*CANCER ORDER*/
		$(".cancelar_pedido").click(function(event) {
			$.ajax({
				url: '../forms/cancel_order.php',
				type: 'POST',
				data: {order_id: id_pedido},
			})
			.done(function(data) {

			});

		});
	/*CANCER ORDER*/

	/*UPDATE QUANTITY*/
		$(".update_quantity").click(function(event) {
			$("#update_quantity_modal").modal("show");
			var quantity = $(this).attr('quantity');
			var id_producto = $(this).attr('id_product');
			$(".quantity_product_input").attr('value',quantity);
			$(".input_id_product").attr({
				'value': id_producto
			});
		});

		$(".btn_update_quantity").click(function(event) {
			$quantity = $(".quantity_product_input").val();
			var id_producto = $(".input_id_product").val();
			$.ajax({
				url: '../forms/quantity_product.php',
				type: 'POST',
				data: {id_product: id_producto},
			})
			.done(function(data) {
				$validar = 0;
				$validar = data;

				if ($quantity > $validar) {
					$(".texto_alerta_cantidad").text("La cantidad maxima es "+data);
					setTimeout(function() {
						$(".texto_alerta_cantidad").text("");
					}, 2000);
				}else{
					$.ajax({
						url: '../forms/update_product_quantity.php',
						type: 'POST',
						data: {id_product:id_producto,quantity:$quantity,id_order:id_pedido },
					})
					.done(function(data) {
						// alert(data);
						location.reload();
					})
				}
			})

		});
	/*UPDATE QUANTITY*/

	/*DELETE PRODUCT ORDER*/
		$(".delete_product_order").click(function(event) {
			var id_order = $(this).attr("id_order");
			$("#cont_"+id_order).hide("slow");
			$.ajax({
				url: '../forms/delete_product_order.php',
				type: 'POST',
				data: {order_id: id_order},
			})
			.done(function(data) {
				update_total(id_pedido);
			})

		});
	/*DELETE PRODUCT ORDER*/

	/*ADD PRODUCT*/
		$(".bt_float_order").click(function(event) {
			$("#add_product_order").modal("show");
		});

		$(".product_modal").click(function(event) {
			$id_producto = $(this).attr("id_product");
			$.ajax({
				url: '../forms/max_product.php',
				type: 'POST',
				data: {id: $id_producto,order: id_pedido},
			})
			.done(function(data) {
				if (data=="si") {
					$("#add_product_order").modal("hide");
					location.reload();
				}else{
					$(".container_producst_order").hide("slow");
					$(".container_quantity").show("slow");
					$(".id_product_select").attr({'value': $id_producto});
					$(".quantity_product").attr({'max': data});
				}
			})


		});

		$(".back_order_add_product").click(function(event) {
			$(".container_quantity").hide("slow");
			$(".container_producst_order").show("slow");

		});

		$(".add_product").click(function(event) {
			var quantity = $(".quantity_product").val();
			var id_prod = $(".id_product_select").val();
			var max_quantity = $(".quantity_product").attr('max');
			if (quantity > max_quantity) {

				$(".texto_alerta").text("La cantidad maxima es: "+max_quantity);
				setTimeout(function(){
					$(".texto_alerta").text("");
				}, 2000);
			}else{
				$.ajax({
					url: '../forms/add_product_order.php',
					type: 'POST',
					data: {quantity:quantity,id_product:id_prod,id_order:id_pedido},
				})
				.done(function(data) {
					location.reload();
				})
			}


		});
	/*ADD PRODUCT*/



	/*GENERATE PDF*/
		$(".generate_pdf").click(function(event) {
			$.ajax({
				url: 'order_pdf.php',
				type: 'POST',
				data: {id_order:id_pedido},
			})
			.done(function(data_html) {

				$.ajax({
					url: 'order_pdf_generate.php',
					type: 'POST',
					data: {html:data_html,validar:1},
				})
				.done(function(datas) {
					window.location.href = "order_pdf_generate.php";
				})
			})
		});
	/*GENERATE PDF*/

	/*GENERATE FACTURA*/
		$('.generate_factura').click(function() {
			var id = $(this).attr('id_order')			
			$.ajax({
				url: 'factura.php',
				type: 'POST',
				data: {id:id},
			})
			.done(function(data_html) {

				$.ajax({
					url: 'factura_pdf_generate.php',
					type: 'POST',
					data: {html:data_html,validar:1},
				})
				.done(function(datas) {
					window.location.href = "factura_pdf_generate.php";
				})
			})


		})
	/*GENERATE FACTURA*/

	/*UPDATE TOTAL*/
		function update_total(id) {
			$.ajax({
				url: '../forms/update_total_order.php',
				type: 'POST',
				data: {id_order: id},
			})
			.done(function(data) {
				$(".total_order").text('Q '+data);
			})
		}
	/*UPDATE TOTAL*/


	/* TRANSFORM PAYMENT */

	$('.transform_payment').click(function () { 
		var id_payment = $(this).attr('id_payment')
		var id_order = $(this).attr('id_order')
		
		$('.input_id_payment').val(id_payment)
		$('.input_id_order').val(id_order)

		$('#modal_save_payment').modal('show')
	})
});
