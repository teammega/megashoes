function deleteItem(id, table){
  $.ajax({
    type: 'POST',
    url: 'delete.php',
    data: {'id': id, 'table': table},            
    success: function(data) {
      	$('.item-' + id).hide('slow');
    }
  });
}