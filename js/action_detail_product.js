$( document ).ready(function() {

    $('.unique_price').keyup(function (e) { 
        var input_value = $(this).val()        
        var percent_detail = $('.percent_detail').val()

        if (parseFloat(percent_detail)>0){
            var new_value = parseFloat((input_value * percent_detail) / 100)
            new_value = parseFloat(input_value + new_value)
            $('.unit_price').val(new_value)
        }


        if (input_value == ''){
            $('.percent_detail').attr('disabled',true);
        }else{
            $('.percent_detail').attr('disabled',false);
        }

    })

    $('.percent_detail').keyup(function (e) { 
        
        var percent = $(this).val()      
        var input_unique_price = parseFloat($('.unique_price').val())

        console.log("percent "+percent);
        console.log("input_unique_price "+input_unique_price);

        var new_value = parseFloat((input_unique_price * percent) / 100)        

        console.log("new_value "+new_value);

        new_value = input_unique_price + new_value


        
        $('.unit_price').val(new_value)
        

    })

    $('.boxes_total').keyup(function (e) { 
        var input_value = $(this).val()        
        if (input_value == ''){
            $('.box_count').attr('disabled',true);
        }else{
            $('.box_count').attr('disabled',false);
        }

    })


    $('.box_count').keyup(function (e) { 
       
        var boxes_total = $('.boxes_total').val()
        var box_valuated = $(this).val()
        

        if (parseInt(box_valuated) > parseInt(boxes_total)){            
            $('.btn_save_product').hide('slow')
        }else{
            $('.btn_save_product').show('slow')
        }
       
    })
});
