<div class="navbar-default sidebar" >
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" > <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <?php
        if ($_SESSION['tipo_usuario'] == 1) {
          $link = "edit_company.php";
        }else{
          $link = "#";
        }
      ?>
      <a class="navbar-brand" href="<?=$link?>"><?=$empresa_nombre?></a> 
    </div>
    <div class="clearfix"></div>
    <div class="sidebar-nav navbar-collapse"> 
      
      <!-- user profile pic -->
      <div class="userprofile text-center">
        <div class="userpic"> <img style="height: 100%; width: 100%;" src="<?=$_SESSION['img_usuario']?>" alt="" class="userpicimg"> <a href="user_profile.php" class="btn btn-primary settingbtn"><i class="fa fa-gear"></i></a> </div>
        <h3 class="username"><?=$_SESSION['usuario']?> <?=$_SESSION['apellido_usuario']?></h3>
        
      </div>
      <div class="clearfix"></div>
      <!-- user profile pic -->
      
      <ul class="nav" id="side-menu">
        <?php
          if($_SESSION['tipo_usuario'] == 1){ ?>
            <li> <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a> </li>
            <li> <a  href="javascript:void(0)" class="menudropdown"><i class="fa fa-table fa-fw"></i>Inventario<span class="badge blue">8</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li><a href="inventario.php">Ver inventario</a></li>
                <li><a href="marcas.php">Marcas</a></li>
                <li><a href="tallas.php">Tallas</a></li>
                <li><a href="colores.php">Colores</a></li>
                <!-- <li><a href="products.html">Productos</a></li> -->
                <li><a href="proveedores.php">Proveedores</a></li>
                <li><a href="cajas.php">Cajas</a></li>
                <li><a href="roles.php">Roles de Usuario</a></li>
                <li><a href="usuarios.php">Usuarios</a></li>
                <!-- <li><a href="products_orderstatus.html">Order status</a></li> -->
              </ul>
              <!-- /.nav-second-level --> 
            </li> 
            <li> <a  href="javascript:void(0)" class="menudropdown"><i class="fa fa-automobile fa-fw"></i>Envíos<span class="badge blue">4</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li><a href="pedidos.php">Ver Envíos</a></li>
                <li><a href="tipo_clientes.php">Tipo cliente</a></li>
                <li><a href="clientes.php">Clientes</a></li>
                <li><a href="rutas.php">Rutas</a></li>
              </ul>
              <!-- /.nav-second-level --> 
            </li>
            <li> <a  href="javascript:void(0)" class="menudropdown"><i class="fa fa-money fa-fw"></i>Pagos<span class="badge blue">3</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li><a href="payments_methods.php?type=depositos">Depósitos</a></li>
                <li><a href="payments_methods.php?type=cheques">Cheques</a></li>
                <li><a href="payments_methods.php?type=efectivo">Efectivo</a></li>
              </ul>
              <!-- /.nav-second-level --> 
            </li>
            <li> <a href="liquidaciones.php"><i class="fa fa-file-excel-o fa-fw"></i> Liquidaciones</a> </li>
            <li> <a href="tutorials.php"><i class="fa fa-pencil"></i> Tutoriales</a> </li>
        <?php
          }else{ ?>
            <li> <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a> </li>
             <li> <a  href="javascript:void(0)" class="menudropdown"><i class="fa fa-table fa-fw"></i>Inventario<span class="badge blue">1</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li><a href="user_inventory.php">Ver inventario</a></li>
              </ul>
            </li> 

            <li> <a  href="javascript:void(0)" class="menudropdown"><i class="fa fa-automobile fa-fw"></i>Pedidos<span class="badge blue">2</span> <span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                <li><a href="pedidos.php">Ver Pedidos</a></li>
                <li><a href="clientes.php">Clientes</a></li>                
              </ul>
              <!-- /.nav-second-level --> 
            </li>
        <?php
          }
        ?>
          </ul>
        </li>      

        
      </ul>
    </div>
    <!-- /.sidebar-collapse --> 
  </div>