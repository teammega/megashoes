 
 <div class="modal fade" id="buscarModulos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title nombreBusqueda" id="myModalLabel3"></h4>
      </div>
      <div class="modal-body"> 
        <div class="contenedorBusquedaModulo">  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>

<div class="row">
      <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
        <button class="menubtn pull-left btn "><i class="glyphicon  glyphicon-th"></i></button>
     <!--    <div class="searchwarpper">
          <div class="input-group searchglobal">            
            <input type="text" class="form-control textoBuscarModulo" placeholder="Buscar..." autofocus>
            <span class="input-group-btn">
            <button class="btn btn-default buscarModulo" type="button"><i class="fa fa-search"></i></button>
            </span> </div>
        </div> -->
        <ul class="nav navbar-top-links navbar-right">
          <!-- <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"> <i class="fa fa-envelope fa-fw"></i> </a>
            <ul class="dropdown-menu dropdown-messages">
              <li> <a href="javascript:void(0)">
                <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <strong>John Smith</strong> <span class="pull-right text-muted"> <em>Yesterday</em> </span> </div>
                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                </a> </li>
              <li> <a class="text-center" href="javascript:void(0)"> <strong>Read All Messages</strong> <i class="fa fa-angle-right"></i> </a> </li>
            </ul>
           
          </li> -->
          <!-- /.dropdown -->
         <!--  <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"> <i class="fa fa-tasks fa-fw"></i> <span class="count">9+</span> </a>
            <ul class="dropdown-menu dropdown-tasks">
              <li> <a href="javascript:void(0)">
                <div>
                  <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                  </div>
                </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div>
                  <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                  </div>
                </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div>
                  <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                  </div>
                </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div>
                  <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                  <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                  </div>
                </div>
                </a> </li>
              <li> <a class="text-center" href="javascript:void(0)"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a> </li>
            </ul>
           
          </li> -->
          <!-- /.dropdown -->
         <!--  <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"> <i class="fa fa-bell fa-fw"></i> <span class="count">1</span> </a>
            <ul class="dropdown-menu dropdown-alerts">
              <li> <a href="javascript:void(0)">
                <div> <i class="fa fa-comment fa-fw"></i> New Comment <span class="pull-right text-muted small">4 minutes ago</span> </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <i class="fa fa-twitter fa-fw"></i> 3 New Followers <span class="pull-right text-muted small">12 minutes ago</span> </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <i class="fa fa-envelope fa-fw"></i> Message Sent <span class="pull-right text-muted small">4 minutes ago</span> </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <i class="fa fa-tasks fa-fw"></i> New Task <span class="pull-right text-muted small">4 minutes ago</span> </div>
                </a> </li>
              <li> <a href="javascript:void(0)">
                <div> <i class="fa fa-upload fa-fw"></i> Server Rebooted <span class="pull-right text-muted small">4 minutes ago</span> </div>
                </a> </li>
              <li> <a class="text-center" href="javascript:void(0)"> <strong>See All Alerts</strong> <i class="fa fa-angle-right"></i> </a> </li>
            </ul>
            
          </li> -->
          <!-- /.dropdown -->
          <li class="dropdown"> <a class="dropdown-toggle userdd" data-toggle="dropdown" href="javascript:void(0)">
            <div class="userprofile small "> <span class="userpic"> <img src="<?=$_SESSION['img_usuario']?>" alt="" class="userpicimg"> </span>
              <div class="textcontainer">
                <h3 class="username"><?=$_SESSION['usuario']." ".$_SESSION['apellido_usuario']?></h3>
                <p style="visibility:hidden">Gujarat, India</p>
              </div>
            </div>
            <i class="caret"></i> </a>
            <ul class="dropdown-menu dropdown-user">
              <!-- <li> <a href="socialprofile.html"><i class="fa fa-user fa-fw"></i> Mi perfil</a> </li> -->
              <?php
                if ($_SESSION['tipo_usuario'] == 1) {?>
                    <li> <a href="edit_company.php"><i class="fa fa-gear fa-fw"></i> Editar Empresa</a> </li>
                  <?php
                }
              ?>
              <li> <a href="user_profile.php"><i class="fa fa-user fa-fw"></i> Editar Perfil</a> </li>
              <li> <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a> </li>
            </ul>
            <!-- /.dropdown-user --> 
          </li>
          <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links --> 
        
      </nav>
    </div>