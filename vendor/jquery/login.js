$(document).ready(function() {
	$('.form-login').submit(function() {  
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),            
            success: function(data) {
                console.log(data);
                if (data==0) {
                	$(".mensaje-form").text("Credenciales incorrectas, intentalo denuevo");
                	$(".recuperar-clave").text("Olvidaste la contraseña?");
                	setTimeout(function() {
                			$(".mensaje-form").text("");
                	}, 3000);
                }else if (data==1) { window.location="index.php"; }
            }
        })
        return false;           
    }); 
});